use std::cmp::Ordering;

use cdt::triangulation::observables::DistanceProfile;
use cdt::triangulation::observables::Graph;
use cdt::triangulation::TriangulationID;
use rand::Rng;
use rand::RngCore;

use cdt::triangulation::observables;
use cdt::triangulation::DynTriangulation;

const LAMBDA: f64 = std::f64::consts::LN_2; // ln(2)
const VOLUME_FIXING_POWER: i32 = 2; // Tune this to get desired behaviour

const DEFAULT_VOLUME_FIXING_STRENGTH: f64 = 0.05;
const DEFAULT_MOVE_RATIO: f64 = 0.5;

pub struct Universe<'a> {
    triangulation: DynTriangulation,
    size: usize,                 // The triangle count we want to use for volume fixing
    timeslices: usize,           // The amount of timeslices in the `Triangulation`
    volume_fixing_strength: f64, // Strength of the volume fixing (bigger -> closer to `size`)
    move_ratio: f64,
    rng: &'a mut dyn RngCore,
}

impl Universe<'_> {
    /// Create a new 2D CDT Universe with a triangulation of size `size`
    /// `size` is in terms of amount of `Triangle`s
    ///
    /// Note that `Triangulations` can only have an _even_ amountof `Triangle`s;
    /// if an odd `size` is requested `size - 1` is used instead.
    pub fn new(
        size: usize,
        timeslices: usize,
        volume_fixing_strength: f64,
        move_ratio: f64,
        rng: &mut dyn RngCore,
    ) -> Universe {
        assert!(
            size >= 2 * timeslices,
            "Cannot create a universe with less than two triangles per timeslice!"
        );
        let start_slice_length = size / timeslices / 2; // In terms of vertices per slice
        let mut universe = Universe {
            triangulation: DynTriangulation::new(start_slice_length, timeslices),
            size: (size / 2) * 2,
            timeslices,
            volume_fixing_strength,
            move_ratio,
            rng,
        };

        let shards_to_add = (size - 2 * start_slice_length * timeslices) / 2;
        for _ in 0..shards_to_add {
            universe.add_move();
        }

        universe
    }

    /// Create a new `Universe` with default parameters
    pub fn default(size: usize, timeslices: usize, rng: &mut dyn RngCore) -> Universe {
        Self::new(
            size,
            timeslices,
            DEFAULT_VOLUME_FIXING_STRENGTH,
            DEFAULT_MOVE_RATIO,
            rng,
        )
    }

    /// Create a new `Universe` with ability to allocate a custom amount of maximum memory.
    ///
    /// Added to allow for more memory allocation of small triangulations as these can grow
    /// larger than twice their original size.
    pub fn new_allocate(
        size: usize,
        max_size: usize,
        timeslices: usize,
        volume_fixing_strength: f64,
        move_ratio: f64,
        rng: &mut dyn RngCore,
    ) -> Universe {
        assert!(
            size >= 2 * timeslices,
            "Cannot create a universe with less than two triangles per timeslice!"
        );
        let start_slice_length = size / timeslices / 2; // In terms of vertices per slice
        let mut universe = Universe {
            triangulation: DynTriangulation::new_allocate(start_slice_length, timeslices, max_size),
            size: (size / 2) * 2,
            timeslices,
            volume_fixing_strength,
            move_ratio,
            rng,
        };

        let shards_to_add = (size - 2 * start_slice_length * timeslices) / 2;
        for _ in 0..shards_to_add {
            universe.add_move();
        }

        universe
    }

    pub fn default_allocate(
        size: usize,
        timeslices: usize,
        rng: &mut dyn RngCore,
        max_size: usize,
    ) -> Universe {
        Self::new_allocate(
            size,
            max_size,
            timeslices,
            DEFAULT_VOLUME_FIXING_STRENGTH,
            DEFAULT_MOVE_RATIO,
            rng,
        )
    }
}

impl Universe<'_> {
    /// Acceptence ratio for a flip move
    /// given that the neighbours are not already flipped.
    /// Hence more flip pairs are created.
    fn acceptence_ratio_flip(&self) -> f64 {
        1.0 / (1.0 + 2.0 / self.triangulation.flip_pair_count() as f64)
    }

    /// Perform a Markov-Chain Monte Carlo move
    pub fn step(&mut self) {
        if self.rng.gen_bool(self.move_ratio) {
            // Flip (If flip pairs are removed or acceptence ratio is satisfied)
            let label = self.triangulation.sample_flip(self.rng);
            if self.triangulation.are_neighbours_flip_pairs(label)
                || self.rng.gen_bool(self.acceptence_ratio_flip())
            // Note lazy OR
            {
                self.flip_move()
            }
        } else {
            // Add of remove
            // If VERY LARGE systems are used a casting bug could form here
            let n = self.triangulation.size() as i32;
            let n0 = self.size as i32;
            let dn = n - n0;
            let n4 = self.triangulation.order_four_count();

            if self.rng.gen_bool(0.5) {
                // println!("Addin'");
                let deltan = (dn as f64).powi(VOLUME_FIXING_POWER)
                    - ((dn + 2) as f64).powi(VOLUME_FIXING_POWER);
                let pow = 2.0 * LAMBDA + self.volume_fixing_strength * deltan;
                let factor = n as f64 / (2 * (n4 + 1)) as f64;
                let p = factor * f64::exp(pow);
                if p >= 1.0 || self.rng.gen_bool(p) {
                    self.add_move()
                }
            } else if self.triangulation.has_order_four() {
                // println!("Removin'");
                let deltan = ((dn - 2) as f64).powi(VOLUME_FIXING_POWER)
                    - (dn as f64).powi(VOLUME_FIXING_POWER);
                let pow = 2.0 * LAMBDA + self.volume_fixing_strength * deltan;
                let factor = (2 * n4) as f64 / (n - 2) as f64;
                let p = factor * f64::exp(pow);
                if p >= 1.0 || self.rng.gen_bool(p) {
                    self.add_move()
                }
                self.remove_move()
            }
        }
    }

    pub fn step_small(&mut self) {
        if self.rng.gen_bool(self.move_ratio) {
            // Flip
            self.flip_move_small()
        } else {
            // Add of remove
            let dn = self.triangulation.size() as i32 - self.size as i32;
            let p = match dn.cmp(&0) {
                Ordering::Equal => 0.5,
                Ordering::Less => {
                    let delta = ((dn - 2) as f64).powi(VOLUME_FIXING_POWER)
                        - (dn as f64).powi(VOLUME_FIXING_POWER);
                    1.0 / (1.0 + (-self.volume_fixing_strength * delta).exp())
                }
                Ordering::Greater => {
                    let delta = ((dn + 2) as f64).powi(VOLUME_FIXING_POWER)
                        - (dn as f64).powi(VOLUME_FIXING_POWER);
                    1.0 / (1.0 + (self.volume_fixing_strength * delta).exp())
                }
            };

            if self.rng.gen_bool(p) {
                self.add_move()
            } else if self.triangulation.has_order_four() {
                self.remove_move()
            }
        }
    }

    /// Perform `size` times [step()](Universe::step()),
    /// such that on average each triangle has been updated once.
    pub fn sweep(&mut self) {
        for _ in 0..self.size {
            self.step()
        }
    }

    fn add_move(&mut self) {
        self.triangulation.add(self.triangulation.sample(self.rng))
    }

    fn remove_move(&mut self) {
        self.triangulation
            .remove(self.triangulation.sample_order_four(self.rng))
    }

    fn flip_move(&mut self) {
        self.triangulation
            .flip(self.triangulation.sample_flip(self.rng))
    }

    fn flip_move_small(&mut self) {
        self.triangulation
            .flip_small(self.triangulation.sample_flip(self.rng))
    }
}

impl Universe<'_> {
    /// Compute action of the current state
    /// S = -lambda N - k(N - N_set)^[some even power]
    fn action(&self) -> f64 {
        let n = self.triangulation.size();
        let n0 = self.size;
        let kappa = self.volume_fixing_strength;
        -LAMBDA * n as f64 - kappa * ((n - n0) as f64).powi(VOLUME_FIXING_POWER) as f64
    }

    /// Computes the current size of the triangulation
    pub fn size(&self) -> usize {
        self.triangulation.size()
    }

    /// Returns the size where the universe is set to.
    ///
    /// This should be the average size of the triangulation as well.
    pub fn mean_size(&self) -> usize {
        self.size
    }

    pub fn vertex_order_distribution(&self) -> observables::VertexOrderDistribution {
        observables::vertex_order_distribution(&self.triangulation)
    }

    pub fn length_profile(&self) -> observables::LengthProfile {
        observables::length_profile(&self.triangulation)
    }

    pub fn flip_sites(&self) -> usize {
        self.triangulation.flip_pair_count()
    }

    /// Return ID of the current `Triangulation` state
    pub fn state_id(&self) -> TriangulationID {
        self.triangulation.id()
    }

    pub fn dual_graph(&self) -> Graph {
        observables::dual_graph(&self.triangulation)
    }

    pub fn vertex_graph(&self) -> Graph {
        observables::vertex_graph(&self.triangulation)
    }

    pub fn dual_distance_profile(&mut self) -> DistanceProfile {
        let graph = self.dual_graph();
        let max_distance = (2 * self.timeslices).min(self.size / self.timeslices);
        graph.distance_profile(graph.sample_node(self.rng), max_distance)
    }

    pub fn vertex_distance_profile(&mut self) -> DistanceProfile {
        let graph = self.vertex_graph();
        let max_distance = (2 * self.timeslices).min(self.size / self.timeslices);
        graph.distance_profile(graph.sample_node(self.rng), max_distance)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::SeedableRng;
    use rand_xoshiro::Xoshiro256StarStar;

    #[test]
    fn universe_creation() {
        let rng = &mut Xoshiro256StarStar::seed_from_u64(7);
        let universe = Universe::default(301, 13, rng);
        assert_eq!(
            universe.triangulation.size(),
            300,
            "Triangulation not created with correct amount of triangles"
        );

        assert_eq!(
            universe.size, 300,
            "Univserse not set to correct amount of triangles"
        )
    }
}
