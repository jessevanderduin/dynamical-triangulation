//! This crate is a test create for creating a good configuration file
//! for a scientific copmutational project.
//!
//! In this example for a Causal Dynamical Triangulation Monte Carlo simulation
//!
//! The configuration file is in TOML format and of the following form
//! ```toml
#![doc = include_str!("example_config.toml")]
//! ```

pub mod observables;
use observables::ObservableParameters;
pub mod number_range;
use number_range::NumberRange;

use serde::{Deserialize, Serialize};
use std::{fs::File, io::Read, path::PathBuf, time::UNIX_EPOCH};

use crate::seeding::{ConfigSeed, Seed};

use self::observables::Epsilon;

/// Constants setting the defaults for configuration file
pub const DEFAULT_MEASUREMENT_AMOUNT: usize = 1;
pub const DEFAULT_BURNIN_SWEEP: usize = 0;
pub const DEFAULT_MEASUREMENT_INTERVAL: f32 = 1.0;
pub const DEFAULT_PARAMETER_FILENAME: &str = "measurement";
pub const DEFAULT_TRIANGULATION_FILENAME: &str = "triangulation";
pub const DEFAULT_DISTANCE_PROFILE_NAME: &str = "distance-profile";
pub const DEFAULT_VERTEX_ORDER_NAME: &str = "vertex-order";
pub const DEFAULT_LENGTH_PROFILE_NAME: &str = "length-profile";
pub const DEFAULT_AVERAGE_SPHERE_DISTANCE_NAME: &str = "average-sphere-distance";
pub const DEFAULT_ASD_CORRELATION_PROFILE_NAME: &str = "asd-correlation-profile";
pub const DEFAULT_RANDOM_CORRELATION_PROFILE_NAME: &str = "random-correlation-profile";
pub const DEFAULT_FIELD_CORRELATION_PROFILE_NAME: &str = "random-correlation-profile";
pub const DEFAULT_VISUALISATION_FILENAME: &str = "visualisation";
pub const DEFAULT_DIFFUSION_FILENAME: &str = "visualisation";
const DEFAULT_TRIANGULATION_SAVETYPE: SaveType = SaveType::None;
const DEFAULT_DISTANCE_PROFILE_MAX_DISTANCE: usize = 15;
const DEFAULT_EPSILON: Epsilon = Epsilon::Delta;
const DEFAULT_DELTA: NumberRange<usize> = NumberRange::Number(5);
const DEFAULT_CORRELATION_DELTA: NumberRange<usize> = NumberRange::Number(5);
const DEFAULT_MAX_CORRELATION_DISTANCE: usize = 15;
const DEFAULT_DIFFUSION_COEFFICIENT: NumberRange<f64> = NumberRange::Number(1.0);

pub fn read_config_file(config_path: &str) -> std::io::Result<impl Iterator<Item = Config>> {
    let mut config_file = File::open(config_path)?;
    let mut config_str = String::new();
    config_file.read_to_string(&mut config_str)?;

    // TODO: this is just awful error handling
    let tomld = &mut toml::Deserializer::new(&config_str);
    let config: ConfigRange = serde_ignored::deserialize(tomld, |ignored_path| {
        println!("NOTE: Ignoring key \"{}\" in configuration", ignored_path)
    })?;
    Ok(config.unzip())
}

#[derive(Debug, Clone, Deserialize)]
pub struct Config {
    pub size: usize,
    pub timeslices: usize,
    pub measurements: usize,
    pub burnin: usize,
    pub interval: f32,
    pub output_directory: Option<PathBuf>,
    pub filename: Option<String>,
    pub uid: String,
    pub seed: Seed,
    pub save_triangulation: SaveType,
    pub visualisation: Option<VisualisationType>,
    pub observables: ObservableParameters,
}

/// Configuration struct for deserialization from configuration TOML
/// Including ranges
#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct ConfigRange {
    size: NumberRange<usize>,

    timeslices: usize,

    #[serde(default = "default_measurement_amount")]
    measurements: usize,

    #[serde(default = "default_burnin_sweep")]
    burnin: usize,

    #[serde(default = "default_measurement_interval")]
    interval: f32,

    output_directory: Option<PathBuf>,

    filename: Option<String>,

    seed: Option<ConfigSeed>,

    /// Way to save the triangulation:
    ///  - "all" => Every observable measurement interval
    ///  - "overwrite" =>   At every interval replacing the previous measurement;
    ///                     meant for storing the state of the triangulation in case of
    ///                     intermediate breaking of the simulation.
    ///  - "last" => Save only the triangulation in the final stage
    #[serde(default = "default_triangulation_savetype")]
    save_triangulation: SaveType,

    visualisation: Option<VisualisationType>,

    observables: ObservableParameters,
}

#[derive(Debug, Clone, Copy, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub enum SaveType {
    None,
    All,
    Overwrite,
    Last,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub enum VisualisationType {
    CylinderBasic,
    CylinderTwisted,
    CylinderMinimal,
    TorusBasic,
    TorusTwisted,
}

fn default_measurement_amount() -> usize {
    DEFAULT_MEASUREMENT_AMOUNT
}

fn default_burnin_sweep() -> usize {
    DEFAULT_BURNIN_SWEEP
}

fn default_measurement_interval() -> f32 {
    DEFAULT_MEASUREMENT_INTERVAL
}

fn default_triangulation_savetype() -> SaveType {
    DEFAULT_TRIANGULATION_SAVETYPE
}

impl ConfigRange {
    /// Unzip a [Config] with iterators of some parameters
    /// to an iterator over [Config]s
    fn unzip(self) -> impl Iterator<Item = Config> {
        let timeslices = self.timeslices;
        let measurements = self.measurements;
        let burnin = self.burnin;
        let interval = self.interval;
        let mut seed = if let Some(config_seed) = self.seed.clone() {
            config_seed.into_fullseed()
        } else {
            Seed::from_entropy()
        };

        let id = match UNIX_EPOCH.elapsed() {
            Ok(timestamp) => timestamp.as_millis(),
            Err(e) => {
                eprintln!("Error measuring time {e}; using non-unique ID");
                0
            }
        };
        self.size
            .into_iter()
            .enumerate()
            .map(move |(i, size)| Config {
                size,
                timeslices,
                measurements,
                burnin,
                interval,
                seed: seed.next_seed(),
                uid: format!("{}{:0>1}", id, i),
                output_directory: self.output_directory.clone(),
                filename: self.filename.clone(),
                save_triangulation: self.save_triangulation,
                visualisation: self.visualisation,
                observables: self.observables.clone(),
            })
    }
}

#[cfg(test)]
mod tests {
    use crate::simulation::parameters::Parameters;

    use super::*;

    #[test]
    fn parsing_config() {
        let tomld = &mut toml::Deserializer::new(include_str!("../config/example_config.toml"));
        let configrange: ConfigRange = serde_ignored::deserialize(tomld, |ignored_path| {
            panic!(
                "NOTE: Ignoring key \"{}\" in test configuration",
                ignored_path
            )
        })
        .unwrap();

        for config in configrange.unzip() {
            let param = Parameters::from_configuration(&config);
            let str = toml::to_string_pretty(&param).unwrap();
            eprintln!("{}", str);
        }
    }
}
