use serde::{Deserialize, Serialize};

use crate::simulation::parameters::GraphType;

use super::number_range::NumberRange;

#[derive(Debug, Default, Deserialize, Clone)]
#[serde(rename_all = "kebab-case", default)]
pub struct ObservableParameters {
    pub distance_profile: Vec<DistanceProfile>,
    pub vertex_order: Vec<VertexOrder>,
    pub length_profile: Vec<LengthProfile>,
    pub average_sphere_distance: Vec<AverageSphereDistance>,
    pub asd_correlation_profile: Vec<ASDCorrelationProfile>,
    pub random_correlation_profile: Vec<RandomCorrelationProfile>,
    pub field_correlation_profile: Vec<FieldCorrelationProfile>,
    pub diffusion: Vec<Diffusion>,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct DistanceProfile {
    pub filename: Option<String>,
    pub graph: GraphType,
    #[serde(default = "ObservableParameters::distance_profile_max_distance")]
    pub max_distance: usize,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct VertexOrder {
    pub filename: Option<String>,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct LengthProfile {
    pub filename: Option<String>,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct AverageSphereDistance {
    pub filename: Option<String>,
    pub graph: GraphType,
    #[serde(default = "ObservableParameters::default_epsilon")]
    pub epsilon: Epsilon,
    #[serde(default = "ObservableParameters::default_delta")]
    pub delta: NumberRange<usize>,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct Diffusion {
    pub filename: Option<String>,
    pub graph: GraphType,
    pub max_diffusion_time: usize,
    #[serde(default = "ObservableParameters::default_diffusion_coefficient")]
    pub diffusion_coefficient: NumberRange<f64>,
}

#[derive(Debug, Clone, Copy, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
/// Enum signifying whether the AverageSphereDistance prescrition of epsilon = delta,
/// or episolon = 0 is used
pub enum Epsilon {
    Zero,
    Delta,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct ASDCorrelationProfile {
    pub filename: Option<String>,
    pub graph: GraphType,
    #[serde(default = "ObservableParameters::default_epsilon")]
    pub epsilon: Epsilon,
    #[serde(default = "ObservableParameters::default_correlation_delta")]
    pub delta: NumberRange<usize>,
    #[serde(default = "ObservableParameters::default_max_correlation_distance")]
    pub max_distance: usize,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct RandomCorrelationProfile {
    pub filename: Option<String>,
    pub graph: GraphType,
    #[serde(default = "ObservableParameters::default_correlation_delta")]
    pub delta: NumberRange<usize>,
    #[serde(default = "ObservableParameters::default_max_correlation_distance")]
    pub max_distance: usize,
    pub mean: Option<f64>,
    pub std: Option<f64>,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct FieldCorrelationProfile {
    pub filename: Option<String>,
    pub graph: GraphType,
    #[serde(default = "ObservableParameters::default_correlation_delta")]
    pub delta: NumberRange<usize>,
    #[serde(default = "ObservableParameters::default_max_correlation_distance")]
    pub max_distance: usize,
    pub field: FieldType,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub enum FieldType {
    VertexOrder,
    SphereAreaProfile,
}

impl ObservableParameters {
    fn distance_profile_max_distance() -> usize {
        super::DEFAULT_DISTANCE_PROFILE_MAX_DISTANCE
    }

    fn default_epsilon() -> Epsilon {
        super::DEFAULT_EPSILON
    }

    fn default_delta() -> NumberRange<usize> {
        super::DEFAULT_DELTA
    }

    fn default_correlation_delta() -> NumberRange<usize> {
        super::DEFAULT_CORRELATION_DELTA
    }

    fn default_max_correlation_distance() -> usize {
        super::DEFAULT_MAX_CORRELATION_DISTANCE
    }

    fn default_diffusion_coefficient() -> NumberRange<f64> {
        super::DEFAULT_DIFFUSION_COEFFICIENT
    }
}
