use std::ops::AddAssign;

use super::*;

pub trait DefaultStepSize {
    fn default_step_size() -> Self;
}

/// Inclusive range allowing custom stepping
#[derive(Debug, Serialize, Deserialize, Clone, Copy)]
pub struct SteppedRange<Idx: DefaultStepSize> {
    pub start: Idx,
    pub end: Idx,
    #[serde(default = "Idx::default_step_size")]
    pub step: Idx,
}

impl DefaultStepSize for i32 {
    fn default_step_size() -> Self {
        1
    }
}

impl DefaultStepSize for usize {
    fn default_step_size() -> Self {
        1
    }
}

// TODO: I don't think this Default step size is a good way to go about this
// with having a default step size, it should probably always be specified
impl DefaultStepSize for f64 {
    fn default_step_size() -> Self {
        1.0
    }
}

impl<T: Copy + DefaultStepSize> From<RangeInclusive<T>> for SteppedRange<T> {
    fn from(range: RangeInclusive<T>) -> Self {
        SteppedRange {
            start: *range.start(),
            end: *range.end(),
            step: T::default_step_size(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct IntoIter<Idx> {
    current: Idx,
    end: Idx,
    step: Idx,
}

impl<Idx> IntoIter<Idx> {
    fn new(start: Idx, end: Idx, step: Idx) -> Self {
        IntoIter {
            current: start,
            end,
            step,
        }
    }
}

impl<Idx: DefaultStepSize> SteppedRange<Idx> {
    pub(super) fn into_iter(self) -> IntoIter<Idx> {
        IntoIter::new(self.start, self.end, self.step)
    }
}

impl<Idx: Copy + AddAssign + PartialOrd> Iterator for IntoIter<Idx> {
    type Item = Idx;

    fn next(&mut self) -> Option<Self::Item> {
        let current = self.current;
        if current > self.end {
            return None;
        }
        self.current += self.step;
        Some(current)
    }
}

#[test]
fn test_stepped_range() {
    // Test normal range
    let range = SteppedRange::from(0..=3);
    let mut iter = range.into_iter();
    assert_eq!(iter.next(), Some(0));
    assert_eq!(iter.next(), Some(1));
    assert_eq!(iter.next(), Some(2));
    assert_eq!(iter.next(), Some(3));
    assert_eq!(iter.next(), None);

    // Test step unequal division of range
    let range = SteppedRange {
        start: 0,
        end: 3,
        step: 2,
    };
    let mut iter = range.into_iter();
    assert_eq!(iter.next(), Some(0));
    assert_eq!(iter.next(), Some(2));
    assert_eq!(iter.next(), None);

    // Test inclusivity step
    let range = SteppedRange {
        start: 0,
        end: 4,
        step: 2,
    };
    let mut iter = range.into_iter();
    assert_eq!(iter.next(), Some(0));
    assert_eq!(iter.next(), Some(2));
    assert_eq!(iter.next(), Some(4));
    assert_eq!(iter.next(), None);

    // Test step larger than range
    let range = SteppedRange {
        start: 0,
        end: 4,
        step: 7,
    };
    let mut iter = range.into_iter();
    assert_eq!(iter.next(), Some(0));
    assert_eq!(iter.next(), None);
}
