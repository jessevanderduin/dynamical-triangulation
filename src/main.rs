mod config;
mod seeding;
mod simulation;
#[allow(dead_code)]
mod static_universe;
#[allow(dead_code)]
mod universe;

use std::thread;

use indicatif::MultiProgress;
use simulation::{parameters::Parameters, Simulation};

const CONFIGURATION_FILE_PATH: &str = "./config.toml";

fn main() {
    let configurations = config::read_config_file(CONFIGURATION_FILE_PATH)
        .expect("Could not open the configuration file");

    let multiprogress = MultiProgress::new();
    let mut measurement_sets = Vec::new();
    for config in configurations {
        let parameters = Parameters::from_configuration(&config);

        let mp = multiprogress.clone();
        let handle = thread::spawn(move || {
            let mut simulation = Simulation::with_multiprogress(parameters, &mp);
            simulation.run();
        });
        measurement_sets.push(handle);
    }

    for set in measurement_sets {
        set.join().expect("Measurement could not be completed")
    }
}
