use cdt::triangulation::FixedTriangulation;
use rand::Rng;

use crate::simulation::measurement::UniverseSnapshot;

const DEFAULT_MOVE_RATIO: f64 = 0.5;

pub struct Universe {
    pub triangulation: FixedTriangulation,
    timeslices: usize, // The amount of timeslices in the `Triangulation`
    move_ratio: f64,   // The selection probability between flip and relocation move
}

/// Implementation about creating universes
impl Universe {
    /// Create a new fixed size 2D CDT Universe with a triangulation of size `size`
    /// `size` is in terms of amount of `Triangle`s
    ///
    /// Note that `Triangulations` can only have a size given by:
    /// `2 * timeslices * average_slice_length`, where the `average_slice_length`
    /// is an integer. This need not necessarily be the case but the current
    /// implementation does not allow for other sizes to be constructed.
    /// If this is deemed important, this could be improved in the future.
    pub fn new(size: usize, timeslices: usize, move_ratio: f64) -> Self {
        assert!(
            size >= 2 * timeslices,
            "Cannot create a universe with less than two triangles per timeslice!"
        );
        let start_slice_length = size / timeslices / 2; // In terms of vertices per slice
        Universe {
            triangulation: FixedTriangulation::new(start_slice_length, timeslices),
            timeslices,
            move_ratio,
        }
    }

    /// Create a new `Universe` with default parameters
    pub fn default(size: usize, timeslices: usize) -> Self {
        Self::new(size, timeslices, DEFAULT_MOVE_RATIO)
    }
}

/// Implementation on performing the Markov-chain moves
impl Universe {
    /// Perform a Markov-Chain Monte Carlo move
    pub fn step<R: Rng + ?Sized>(&mut self, rng: &mut R) {
        if rng.gen_bool(self.move_ratio) {
            // Flip
            let label = self.triangulation.sample(rng);
            if self.triangulation.is_flip_pair(label) {
                self.triangulation.flip_checkless(label);
            }
        } else {
            // Relocate
            if !self.triangulation.has_order_four() {
                return;
            }
            let from = self.triangulation.sample_order_four(rng);
            let to = self.triangulation.sample(rng);
            self.triangulation.relocate(from, to);
        }
    }
}

/// Implementation of measurement on the universe
impl Universe {
    /// Computes the current size of the triangulation
    pub fn size(&self) -> usize {
        self.triangulation.size()
    }

    /// Returns the size where the universe is set to.
    ///
    /// This is the size of the static triangulation at any time,
    /// as it does not resize.
    pub fn mean_size(&self) -> usize {
        self.size()
    }

    pub fn create_snapshot(&'_ self) -> UniverseSnapshot<'_, FixedTriangulation> {
        UniverseSnapshot::from_triangulation(&self.triangulation)
    }
}
