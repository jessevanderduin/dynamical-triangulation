use rand::{RngCore, SeedableRng};
use rand_xoshiro::{SplitMix64, Xoshiro256StarStar};
use serde::{Deserialize, Serialize};

const DEFAULT_LONGS_JUMPS: usize = 0;

#[derive(Debug, Clone, Deserialize)]
#[serde(untagged)]
pub enum ConfigSeed {
    FullHex(Seed),
    ShortHex(InnerSeed),
    Short(u64),
}

impl ConfigSeed {
    pub fn into_fullseed(self) -> Seed {
        match self {
            ConfigSeed::FullHex(seed) => seed,
            ConfigSeed::ShortHex(seed) => Seed::from_inner_seed(seed),
            ConfigSeed::Short(seed) => Seed::from_u64(seed),
        }
    }
}

type SeedType = <Xoshiro256StarStar as SeedableRng>::Seed;
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(from = "HexSeed", into = "HexSeed")]
pub struct InnerSeed(SeedType);

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct Seed {
    seed: InnerSeed,
    long_jumps: usize,
}

impl Seed {
    pub fn from_inner_seed(innerseed: InnerSeed) -> Self {
        Seed {
            seed: innerseed,
            long_jumps: DEFAULT_LONGS_JUMPS,
        }
    }

    fn from_seed(seed: SeedType) -> Self {
        Self::from_inner_seed(InnerSeed(seed))
    }

    pub fn from_u64(seed: u64) -> Self {
        let mut rng = SplitMix64::seed_from_u64(seed);
        let mut seed = SeedType::default();
        rng.try_fill_bytes(seed.as_mut()).unwrap();
        Self::from_seed(seed)
    }

    pub fn from_entropy() -> Self {
        let mut seed = SeedType::default();
        if let Err(err) = getrandom::getrandom(seed.as_mut()) {
            panic!("from_entropy failed: {}", err);
        }
        Self::from_seed(seed)
    }
}

impl Seed {
    pub fn next_seed(&mut self) -> Self {
        let seed = self.clone();
        self.long_jumps += 1;
        seed
    }

    pub fn create_rng(&self) -> Xoshiro256StarStar {
        let mut rng = Xoshiro256StarStar::from_seed(self.seed.0);
        // OPTIMIZABLE: if many parallel measurements are done long jumping like this
        // is very ineffective and one want to reuse the previous jumps.
        // But since usually measurements will be done with many less than 100 parallel
        // measurements this should be no issue.
        for _ in 0..self.long_jumps {
            rng.long_jump();
        }
        rng
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
struct HexSeed(String);

impl From<InnerSeed> for HexSeed {
    fn from(seed: InnerSeed) -> Self {
        Self(hex::encode(seed.0))
    }
}
impl From<HexSeed> for InnerSeed {
    fn from(hex: HexSeed) -> Self {
        let mut seed = [0u8; 32];
        hex::decode_to_slice(hex.0, &mut seed).expect("String could not be converted to seed");
        InnerSeed(seed)
    }
}
