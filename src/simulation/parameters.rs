use crate::config::number_range::NumberRange;
use crate::config::observables::{Epsilon, FieldType};
use crate::config::{
    self, observables, Config, SaveType, VisualisationType, DEFAULT_PARAMETER_FILENAME,
    DEFAULT_TRIANGULATION_FILENAME, DEFAULT_VISUALISATION_FILENAME,
};
use crate::seeding::Seed;
use serde::{Deserialize, Serialize};
use std::path::{Path, PathBuf};

const EXPECTED_HAUSDORFF_DIMENSION: i32 = 2;

#[derive(Debug, Clone, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct Parameters {
    pub size: usize,
    pub timeslices: usize,
    pub sweep: usize,

    pub measurements: usize,
    pub burnin: usize,
    pub interval: f32,

    #[serde(skip)]
    pub output_filepath: PathBuf,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub triangulation_filepath: Option<Filepath>,
    #[serde(skip)]
    pub save_triangulation: SaveType,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub visualisation_filepath: Option<Filepath>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub visualisation: Option<VisualisationType>,

    pub seed: Seed,

    #[serde(rename = "observable", skip_serializing_if = "std::vec::Vec::is_empty")]
    pub observables: Vec<ObservableParameters>,
}

#[derive(Debug, Clone, Serialize)]
pub struct ObservableParameters {
    pub filepath: Filepath,
    #[serde(flatten)]
    pub observable: ObservableSpecificParameters,
}

#[derive(Debug, Clone, Serialize)]
#[serde(tag = "type", rename_all = "kebab-case")]
pub enum ObservableSpecificParameters {
    DistanceProfile {
        graph: GraphType,
        max_distance: usize,
    },
    VertexOrder {},
    LengthProfile {},
    Diffusion {
        graph: GraphType,
        max_diffusion_time: usize,
        diffusion_coefficient: f64,
    },
    AverageSphereDistance {
        graph: GraphType,
        epsilon: Epsilon,
        delta: NumberRange<usize>,
    },
    ASDCorrelationProfile {
        graph: GraphType,
        epsilon: Epsilon,
        delta: usize,
        max_distance: usize,
    },
    RandomCorrelationProfile {
        graph: GraphType,
        delta: usize,
        max_distance: usize,
        mean: f64,
        std: f64,
    },
    FieldCorrelationProfile {
        graph: GraphType,
        delta: usize,
        max_distance: usize,
        field: FieldType,
    },
}

#[derive(Debug, Clone, Copy, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
#[allow(clippy::enum_variant_names)]
pub enum GraphType {
    VertexGraph,
    TiledVertexGraph,
    DualGraph,
    TiledDualGraph,
}

impl ObservableParameters {
    fn from_distance_profile(
        distance_profile: &observables::DistanceProfile,
        output_directory: &Path,
        uid: String,
    ) -> Self {
        let filename = distance_profile
            .filename
            .clone()
            .unwrap_or_else(|| config::DEFAULT_DISTANCE_PROFILE_NAME.to_string());
        Self {
            filepath: create_filepath(&format!("{}-{}", filename, uid), output_directory, "csv"),
            observable: ObservableSpecificParameters::DistanceProfile {
                max_distance: distance_profile.max_distance,
                graph: distance_profile.graph,
            },
        }
    }
    fn from_vertex_order(
        vertex_order: &observables::VertexOrder,
        output_directory: &Path,
        uid: String,
    ) -> Self {
        let filename = vertex_order
            .filename
            .clone()
            .unwrap_or_else(|| config::DEFAULT_VERTEX_ORDER_NAME.to_string());
        Self {
            filepath: create_filepath(&format!("{}-{}", filename, uid), output_directory, "csv"),
            observable: ObservableSpecificParameters::VertexOrder {},
        }
    }
    fn from_length_profile(
        length_profile: &observables::LengthProfile,
        output_directory: &Path,
        uid: String,
    ) -> Self {
        let filename = length_profile
            .filename
            .clone()
            .unwrap_or_else(|| config::DEFAULT_LENGTH_PROFILE_NAME.to_string());
        Self {
            filepath: create_filepath(&format!("{}-{}", filename, uid), output_directory, "csv"),
            observable: ObservableSpecificParameters::LengthProfile {},
        }
    }
    fn from_average_sphere_distance(
        average_sphere_distance: &observables::AverageSphereDistance,
        output_directory: &Path,
        uid: String,
    ) -> Self {
        let filename = average_sphere_distance
            .filename
            .clone()
            .unwrap_or_else(|| config::DEFAULT_AVERAGE_SPHERE_DISTANCE_NAME.to_string());
        Self {
            filepath: create_filepath(&format!("{}-{}", filename, uid), output_directory, "csv"),
            observable: ObservableSpecificParameters::AverageSphereDistance {
                delta: average_sphere_distance.delta.clone(),
                epsilon: average_sphere_distance.epsilon,
                graph: average_sphere_distance.graph,
            },
        }
    }
    fn from_asd_correlation_profile<'a>(
        asd_correlation_profile: &'a observables::ASDCorrelationProfile,
        output_directory: &'a Path,
        uid: String,
    ) -> impl Iterator<Item = Self> + 'a {
        let filename = asd_correlation_profile
            .filename
            .clone()
            .unwrap_or_else(|| config::DEFAULT_ASD_CORRELATION_PROFILE_NAME.to_string());
        // TODO: Implement Iter by reference for NumberRange to avoid Clone here
        let observables = asd_correlation_profile
            .delta
            .clone()
            .into_iter()
            .map(
                |delta| ObservableSpecificParameters::ASDCorrelationProfile {
                    epsilon: asd_correlation_profile.epsilon,
                    delta,
                    max_distance: asd_correlation_profile.max_distance,
                    graph: asd_correlation_profile.graph,
                },
            );
        observables.enumerate().map(move |(i, observable)| Self {
            filepath: create_filepath(
                &format!("{}-{}{:0>2}", filename, uid, i),
                output_directory,
                "csv",
            ),

            observable,
        })
    }
    fn from_diffusion<'a>(
        diffusion: &'a observables::Diffusion,
        output_directory: &'a Path,
        uid: String,
    ) -> impl Iterator<Item = Self> + 'a {
        let filename = diffusion
            .filename
            .clone()
            .unwrap_or_else(|| config::DEFAULT_DIFFUSION_FILENAME.to_string());
        // TODO: Implement Iter by reference for NumberRange to avoid Clone here
        let observables = diffusion
            .diffusion_coefficient
            .clone()
            .into_iter()
            .map(|cdiff| ObservableSpecificParameters::Diffusion {
                graph: diffusion.graph,
                diffusion_coefficient: cdiff,
                max_diffusion_time: diffusion.max_diffusion_time,
            });
        observables.enumerate().map(move |(i, observable)| Self {
            filepath: create_filepath(
                &format!("{}-{}{:0>2}", filename, uid, i),
                output_directory,
                "csv",
            ),

            observable,
        })
    }
    fn from_random_correlation_profile<'a>(
        random_correlation_profile: &'a observables::RandomCorrelationProfile,
        output_directory: &'a Path,
        uid: String,
    ) -> impl Iterator<Item = Self> + 'a {
        let filename = random_correlation_profile
            .filename
            .clone()
            .unwrap_or_else(|| config::DEFAULT_RANDOM_CORRELATION_PROFILE_NAME.to_string());
        // TODO: Implement Iter by reference for NumberRange to avoid Clone here
        let observables = random_correlation_profile
            .delta
            .clone()
            .into_iter()
            .map(
                |delta| ObservableSpecificParameters::RandomCorrelationProfile {
                    delta,
                    max_distance: random_correlation_profile.max_distance,
                    graph: random_correlation_profile.graph,
                    mean: random_correlation_profile
                        .mean
                        .unwrap_or(1.2 * delta as f64),
                    std: random_correlation_profile.std.unwrap_or(
                        4e-3 * (delta as f64).powf(1.5 * EXPECTED_HAUSDORFF_DIMENSION as f64 + 1.0),
                    ),
                },
            );
        observables.enumerate().map(move |(i, observable)| Self {
            filepath: create_filepath(
                &format!("{}-{}{:0>2}", filename, uid, i),
                output_directory,
                "csv",
            ),
            observable,
        })
    }
    fn from_field_correlation_profile<'a>(
        field_correlation_profile: &'a observables::FieldCorrelationProfile,
        output_directory: &'a Path,
        uid: String,
    ) -> impl Iterator<Item = Self> + 'a {
        let filename = field_correlation_profile
            .filename
            .clone()
            .unwrap_or_else(|| config::DEFAULT_FIELD_CORRELATION_PROFILE_NAME.to_string());
        // TODO: Implement Iter by reference for NumberRange to avoid Clone here
        let observables = field_correlation_profile
            .delta
            .clone()
            .into_iter()
            .map(
                |delta| ObservableSpecificParameters::FieldCorrelationProfile {
                    delta,
                    max_distance: field_correlation_profile.max_distance,
                    graph: field_correlation_profile.graph,
                    field: field_correlation_profile.field,
                },
            );
        observables.enumerate().map(move |(i, observable)| Self {
            filepath: create_filepath(
                &format!("{}-{}{:0>2}", filename, uid, i),
                output_directory,
                "csv",
            ),
            observable,
        })
    }
}

impl ObservableParameters {
    pub fn type_str(&self) -> &str {
        self.observable.type_str()
    }
}

impl ObservableSpecificParameters {
    fn type_str(&self) -> &str {
        match self {
            Self::DistanceProfile { .. } => config::DEFAULT_DISTANCE_PROFILE_NAME,
            Self::VertexOrder { .. } => config::DEFAULT_VERTEX_ORDER_NAME,
            Self::LengthProfile { .. } => config::DEFAULT_LENGTH_PROFILE_NAME,
            Self::AverageSphereDistance { .. } => config::DEFAULT_AVERAGE_SPHERE_DISTANCE_NAME,
            Self::ASDCorrelationProfile { .. } => config::DEFAULT_ASD_CORRELATION_PROFILE_NAME,
            Self::RandomCorrelationProfile { .. } => {
                config::DEFAULT_RANDOM_CORRELATION_PROFILE_NAME
            }
            Self::Diffusion { .. } => config::DEFAULT_DIFFUSION_FILENAME,
            Self::FieldCorrelationProfile { .. } => config::DEFAULT_FIELD_CORRELATION_PROFILE_NAME,
        }
    }
}

impl Parameters {
    pub fn from_configuration(config: &Config) -> Parameters {
        // Get output directory, defaulting to current directory if not supplied
        let output_directory = config.output_directory.clone().unwrap_or_else(|| {
            std::env::current_dir().expect("Could not determine current directory")
        });

        // Get filename, defaulting to size labelled name if not supplied
        let parameter_filename = format!(
            "{}-{}",
            if let Some(filename) = config.filename.clone() {
                filename
            } else {
                format!("{}-N{}", DEFAULT_PARAMETER_FILENAME, config.size)
            },
            config.uid
        );

        // Determine filepath for JSON parameter file
        let output_filepath = create_filepath(&parameter_filename, &output_directory, "toml").0;
        // Determine possible filepath for triangulation output
        let triangulation_filepath = match config.save_triangulation {
            SaveType::None => None,
            _ => Some(create_filepath(
                &format!(
                    "{}-N{}-{}",
                    DEFAULT_TRIANGULATION_FILENAME, config.size, config.uid
                ),
                &output_directory,
                "csv",
            )),
        };
        // Idem for visualisation filepath
        let visualisation_filepath = config.visualisation.map(|_| {
            create_filepath(
                &format!(
                    "{}-N{}-{}",
                    DEFAULT_VISUALISATION_FILENAME, config.size, config.uid
                ),
                &output_directory,
                "ply",
            )
        });

        // Convert coniguration `config::ObservableParameters` struct into `Vec<..>`
        let observables = Self::observable_parameters_list_from_config(
            &config.observables,
            &output_directory,
            &config.uid,
        );

        Parameters {
            size: config.size,
            timeslices: config.timeslices,
            sweep: config.size,
            measurements: config.measurements,
            burnin: config.burnin,
            interval: config.interval,
            output_filepath,
            seed: config.seed.clone(),
            triangulation_filepath,
            save_triangulation: config.save_triangulation,
            visualisation_filepath,
            visualisation: config.visualisation,
            observables,
        }
    }

    fn observable_parameters_list_from_config(
        observable_parameters: &observables::ObservableParameters,
        output_directory: &Path,
        uid: &str,
    ) -> Vec<ObservableParameters> {
        // Order the vector such that the observables with most pre-computation are first in the list
        // This makes the measurement phase faster as the computation can reuse pre-computations.
        let mut observables = Vec::new();
        // Observable counter
        let mut i = 0;

        observables.extend(observable_parameters.average_sphere_distance.iter().map(
            |average_sphere_distance| {
                i += 1;
                ObservableParameters::from_average_sphere_distance(
                    average_sphere_distance,
                    output_directory,
                    format!("{}{:0>2}", uid, i),
                )
            },
        ));
        observables.extend(
            observable_parameters
                .asd_correlation_profile
                .iter()
                .flat_map(|asd_correlation_profile| {
                    i += 1;
                    ObservableParameters::from_asd_correlation_profile(
                        asd_correlation_profile,
                        output_directory,
                        format!("{}{:0>2}", uid, i),
                    )
                }),
        );
        observables.extend(
            observable_parameters
                .random_correlation_profile
                .iter()
                .flat_map(|random_correlation_profile| {
                    i += 1;
                    ObservableParameters::from_random_correlation_profile(
                        random_correlation_profile,
                        output_directory,
                        format!("{}{:0>2}", uid, i),
                    )
                }),
        );
        observables.extend(
            observable_parameters
                .field_correlation_profile
                .iter()
                .flat_map(|field_correlation_profile| {
                    i += 1;
                    ObservableParameters::from_field_correlation_profile(
                        field_correlation_profile,
                        output_directory,
                        format!("{}{:0>2}", uid, i),
                    )
                }),
        );
        observables.extend(
            observable_parameters
                .diffusion
                .iter()
                .flat_map(|diff| {
                    i += 1;
                    ObservableParameters::from_diffusion(
                        diff,
                        output_directory,
                        format!("{}{:0>2}", uid, i),
                    )
                }),
        );
        observables.extend(
            observable_parameters
                .distance_profile
                .iter()
                .map(|distance_profile| {
                    i += 1;
                    ObservableParameters::from_distance_profile(
                        distance_profile,
                        output_directory,
                        format!("{}{:0>2}", uid, i),
                    )
                }),
        );
        observables.extend(
            observable_parameters
                .length_profile
                .iter()
                .map(|length_profile| {
                    i += 1;
                    ObservableParameters::from_length_profile(
                        length_profile,
                        output_directory,
                        format!("{}{:0>2}", uid, i),
                    )
                }),
        );
        observables.extend(
            observable_parameters
                .vertex_order
                .iter()
                .map(|vertex_order| {
                    i += 1;
                    ObservableParameters::from_vertex_order(
                        vertex_order,
                        output_directory,
                        format!("{}{:0>2}", uid, i),
                    )
                }),
        );
        observables
    }
}

/// Create filepath from given `filename`, `directory` and `extension`
pub fn create_filepath(filename: &str, directory: &Path, extension: &str) -> Filepath {
    let mut directory = directory.to_path_buf();
    assert!(
        directory.is_dir(),
        "Given path '{:?}' does not point to an existing directory",
        directory
    );
    directory = directory
        .canonicalize()
        .expect("Directory could not be made absolute");
    directory.push(filename);
    directory.set_extension(extension);
    Filepath(directory)
}

#[derive(Debug, Clone, Serialize)]
#[serde(into = "StrippedFilepath")]
pub struct Filepath(pub PathBuf);

#[derive(Debug, Clone, Serialize)]
// Strip the directory of the filepath leaving only the filename
struct StrippedFilepath(String);

impl From<Filepath> for StrippedFilepath {
    fn from(path: Filepath) -> Self {
        StrippedFilepath(
            path.0
                .file_name()
                .expect("Filename was somehow wrongly set")
                .to_str()
                .expect("Could not convert string from OSstring")
                .to_owned(),
        )
    }
}
