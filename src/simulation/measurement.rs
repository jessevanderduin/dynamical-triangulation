use cdt::triangulation::observables::diffusion::diffusion;
use rand::Rng;
use serde::Serialize;
use std::fmt::Display;

use cdt::triangulation::observables::{
    self, area_correlation_profile, field_correlation_profile, random_correlation_profile,
    GraphTrait, TiledPlanarGraph, ToroidalGraph, VertexDegree,
};
use cdt::triangulation::{FullTriangulation, Triangulation};

use crate::config::observables::{Epsilon, FieldType};

use super::parameters::{GraphType, ObservableParameters, ObservableSpecificParameters};

pub struct UniverseSnapshot<'a, T> {
    triangulation: &'a T,
    full_triangulation: Option<FullTriangulation>,
    vertex_graph: Option<ToroidalGraph>,
    tiled_vertex_graph: Option<TiledPlanarGraph>,
    dual_graph: Option<ToroidalGraph>,
    tiled_dual_graph: Option<TiledPlanarGraph>,
}

pub enum Observable {
    DistanceProfile(observables::DistanceProfile),
    VertexOrder(observables::VertexOrderDistribution),
    LengthProfile(observables::LengthProfile),
    Diffusion(Vec<f64>),
    AverageSphereDistance(observables::AverageSphereDistanceProfile),
    ASDCorrelationProfile(observables::ASDCorrelationProfile),
    RandomCorrelationProfile(observables::CorrelationProfile<f64>),
    FieldCorrelationProfile(observables::CorrelationProfile<f64>),
    AreaCorrelationProfile(observables::CorrelationProfile<Vec<usize>>),
}

impl<'a, T: Triangulation<'a>> UniverseSnapshot<'a, T> {
    pub fn from_triangulation(triangulation: &'a T) -> Self {
        Self {
            triangulation,
            full_triangulation: None,
            vertex_graph: None,
            tiled_vertex_graph: None,
            dual_graph: None,
            tiled_dual_graph: None,
        }
    }
}

impl<'a, T: Triangulation<'a>> UniverseSnapshot<'a, T> {
    pub fn get_full_triangulation(&mut self) -> &FullTriangulation {
        self.full_triangulation
            .get_or_insert_with(|| FullTriangulation::from_triangulation(self.triangulation))
    }

    fn get_vertex_graph(&mut self) -> &ToroidalGraph {
        if self.vertex_graph.is_none() {
            let vertex_graph = self.get_full_triangulation().construct_vertex_graph();
            self.vertex_graph = Some(vertex_graph);
        }
        self.vertex_graph.as_ref().unwrap()
    }

    fn get_tiled_vertex_graph(&mut self) -> &TiledPlanarGraph {
        if self.tiled_vertex_graph.is_none() {
            let stacked_vertex_graph = self
                .get_full_triangulation()
                .construct_stacked_vertex_graph();
            let tiled_vertex_graph = TiledPlanarGraph::from_stacked_graph(stacked_vertex_graph);
            self.tiled_vertex_graph = Some(tiled_vertex_graph);
        }
        self.tiled_vertex_graph.as_ref().unwrap()
    }

    fn get_dual_graph(&mut self) -> &ToroidalGraph {
        if self.dual_graph.is_none() {
            let dual_graph = self.get_full_triangulation().construct_dual_graph();
            self.dual_graph = Some(dual_graph);
        }
        self.dual_graph.as_ref().unwrap()
    }

    fn get_tiled_dual_graph(&mut self) -> &TiledPlanarGraph {
        if self.dual_graph.is_none() {
            let stacked_dual_graph = self.get_full_triangulation().construct_stacked_dual_graph();
            let tiled_dual_graph = TiledPlanarGraph::from_stacked_graph(stacked_dual_graph);
            self.tiled_dual_graph = Some(tiled_dual_graph);
        }
        self.tiled_dual_graph.as_ref().unwrap()
    }

    /// Note it is setup in such a way to the measurements are more effective
    /// if the measurements with more precompution are performed first,
    /// such that later measurements can make use of them.
    pub fn measure_observable<R>(
        &mut self,
        parameters: &ObservableParameters,
        rng: &mut R,
    ) -> Observable
    where
        R: Rng + ?Sized,
    {
        match &parameters.observable {
            ObservableSpecificParameters::VertexOrder {} => {
                let vertex_order = if let Some(full_triangulation) = &self.full_triangulation {
                    full_triangulation.vertex_order_distribution()
                } else {
                    observables::vertex_order_distribution(self.triangulation)
                };

                Observable::VertexOrder(vertex_order)
            }
            ObservableSpecificParameters::LengthProfile {} => {
                let length_profile = if let Some(full_triangulation) = &self.full_triangulation {
                    full_triangulation.length_profile()
                } else {
                    observables::length_profile(self.triangulation)
                };
                Observable::LengthProfile(length_profile)
            }
            ObservableSpecificParameters::DistanceProfile {
                max_distance,
                graph: graph_type,
            } => {
                let distance_profile = match graph_type {
                    GraphType::VertexGraph => {
                        let graph = self.get_vertex_graph();
                        let origin = graph.sample_node(rng);
                        graph.distance_profile(origin, *max_distance)
                    }
                    GraphType::TiledVertexGraph => {
                        let graph = self.get_tiled_vertex_graph();
                        let origin = graph.sample_node(rng);
                        graph.distance_profile(origin, *max_distance)
                    }
                    _ => unimplemented!(),
                };
                Observable::DistanceProfile(distance_profile)
            }
            ObservableSpecificParameters::Diffusion {
                graph: graph_type,
                max_diffusion_time,
                diffusion_coefficient,
            } => {
                let diffusion_profile = match graph_type {
                    GraphType::VertexGraph => {
                        let graph = self.get_vertex_graph();
                        let origin = graph.sample_node(rng);
                        diffusion(graph, origin, *max_diffusion_time, *diffusion_coefficient)
                    }
                    GraphType::TiledVertexGraph => {
                        let graph = self.get_tiled_vertex_graph();
                        let origin = graph.sample_node(rng);
                        diffusion(graph, origin, *max_diffusion_time, *diffusion_coefficient)
                    }
                    _ => unimplemented!(),
                };
                Observable::Diffusion(diffusion_profile)
            }
            ObservableSpecificParameters::AverageSphereDistance {
                epsilon,
                delta,
                graph: graph_type,
            } => {
                let average_sphere_distance = match graph_type {
                    GraphType::VertexGraph => {
                        let graph = self.get_vertex_graph();
                        match epsilon {
                            Epsilon::Zero => observables::overlapping_sphere_distance_profile(
                                graph,
                                delta.iter(),
                                rng,
                            ),
                            Epsilon::Delta => observables::average_sphere_distance_profile(
                                graph,
                                delta.iter(),
                                rng,
                            ),
                        }
                    }
                    GraphType::TiledVertexGraph => {
                        let graph = self.get_tiled_vertex_graph();
                        match epsilon {
                            Epsilon::Zero => observables::overlapping_sphere_distance_profile(
                                graph,
                                delta.iter(),
                                rng,
                            ),
                            Epsilon::Delta => observables::average_sphere_distance_profile(
                                graph,
                                delta.iter(),
                                rng,
                            ),
                        }
                    }
                    GraphType::DualGraph => {
                        let graph = self.get_dual_graph();
                        match epsilon {
                            Epsilon::Zero => observables::overlapping_sphere_distance_profile(
                                graph,
                                delta.iter(),
                                rng,
                            ),
                            Epsilon::Delta => observables::average_sphere_distance_profile(
                                graph,
                                delta.iter(),
                                rng,
                            ),
                        }
                    }
                    GraphType::TiledDualGraph => {
                        let graph = self.get_tiled_dual_graph();
                        match epsilon {
                            Epsilon::Zero => observables::overlapping_sphere_distance_profile(
                                graph,
                                delta.iter(),
                                rng,
                            ),
                            Epsilon::Delta => observables::average_sphere_distance_profile(
                                graph,
                                delta.iter(),
                                rng,
                            ),
                        }
                    }
                    _ => unimplemented!(),
                };
                Observable::AverageSphereDistance(average_sphere_distance)
            }
            ObservableSpecificParameters::ASDCorrelationProfile {
                epsilon,
                delta,
                max_distance,
                graph: graph_type,
            } => {
                let asd_correlation = match graph_type {
                    GraphType::VertexGraph => {
                        let graph = self.get_vertex_graph();
                        let origin = graph.sample_node(rng);
                        match epsilon {
                            Epsilon::Zero => observables::osd_correlation_profile(
                                graph,
                                origin,
                                *max_distance,
                                *delta,
                                rng,
                            ),
                            Epsilon::Delta => observables::asd_correlation_profile(
                                graph,
                                origin,
                                *max_distance,
                                *delta,
                                rng,
                            ),
                        }
                    }
                    GraphType::TiledVertexGraph => {
                        let graph = self.get_tiled_vertex_graph();
                        let origin = graph.sample_node(rng);
                        match epsilon {
                            Epsilon::Zero => observables::osd_correlation_profile(
                                graph,
                                origin,
                                *max_distance,
                                *delta,
                                rng,
                            ),
                            Epsilon::Delta => observables::asd_correlation_profile(
                                graph,
                                origin,
                                *max_distance,
                                *delta,
                                rng,
                            ),
                        }
                    }
                    GraphType::DualGraph => {
                        let graph = self.get_dual_graph();
                        let origin = graph.sample_node(rng);
                        match epsilon {
                            Epsilon::Zero => observables::osd_correlation_profile(
                                graph,
                                origin,
                                *max_distance,
                                *delta,
                                rng,
                            ),
                            Epsilon::Delta => observables::asd_correlation_profile(
                                graph,
                                origin,
                                *max_distance,
                                *delta,
                                rng,
                            ),
                        }
                    }
                    GraphType::TiledDualGraph => {
                        let graph = self.get_tiled_dual_graph();
                        let origin = graph.sample_node(rng);
                        match epsilon {
                            Epsilon::Zero => observables::osd_correlation_profile(
                                graph,
                                origin,
                                *max_distance,
                                *delta,
                                rng,
                            ),
                            Epsilon::Delta => observables::asd_correlation_profile(
                                graph,
                                origin,
                                *max_distance,
                                *delta,
                                rng,
                            ),
                        }
                    }
                    _ => unimplemented!(),
                };
                Observable::ASDCorrelationProfile(asd_correlation)
            }
            ObservableSpecificParameters::FieldCorrelationProfile {
                delta,
                max_distance,
                graph: graph_type,
                field,
            } => match graph_type {
                GraphType::VertexGraph => {
                    let graph = self.get_vertex_graph();
                    let origin = graph.sample_node(rng);
                    match field {
                        FieldType::VertexOrder => {
                            let field = VertexDegree::new(graph);
                            Observable::FieldCorrelationProfile(field_correlation_profile(
                                graph,
                                origin,
                                *max_distance,
                                *delta,
                                field,
                                rng,
                            ))
                        }
                        FieldType::SphereAreaProfile => Observable::AreaCorrelationProfile(
                            area_correlation_profile(graph, origin, *max_distance, *delta, rng),
                        ),
                        #[allow(unreachable_patterns)]
                        _ => unimplemented!(),
                    }
                }
                GraphType::TiledVertexGraph => {
                    let graph = self.get_tiled_vertex_graph();
                    let origin = graph.sample_node(rng);
                    match field {
                        FieldType::VertexOrder => {
                            let field = VertexDegree::new(graph);
                            Observable::FieldCorrelationProfile(field_correlation_profile(
                                graph,
                                origin,
                                *max_distance,
                                *delta,
                                field,
                                rng,
                            ))
                        }
                        FieldType::SphereAreaProfile => Observable::AreaCorrelationProfile(
                            area_correlation_profile(graph, origin, *max_distance, *delta, rng),
                        ),
                        #[allow(unreachable_patterns)]
                        _ => unimplemented!(),
                    }
                }
                _ => unimplemented!(),
            },
            ObservableSpecificParameters::RandomCorrelationProfile {
                graph: graph_type,
                delta,
                max_distance,
                mean,
                std,
            } => {
                let random_correlation = match graph_type {
                    GraphType::VertexGraph => {
                        let graph = self.get_vertex_graph();
                        let origin = graph.sample_node(rng);
                        random_correlation_profile(
                            graph,
                            origin,
                            *max_distance,
                            *delta,
                            rng,
                            *mean,
                            *std,
                        )
                    }
                    GraphType::TiledVertexGraph => {
                        let graph = self.get_tiled_vertex_graph();
                        let origin = graph.sample_node(rng);
                        random_correlation_profile(
                            graph,
                            origin,
                            *max_distance,
                            *delta,
                            rng,
                            *mean,
                            *std,
                        )
                    }
                    _ => unimplemented!(),
                };
                Observable::RandomCorrelationProfile(random_correlation)
            }
        }
    }
}

impl Display for Observable {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::DistanceProfile(obs) => write!(f, "{:}", obs),
            Self::VertexOrder(obs) => write!(f, "{:}", obs),
            Self::LengthProfile(obs) => write!(f, "{:}", obs),
            Self::AverageSphereDistance(obs) => write!(f, "{:}", obs),
            Self::ASDCorrelationProfile(obs) => write!(f, "{:}", obs),
            Self::RandomCorrelationProfile(obs) => write!(f, "{:}", obs),
            Self::FieldCorrelationProfile(obs) => write!(f, "{:}", obs),
            Self::AreaCorrelationProfile(obs) => write!(f, "{:?}", obs),
            Self::Diffusion(obs) => write!(f, "{:?}", obs),
        }
    }
}
impl Serialize for Observable {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        match self {
            Self::DistanceProfile(obs) => obs.serialize(serializer),
            Self::VertexOrder(obs) => obs.serialize(serializer),
            Self::LengthProfile(obs) => obs.serialize(serializer),
            Self::AverageSphereDistance(obs) => obs.serialize(serializer),
            Self::ASDCorrelationProfile(obs) => obs.serialize(serializer),
            Self::RandomCorrelationProfile(obs) => obs.serialize(serializer),
            Self::FieldCorrelationProfile(obs) => obs.serialize(serializer),
            Self::AreaCorrelationProfile(obs) => obs.serialize(serializer),
            Self::Diffusion(obs) => obs.serialize(serializer),
        }
    }
}
