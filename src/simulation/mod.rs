use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
use serde::Serialize;
use std::{path::PathBuf, time::Duration};

use csv::{Writer, WriterBuilder};
use std::fs::File;
use std::io::Write;

use rand_xoshiro::Xoshiro256StarStar;

use self::parameters::{ObservableSpecificParameters, Parameters};
use crate::{
    config::{SaveType, VisualisationType},
    static_universe::Universe,
};
use cdt::{
    exporting::ply::Ply,
    triangulation::observables::{self, EmbeddedTriangulation},
};

pub mod measurement;
pub mod parameters;

/// Simulation steps taken before refreshing progress bar.
/// This should be chosen such that it updates 15x per second
/// or less to not waste computation time.
const MILLISEC_BETWEEN_SPINNER_UPDATES: u64 = 50;

pub struct Simulation {
    parameters: Parameters,
    rng: Xoshiro256StarStar,
    cdt: Universe,
    progress_bars: ProgressBars,
    writers: Option<Vec<Writer<File>>>,
}

impl Simulation {
    #[allow(dead_code)]
    pub fn new(parameters: Parameters) -> Self {
        let rng = parameters.seed.create_rng();

        let cdt = Universe::default(parameters.size, parameters.timeslices);
        let progress_bars = ProgressBars::setup(&parameters);

        let writers = Some(prepare_writers(&parameters));
        Simulation {
            parameters,
            rng,
            cdt,
            progress_bars,
            writers,
        }
    }

    pub fn with_multiprogress(parameters: Parameters, multiprogress: &MultiProgress) -> Self {
        let rng = parameters.seed.create_rng();

        let cdt = Universe::default(parameters.size, parameters.timeslices);
        let progress_bars = ProgressBars::setup_with_multiprogress(&parameters, multiprogress);

        let writers = Some(prepare_writers(&parameters));
        Simulation {
            parameters,
            rng,
            cdt,
            progress_bars,
            writers,
        }
    }
}

fn prepare_writers(parameters: &Parameters) -> Vec<Writer<File>> {
    let mut wrt = File::create(&parameters.output_filepath).expect("File could not be created");
    let mut configfile = String::new();
    parameters
        .serialize(toml::Serializer::new(&mut configfile).pretty_array(false))
        .expect("Creating the Config TOML went wrong");
    write!(wrt, "{:}", configfile).expect("Writing the Config TOML went wrong");

    // Open all writers
    let mut writers: Vec<Writer<File>> = parameters
        .observables
        .iter()
        .map(|params| {
            WriterBuilder::new()
                .has_headers(false)
                .from_path(&params.filepath.0)
                .unwrap_or_else(|_| panic!("Could not open file for observable {:?}", params))
        })
        .collect();

    // Make headers for some observables
    for (obs, wrt) in parameters.observables.iter().zip(writers.iter_mut()) {
        match &obs.observable {
            // TODO: add explainer comment on first line, doens't work with default CSV
            ObservableSpecificParameters::VertexOrder { .. } => {
                let headers: Vec<usize> = (observables::MIN_VERTEX_ORDER
                    ..observables::MAX_EXPECTED_VERTEX_ORDER + 1)
                    .collect();
                wrt.serialize(&headers).unwrap();
            }
            ObservableSpecificParameters::AverageSphereDistance { delta, .. } => {
                let deltas = delta
                    .iter()
                    .chain(delta.iter())
                    .map(|delta| delta.to_string());
                wrt.write_record(deltas).unwrap();
            }
            ObservableSpecificParameters::ASDCorrelationProfile { max_distance, .. } => {
                let distance_vec: Vec<usize> = (0..*max_distance).collect();
                let headers = (
                    "origin",
                    &distance_vec,
                    "violation_ratio",
                    &distance_vec,
                    &distance_vec,
                    &distance_vec,
                );
                wrt.serialize(&headers).unwrap();
            }
            _ => {}
        }
    }

    writers
}

impl Simulation {
    fn burnin_bar(&self) -> &ProgressBar {
        &self.progress_bars.burnin_bar
    }
    fn measurement_bar(&self) -> &ProgressBar {
        &self.progress_bars.measurement_bar
    }
    fn display_bar(&self) -> &ProgressBar {
        &self.progress_bars.display_bar
    }
    #[allow(dead_code)]
    /// Use to display print messages above the progress bars
    fn println(&self, msg: impl AsRef<str>) {
        self.display_bar().println(msg)
    }

    fn step(&mut self) {
        self.cdt.step(&mut self.rng)
    }

    pub fn run(&mut self) {
        self.run_burnin();
        self.run_simulation();
    }

    fn run_burnin(&mut self) {
        self.burnin_bar().set_message("Equilibrating system...");
        for _ in 0..self.parameters.burnin {
            for _ in 0..self.parameters.sweep {
                self.step();
            }
            self.burnin_bar().inc(1)
        }
        self.burnin_bar().finish_with_message("Equilibrated");
    }

    fn run_simulation(&mut self) {
        // Set progress bars
        self.measurement_bar()
            .set_message("Performing measurements");
        self.measurement_bar()
            .enable_steady_tick(Duration::from_millis(MILLISEC_BETWEEN_SPINNER_UPDATES));

        // Initial measurement
        self.save_triangulation(0);
        self.create_visualisation();
        self.perform_measurements();
        // Update progress bars
        self.measurement_bar().inc(1);
        self.measurement_bar().set_message("Running simulation");

        // Run simulation
        for step in 1..self.parameters.measurements {
            // Run for `interval` sweeps
            let interval_steps = (self.parameters.interval * self.parameters.sweep as f32) as usize;
            for _ in 0..interval_steps {
                self.step();
            }

            self.measurement_bar()
                .set_message("Performing measurements");
            self.save_triangulation(step);
            self.create_visualisation();
            self.perform_measurements();
            self.measurement_bar().inc(1);
            self.measurement_bar().set_message("Running simulation");
        }

        // Clean up progress bars
        self.measurement_bar().disable_steady_tick();
        self.measurement_bar()
            .finish_with_message("Finished simulation");
        self.display_bar().set_prefix("🗸");
        self.display_bar().finish();
    }

    fn create_visualisation(&self) {
        let parameters = &self.parameters;
        let (visualisation_type, filepath) = if let Some(pair) =
            parameters.visualisation.and_then(|vtype| {
                parameters
                    .visualisation_filepath
                    .as_ref()
                    .map(|path| (vtype, path))
            }) {
            pair
        } else {
            return;
        };

        // TODO: computation of the full computation is wasteful if needed for observables,
        // however visualisation should not need to be created very often.
        let mut snapshot = self.cdt.create_snapshot();
        let full_triangulation = snapshot.get_full_triangulation();

        let embedding = match visualisation_type {
            VisualisationType::CylinderBasic => {
                EmbeddedTriangulation::new_cylindrical_basic(full_triangulation)
            }
            VisualisationType::CylinderTwisted => {
                EmbeddedTriangulation::new_cylindrical_minimal(full_triangulation)
            }
            VisualisationType::CylinderMinimal => {
                EmbeddedTriangulation::new_cylindrical_warped(full_triangulation)
            }
            _ => unimplemented!(),
        };
        let ply = Ply::from_embedded_triangulation(&embedding);

        let mut wrt = File::create(&filepath.0).expect("Visualisation file could not be created");
        write!(wrt, "{:}", ply).expect("Writing the PLY to file went wrong");
    }

    fn perform_measurements(&mut self) {
        let mut snapshot = self.cdt.create_snapshot();

        for (i, observable_parameters) in self.parameters.observables.iter().enumerate() {
            let obs = snapshot.measure_observable(observable_parameters, &mut self.rng);
            if let Some(writers) = &mut self.writers {
                // Write output to file mode
                let wrt = &mut writers[i];
                wrt.serialize(obs).expect("Could not serialize observable");
                wrt.flush().expect("Could not flush buffer");
            } else {
                // Write output to terminal mode
                self.measurement_bar().println(format!(
                    "{}: {}",
                    observable_parameters.type_str(),
                    obs
                ));
            }
        }
    }

    fn save_triangulation(&self, step: usize) {
        let parameters = &self.parameters;
        let filepath: PathBuf = match parameters.save_triangulation {
            SaveType::None => return,
            SaveType::Overwrite => {
                let mut filepath = parameters
                    .triangulation_filepath
                    .clone()
                    .expect("The filepath for triangulation savefile is not set")
                    .0;
                let extension = filepath
                    .extension()
                    .expect("Extension does not exist")
                    .to_str()
                    .unwrap();
                let filename = filepath
                    .file_stem()
                    .expect("Current filename does not exist.")
                    .to_str()
                    .unwrap();
                if step > 0 {
                    let old_filename = &format!("{}-{:04}.{}", filename, step - 1, extension);
                    let mut old_filepath = filepath.clone();
                    old_filepath.set_file_name(old_filename);
                    std::fs::remove_file(old_filepath).unwrap_or_else(|_| {
                        panic!(
                            "The previous triangulation savefile {:?} did not exist.",
                            old_filename
                        )
                    });
                }
                filepath.set_file_name(&format!("{}-{:04}.{}", filename, step, extension));
                filepath
            }
            SaveType::All => {
                let mut filepath = parameters
                    .triangulation_filepath
                    .clone()
                    .expect("The filepath for triangulation savefile is not set")
                    .0;
                let extension = filepath
                    .extension()
                    .expect("Extension does not exist")
                    .to_str()
                    .unwrap();
                let filename = filepath
                    .file_stem()
                    .expect("Current filename does not exist.")
                    .to_str()
                    .unwrap();
                filepath.set_file_name(&format!("{}-{:04}.{}", filename, step, extension));
                filepath
            }
            SaveType::Last => {
                if step < parameters.measurements - 1 {
                    return;
                }
                parameters
                    .triangulation_filepath
                    .clone()
                    .expect("The filepath for triangulation savefile is not set")
                    .0
            }
        };

        let mut wrt = WriterBuilder::new()
            .has_headers(false)
            .from_path(&filepath)
            .unwrap_or_else(|_| panic!("Could not create file for triangulation savefile"));
        for triangle in self.cdt.triangulation.triangles() {
            wrt.serialize(triangle).unwrap();
        }
    }
}

struct ProgressBars {
    burnin_bar: ProgressBar,
    measurement_bar: ProgressBar,
    display_bar: ProgressBar,
}

impl ProgressBars {
    fn setup(parameters: &Parameters) -> Self {
        // Setup progress bars in error output
        let multiprogress = MultiProgress::new();
        Self::setup_with_multiprogress(parameters, &multiprogress)
    }

    fn setup_with_multiprogress(parameters: &Parameters, multiprogress: &MultiProgress) -> Self {
        // Setup title display bar with simulation information
        let display_bar = multiprogress.add(ProgressBar::new_spinner());
        let filename = parameters
            .output_filepath
            .file_stem()
            .unwrap()
            .to_str()
            .unwrap();
        // Get last few chars from the filename as identifier
        let id: String = filename
            .chars()
            .rev()
            .take(6)
            .collect::<String>()
            .chars()
            .rev()
            .collect();
        let title_message = format!(
            "Measurement at size {} with {} timeslices, id: ...{}",
            parameters.size, parameters.timeslices, id
        );
        display_bar.set_style(ProgressStyle::with_template("({prefix}) {msg}").unwrap());
        display_bar.set_message(title_message);
        display_bar.set_prefix(" ");

        // Setup progress bars in error output
        let burnin_bar = multiprogress.add(ProgressBar::new(parameters.burnin as u64));
        burnin_bar.set_style(
            ProgressStyle::with_template(
                "{spinner:.yellow} [{bar:40.green/cyan}] {msg:^25} {elapsed_precise}  ETA: {eta}",
            )
            .unwrap()
            .progress_chars("#>-"),
        );
        let measurement_bar = multiprogress.add(ProgressBar::new(parameters.measurements as u64));
        let measurement_char_length = format!("{:}", parameters.measurements).chars().count();
        measurement_bar.set_style(
            ProgressStyle::with_template(&format!(
                "{{spinner:.yellow}} [{{pos:>{0:}}}/{{len:{0:}}}] [{{bar:{1:}.green/cyan}}] {{msg:^25}} {{elapsed_precise}}  ETA: {{eta}}",
                measurement_char_length, 40 - 2*measurement_char_length - 4
            ))
            .unwrap()
            .progress_chars("#>-"),
        );

        display_bar.tick();

        ProgressBars {
            burnin_bar,
            measurement_bar,
            display_bar,
        }
    }
}
