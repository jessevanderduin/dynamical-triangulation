# Dynamical Triangulation

Project in development for 2D Causal Dynamical Triangulation simulations.

This project is part of a Master Research project in Quantum Gravity at Radboud University in Nijmegen, supervised by [_prof. dr. Renate Loll_](https://www.hef.ru.nl/~rloll/Web/title/title.html).

## Features

- Efficient collection datatypes (these may be of more general interest, see [Docs](https://jessevanderduin.gitlab.io/dynamical-triangulation/cdt/))
- Dynamic and static size *causal dynamical triangulation*s
- Markov-chain Monte Carlo sampling of 2D CDT (dynamic and static size)
- Observable measurement
  - Volume profiles
  - Vertex order distribution
  - Distance profile (sphere volume over distance, for Hausdorff dimension measurement)
  * Average sphere distance
  * Average sphere distance correlation

* Different graph types for dealing with finite size effects
  - Cylindrical graph
  - Stacked cylindrical graph
  - Tiled planar graph
* ✖ Rich Command Line Interface
* Configuration by configuration file
* Basic mesh creation for visualization

## TODO

List of features that I would still like to be implemented in rough order of priority

- [x] **Measure wrapping violations**
- [ ] **Perform large measurements**
- [ ] Reintroduce CLI on top of configuration file
- [x] Allow input of parameter ranges
- [ ] **Profile and optimize**
- [ ] Read/write triangulation to file
- [ ] Visualization mesh edge length minimization (using argmin and kiss3d)
- [x] Optimize ASD calculations
  - [x] BFS from smallest sphere
  - [x] Marking with array with label positions
  - [x] Using custom collections structs
- [ ] Measure overlap of spheres not sphere with origins

### Refactoring

- [x] Refactor `Label` code
- [ ] Make argument parsing and model running more clear
- [ ] Clean-up double triangulation implementations
- [ ] Clean-up observable measurement mess
- [ ] Clean up old Graph implementations
- [ ] Clean up old observables
