use std::{thread, time};

use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
use rand::{thread_rng, Rng};

const MEASUREMENTS: usize = 10;
const STEPS: usize = 100;
const DELTA: u64 = 20;

fn main() {
    let mut rng = thread_rng();
    let mut obj = RandObj::initialize();
    let mut handle: Option<thread::JoinHandle<()>> = None;

    let progress_bars = MultiProgress::new();

    let simulation_bar =
        progress_bars.add(ProgressBar::new((MEASUREMENTS * STEPS).try_into().unwrap()));
    simulation_bar.set_style(
        ProgressStyle::with_template("{spinner} [{bar:40.cyan/green}] {elapsed_precise} {eta}")
            .unwrap()
            .progress_chars("#>-"),
    );

    let measurement_bar = progress_bars.add(ProgressBar::new((MEASUREMENTS).try_into().unwrap()));
    let measurement_char_length = format!("{:}", MEASUREMENTS).chars().count();
    measurement_bar.set_style(
        ProgressStyle::with_template(&format!(
            "{{spinner}} [{{pos:>{0:}}}/{{len:{0:}}}] {{msg}}",
            measurement_char_length
        ))
        .unwrap(),
    );

    progress_bars.println("Starting...").unwrap();
    simulation_bar.tick();
    measurement_bar.tick();

    measurement_bar.suspend(|| println!("Testing printing 1"));
    // println!("Testing printing 2");
    // eprintln!("Testing error printing 1");
    // eprintln!("Testing error printing 2");

    for _ in 0..MEASUREMENTS {
        for _ in 0..STEPS {
            simulation_bar.inc(1);
            thread::sleep(time::Duration::from_millis(2 * DELTA));
            obj.0 += rng.gen_range(-1.0..=1.0)
        }

        if let Some(handle) = handle {
            if !handle.is_finished() {
                println!(
                    "Warning! The calculation of the observable takes \
                        longer than the simulation cycle"
                );
            }
            handle.join().unwrap();
        };

        let obs = obj.clone();
        let measurement_bar = measurement_bar.clone();
        handle = Some(thread::spawn(move || {
            do_measurement(&obs, &measurement_bar)
        }));
    }

    simulation_bar.finish();

    if let Some(handle) = handle {
        handle.join().unwrap();
    };
    measurement_bar.finish();

    eprintln!("Finished measurements!")
}

fn do_measurement(obs: &RandObj, measurement_bar: &ProgressBar) {
    // Simulation slow observable calculation
    measurement_bar.set_message("Performing measurement...");

    for _ in 0..STEPS {
        thread::sleep(time::Duration::from_millis(DELTA));
        measurement_bar.tick();
    }

    measurement_bar.set_message(format!("Result: {:?}", obs));
    measurement_bar.inc(1);
}

// Pretend this object is not type Copy
#[derive(Debug, Clone)]
struct RandObj(f64);

impl RandObj {
    fn initialize() -> Self {
        RandObj(0.0f64)
    }
}
