//! This crate contains code for performing a numerical simulation of
//! Causal Dynamical Triangulation (CDT), using Markov Chain Monte Carlo methods.
//! 
//! For storing these [`Triangulation`](triangulation::Triangulation)s effectively
//! custom collection data structures have been designed, which could be useful
//! for applications different from CDT.
//! For these check out the [`collections`] module.

pub mod collections;
pub mod triangulation;
pub mod exporting;
