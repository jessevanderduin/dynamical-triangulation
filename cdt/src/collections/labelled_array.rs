//! Module describing the type specific label and array indexed by this label

use std::fmt::{Debug, Display};
use std::hash::Hash;
use std::marker::PhantomData;
use std::ops::{Index, IndexMut};
use std::{iter, slice};

use rand::Rng;
use serde::{Deserialize, Serialize};

/// Generic `Label` that marks it will label data of type T
#[derive(Serialize, Deserialize)]
pub struct Label<T>(usize, #[serde(skip)] PhantomData<T>);

impl<T> Display for Label<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}
impl<T> Debug for Label<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Label({})", self.0)
    }
}
impl<T> Hash for Label<T> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.0.hash(state)
    }
}
impl<T> Copy for Label<T> {}
impl<T> Clone for Label<T> {
    fn clone(&self) -> Self {
        Label(self.0, PhantomData)
    }
}
impl<T> PartialEq for Label<T> {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}
impl<T> Eq for Label<T> {}
impl<T> PartialOrd for Label<T> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.0.partial_cmp(&other.0)
    }
}
impl<T> Ord for Label<T> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.0.cmp(&other.0)
    }
}

impl<T> From<Label<T>> for usize {
    fn from(label: Label<T>) -> Self {
        label.0
    }
}
impl<T> From<usize> for Label<T> {
    fn from(index: usize) -> Self {
        Label(index, PhantomData)
    }
}
impl<T> From<Label<Option<T>>> for Label<T> {
    fn from(label: Label<Option<T>>) -> Self {
        Label(label.0, PhantomData)
    }
}
impl<T> From<Label<T>> for Label<Option<T>> {
    fn from(label: Label<T>) -> Self {
        Label(label.0, PhantomData)
    }
}

impl<T> Label<T> {
    /// Construct a new `Label` starting at some first element
    pub fn initial() -> Self {
        Label(0, PhantomData)
    }
}

impl<T> Label<T> {
    /// Returns the succesor `Label` of itself
    pub fn succ(&self) -> Self {
        Label(self.0 + 1, PhantomData)
    }
    /// Advances the `Label` to it's succesor
    pub fn advance(&mut self) {
        self.0 += 1
    }
}

/// Structure for storing a constant size list of items
/// indexed by [`Label`](crate::collections::Label).
/// This is supposed to be used a constant size variant of
/// [`LabelledList`](crate::collections::LabelledList)
#[derive(Clone)]
pub struct LabelledArray<T, L: ?Sized = Label<T>>(Box<[T]>, PhantomData<L>);

impl<T: Debug, L> Debug for LabelledArray<T, L> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.0)
    }
}

impl<T, L: Into<usize>> Index<L> for LabelledArray<T, L> {
    type Output = T;
    fn index(&self, index: L) -> &Self::Output {
        &self.0[index.into()]
    }
}

impl<T> Index<Label<T>> for LabelledArray<Option<T>> {
    type Output = Option<T>;
    fn index(&self, index: Label<T>) -> &Self::Output {
        &self.0[usize::from(index)]
    }
}

impl<T, L: Into<usize>> IndexMut<L> for LabelledArray<T, L> {
    fn index_mut(&mut self, index: L) -> &mut Self::Output {
        &mut self.0[index.into()]
    }
}

impl<T> IndexMut<Label<T>> for LabelledArray<Option<T>> {
    fn index_mut(&mut self, index: Label<T>) -> &mut Self::Output {
        &mut self.0[usize::from(index)]
    }
}

impl<T, L> From<Vec<T>> for LabelledArray<T, L> {
    fn from(list: Vec<T>) -> Self {
        LabelledArray(list.into_boxed_slice(), PhantomData)
    }
}

impl<T, L> LabelledArray<T, L> {
    /// Returns length of the array
    pub fn len(&self) -> usize {
        self.0.len()
    }

    /// Returns whether the [[LabelledArray]] is empty,
    /// e.g. contains no elements
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    /// Returns iterator over the items in the [`LabelledArray`]
    pub fn iter(&self) -> Iter<T> {
        Iter {
            iter: self.0.iter(),
        }
    }
}

impl<T, L: Into<usize>> LabelledArray<T, L> {
    /// Returns whether the array has an item at the given `label`
    pub fn has(&self, label: L) -> bool {
        self.len() > label.into()
    }
}

impl<T, L: From<usize>> LabelledArray<T, L> {
    /// Returns the last label in the [LabelledArray]
    pub fn last_label(&self) -> L {
        (self.len() - 1).into()
    }

    /// Returns iterator of the `Label`s in the array.
    pub fn labels(&self) -> impl Iterator<Item = L> {
        (0..self.len()).into_iter().map(|i| i.into())
    }

    /// Returns iterator over the tuples `(label, item)` in the [`LabelledArray`]
    pub fn enumerate(&self) -> impl Iterator<Item = (L, &T)> {
        self.iter().enumerate().map(|(i, item)| (i.into(), item))
    }

    /// Map `LabelledArray` to other `LabelledArray`
    pub fn map<U, F, M>(&self, f: F) -> LabelledArray<U, M>
    where
        F: FnMut(&T) -> U,
    {
        let list: Vec<U> = self.0.iter().map(f).collect();
        LabelledArray(list.into_boxed_slice(), PhantomData)
    }

    /// Map `LabelledArray` to other `LabelledArray`, consuming the original
    pub fn map_into<U, F, M>(self, f: F) -> LabelledArray<U, M>
    where
        F: FnMut(T) -> U,
    {
        let list: Vec<U> = self.0.into_vec().into_iter().map(f).collect();
        LabelledArray(list.into_boxed_slice(), PhantomData)
    }
}

impl<T, L: From<usize> + Into<usize>> LabelledArray<T, L> {
    /// Sample an item from the [`LabelledArray`]
    pub fn sample<'a, R: Rng + ?Sized>(&'a self, rng: &mut R) -> &'a T {
        &self[self.sample_label(rng)]
    }

    /// Sample a label present in the [`LabelledArray`]
    pub fn sample_label<R: Rng + ?Sized>(&self, rng: &mut R) -> L {
        L::from(rng.gen_range(0..self.0.len()))
    }
}

impl<T, L> LabelledArray<T, L> {
    /// Create empty `LabelledArray`
    pub fn empty() -> Self {
        LabelledArray(Box::new([]), PhantomData)
    }
    /// Create new `LabelledArray` filled with `filler`
    pub fn fill(filler: impl FnMut() -> T, len: usize) -> Self {
        let list: Vec<T> = iter::repeat_with(filler).take(len).collect();
        LabelledArray(list.into_boxed_slice(), PhantomData)
    }
}

/// Iterator over the [`LabelledArray`]
pub struct Iter<'a, T: 'a> {
    iter: slice::Iter<'a, T>,
}

impl<'a, T: 'a> Iterator for Iter<'a, T> {
    type Item = &'a T;
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn create_array() {
        let test = vec![1, 2, 3, 4, 5];
        let array: LabelledArray<_> = test.into();
        println!("{:?}", array.0)
    }

    #[test]
    fn option_array() {
        let mut array: LabelledArray<Option<f64>> = LabelledArray::fill(|| None, 50);
        for label in array.labels() {
            array[label] = Some(2.3)
        }
        let _ = array.map_into::<_, _, Label<Option<f64>>>(|item| item.unwrap());
    }
}
