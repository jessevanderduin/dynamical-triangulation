//! Old labelled_array with old Label

use super::Label;
use std::ops::{Index, IndexMut};

/// Structure for storing a constant size list of items
/// indexed by [`Label`](crate::collections::Label).
/// This is supposed to be used a constant size variant of
/// [`LabelledList`](crate::collections::LabelledList)
#[derive(Debug, Clone)]
pub struct LabelledArray<T>(Box<[T]>);

impl<T> Index<Label> for LabelledArray<T> {
    type Output = T;
    fn index(&self, index: Label) -> &Self::Output {
        &self.0[usize::from(index)]
    }
}

impl<T> IndexMut<Label> for LabelledArray<T> {
    fn index_mut(&mut self, index: Label) -> &mut Self::Output {
        &mut self.0[usize::from(index)]
    }
}

impl<T> From<Vec<T>> for LabelledArray<T> {
    fn from(list: Vec<T>) -> Self {
        LabelledArray(list.into_boxed_slice())
    }
}

impl<T> LabelledArray<T> {
    /// Return length of the array
    pub fn len(&self) -> usize {
        self.0.len()
    }

    /// Returns whether array is empty
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    /// Return iterator of the `Label`s in the array.
    pub fn labels(&self) -> impl Iterator<Item = Label> {
        (0..self.len()).into_iter().map(|i| i.into())
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn create_array() {
        let test = vec![1, 2, 3, 4, 5];
        let array: LabelledArray<_> = test.into();
        println!("{:?}", array.0)
    }
}
