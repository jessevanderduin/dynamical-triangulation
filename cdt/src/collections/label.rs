use std::fmt::{self, Display};

use serde::{Deserialize, Serialize};


/// `Label` used for indexing [`LabelledList`](super::LabelledList) and other collections
/// 
/// This `Label` is simply a wrapper over the built-in indexing type `usize`.
/// This is used to clearify to the programmer that the `Label` is a specific type of index,
/// used in this specific application such that it doesn't get confused with other `usize`s.
/// This is not at all necessary, but can be preferable to some programmers.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct Label(pub usize);

impl Display for Label {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl From<Label> for usize {
    fn from(label: Label) -> Self {
        label.0
    }
}

impl From<usize> for Label {
    fn from(idx: usize) -> Self {
        Label(idx)
    }
}
