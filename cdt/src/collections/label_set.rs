use super::TypedLabel as Label;
use rand::Rng;
use std::fmt;
use std::ops::IndexMut;
use std::{fmt::Display, ops::Index};

const FACTOR_MAX_SIZE: usize = 2; // Factor of the size they List gets when created from existing list

#[derive(Debug, Clone, Copy)]
enum Element<T> {
    // An enum representing the possible elements of LabelArray and IndexArray (Empty or some Index(T))
    Empty,
    Index(T),
}

#[derive(Debug, Clone)]
struct LabelArray<T>(
    // Array which should be longer than there will be elements needed in Pool,
    // It will hold all Label in consecutative manner, after which there are only Empty elements,
    // This array will be indexed by the indices from IndexArray.
    // This is an abstraction over an array, which is boxed to include it on the heap.
    Box<[Element<Label<T>>]>,
);

#[derive(Debug, Clone)]
struct IndexArray(
    // Array which should be longer than there will be elements needed in Pool,
    // It will hold all indices (usize) at the index given by the label the indices point to;
    // This means there can be holes of Empty inbetween the indices;
    // This array will be indexed by the Label from LabelArray.
    // This is an abstraction over an array, which is boxed to include it on the heap.
    Box<[Element<usize>]>,
);

/// A collection type for storing an unordered set of unique [`Label`](super::label::Label)s with fast `contain`, `add`, `remove`, and `sample`.
///
/// `LabelSet` is an implementation of a collection data structure that stores unique `Label`s without indexing, so should be used as an unordered set.
/// It includes methods which can do `Label` containment checks, addition, removal, and sampling of a random `Label` from the set in O(1) complexity,
/// meaning the exectution time of these operations is independent of the amount of elements stored in the set.
/// The generic type argument is used to mark what type of items are being labelled, this does not
/// matter interally but is used for bookkeeping to not confuse labels marking different types of
/// objects.
///
/// This data structure is heavily inspired by the "Bag" collection of _J. Brunekreef_ and _A. Görlich_ as
/// included in the Causal Dynamical Triangulation project [https://github.com/JorenB/2d-cdt](https://github.com/JorenB/2d-cdt).
///
/// #### When to use
/// This collection is designed to be useful when storing a set of unique (so any `Label` can only appear once in the set) `Label`s
/// which do not need to have an ordering; and fast containment checks, addition, removal, and/or sampling of `Label`s is required.
/// Currently it is required to know the largest `Label` which may need to be included in the set,
/// but this condition could possibly be relaxed if necessary.
///
/// And although all operations will still be fast, this collection is very memory inefficient if the
/// largest `Label` is much larger than the total amount of `Label`s in the set.
/// So this collection is best used for a reasonably "dense" set of `Label`s, meaning that the `Label`s stored
/// in the set is a major portion of all possible `Label`s.
#[derive(Debug, Clone)]
pub struct LabelSet<T> {
    // A collection type designed to have O(1) containment checks, addition, removal, and sampling
    // Designed to hold Label, but would work with anything that can be mapped to usize
    // This collection can only hold unique items, so addition and removal come with automatic contain checks.
    labels: LabelArray<T>, // Array holding `Label`s
    indices: IndexArray,   // Array holding indices for `labels`
    length: usize, // Amount of active elements in labels, equivalently the index of the first inactive element
}

// Pool creation
impl<T> LabelArray<T> {
    fn new(length: usize) -> LabelArray<T> {
        LabelArray(vec![Element::Empty; length].into_boxed_slice())
    }
}

impl IndexArray {
    fn new(length: usize) -> IndexArray {
        IndexArray(vec![Element::Empty; length].into_boxed_slice())
    }
}

impl<T> LabelSet<T> {
    /// Construct a new empty `LabelSet`.
    ///
    /// The set can include `Label`s up to `maximum_length`, so `maximum_length`
    /// should be set to the maximum `Label` that could be included in the set.
    pub fn new(maximum_length: usize) -> LabelSet<T> {
        LabelSet {
            labels: LabelArray::new(maximum_length),
            indices: IndexArray::new(maximum_length),
            length: 0,
        }
    }
}

impl<T> From<Element<T>> for Option<T> {
    fn from(elem: Element<T>) -> Self {
        match elem {
            Element::Index(i) => Some(i),
            Element::Empty => None,
        }
    }
}

// Indexing implementation
impl<T> Index<usize> for LabelArray<T> {
    // Allows for indexing of LabelArray using usize indices
    type Output = Element<Label<T>>;
    fn index(&self, index: usize) -> &Self::Output {
        &self.0[index]
    }
}

impl<T> IndexMut<usize> for LabelArray<T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.0[index]
    }
}

impl<T> Index<Label<T>> for IndexArray {
    // Allows for indexing of IndexArray using Label indices
    type Output = Element<usize>;
    fn index(&self, index: Label<T>) -> &Self::Output {
        &self.0[usize::from(index)]
    }
}

impl<T> IndexMut<Label<T>> for IndexArray {
    fn index_mut(&mut self, index: Label<T>) -> &mut Self::Output {
        &mut self.0[usize::from(index)]
    }
}

// Pool methods
impl<T> LabelSet<T> {
    /// Return the amount of `Label`s in the `LabelSet`
    pub fn len(&self) -> usize {
        self.length
    }

    /// Return wether the [[LabelSet]] is empty
    pub fn is_empty(&self) -> bool {
        self.length == 0
    }

    /// Returns whether `LabelSet` contains `Label`
    pub fn contains(&self, label: Label<T>) -> bool {
        // Simply checks if element is Empty or not
        match self.indices[label] {
            Element::Empty => false,
            Element::Index(_) => true,
        }
    }

    /// Add a `Label` to the `LabelSet`, returning whether
    /// `LabelSet` previously contained that `Label`.
    ///
    /// So `true` if the `label` was already present, so nothing has changed;
    /// or `false` if the `label` was not present, so now the `label` has been added.
    pub fn add(&mut self, label: Label<T>) -> bool {
        // Check whether set contains label.
        // If not add it by including an index at the last position pointing to that label,
        // and adding a label link to that index, after which the last position is incremented.
        match self.indices[label] {
            Element::Index(_) => true,
            Element::Empty => {
                self.add_checkless(label);
                false
            }
        }
    }

    /// Add a `Label` to the `LabelSet`, when already knowing it is active.
    /// NOTE: Do **not** use this when uncertain if `LabelSet` contains the `Label`,
    /// in this case use [`add()`](LabelSet::add()).
    pub fn add_checkless(&mut self, label: Label<T>) {
        self.indices[label] = Element::Index(self.length);
        self.labels[self.length] = Element::Index(label);
        self.length += 1;
    }

    /// Removes `label` from `LabelSet`, returnign whether `LabelSet` previously contained `label`.
    /// So `true` if the `label` was already present, so the label has been removed;
    /// or `false` if the `label` was not present, so nothing has changed.
    pub fn remove(&mut self, label: Label<T>) -> bool {
        // Check whether `label` is already present
        // If it is remove it from the `indices` list,and let the last `label` in
        // the `labels` list to this position to fill the hole; correctly linking the indices.
        // The last position is then decremented.
        match self.indices[label] {
            Element::Empty => false,
            Element::Index(i) => {
                self.remove_checkless(i, label);
                true
            }
        }
    }

    /// Remove a `Label` from the `LabelSet`, when knowing it is active
    /// and at which `index`.
    /// NOTE: If it is unknown whether `LabelSet` contains `label` and/or
    /// at what `index` use [`remove()`](LabelSet::remove()).
    pub fn remove_checkless(&mut self, index: usize, label: Label<T>) {
        self.length -= 1;

        let last = self.labels[self.length];
        self.labels[index] = last; // Place last element on removed element's spot
        if let Element::Index(last_label) = last {
            // Update the index of the last element
            self.indices[last_label] = Element::Index(index);
        } else {
            panic!("Something went wrong, somehow the last element is empty.");
        }
        self.labels[self.length] = Element::Empty; // Clear last element's spot
        self.indices[label] = Element::Empty; // Clear the index of the removed element
    }

    /// Toggles the status of an label in the `LabelSet`.
    ///
    /// This means that the `Label` will get added to the `LabelSet` if is was not present,
    /// or removed from the set is it was already present.
    pub fn toggle(&mut self, label: Label<T>) {
        match self.indices[label] {
            Element::Index(i) => self.remove_checkless(i, label),
            Element::Empty => self.add_checkless(label),
        };
    }

    /// Sample a random `Label` from the `LabelSet` using _random number generator_ `rng`
    /// (see [`rand::Rng`] for more details on how to create a _random number generator_).
    pub fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Label<T> {
        if let Element::Index(label) = self.labels[rng.gen_range(0..self.length)] {
            label
        } else {
            panic!("Something went wrong somehow there was a hole in the labels list")
        }
    }
}

impl<T> Extend<Label<T>> for LabelSet<T> {
    fn extend<U: IntoIterator<Item = Label<T>>>(&mut self, iter: U) {
        for label in iter {
            self.add(label);
        }
    }
}

// Added iterator struct for easy debugging and comparison
impl<'a, T> LabelSet<T> {
    /// Return an iterator over the `Label`s in the `LabelSet`
    pub fn iter(&'a self) -> Box<dyn Iterator<Item = Label<T>> + 'a> {
        Box::new((0..self.length).filter_map(|i| self.labels[i].into()))
    }
}

// All Display implementations, only use for visualisation
impl<T: Display> Display for Element<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Element::Empty => write!(f, "X"),
            Element::Index(i) => write!(f, "{:}", i),
        }
    }
}

/// Display trait for printing of the `LabelSet`.
impl<T> Display for LabelSet<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.length == 0 {
            return write!(f, "[ ]");
        }

        let mut labels = self.iter();
        match labels.next() {
            None => return write!(f, "[ ]"),
            Some(label) => write!(f, "[{}", label)?,
        }
        for label in labels {
            write!(f, " {}", label)?
        }
        write!(f, "]")
    }
}

/// Create a `LabelSet` from other collections of `Label`s.
///
/// A `LabelSet` can be created from any other collection, as long as they can be iterated over.
/// Important here is that the maximum size of the `LabelSet` created in this way will be set to be
/// `FACTOR_MAX_SIZE` times the highest `Label` given in the collection.
/// This choice is quite arbitrary (at time of writing `FACTOR_MAX_SIZE = 2`) and works well
/// for some applications, but may not be suitable for others.
/// There is unfortunatly no clean way using the default _Rust_ `from()` and still allowing
/// the user to choose the length of the Set.
/// However this could be implemented in other ways, and may be done in the future.
impl<T, I> From<I> for LabelSet<T>
where
    I: IntoIterator<Item = Label<T>>,
{
    fn from(iterable: I) -> Self {
        let labels: Vec<Label<T>> = iterable.into_iter().collect();
        let max_label = labels
            .iter()
            .map(|label| usize::from(*label))
            .max()
            .expect("No maximal value could be determined from given list");
        let mut set = LabelSet::new(FACTOR_MAX_SIZE * max_label);

        for label in labels {
            set.add(label);
        }
        set
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand_xoshiro::rand_core::SeedableRng;
    use rand_xoshiro::Xoshiro256StarStar;

    /// Assert if given `set` represents the labels given in `list`
    fn assert_set<T>(set: &LabelSet<T>, list: &[usize]) {
        // Compare each active item in the set with those in list
        if list.is_empty() && set.is_empty() {
            return;
        }

        assert_eq!(
            list.len(),
            set.len(),
            "LabelSet does not represent {:?}",
            list
        );

        let test_set = LabelSet::from(list.iter().map(|&i| Label::from(i)));
        assert_eq!(
            test_set.len(),
            list.len(),
            "Given list contains duplicate Labels"
        );

        for label in test_set.iter() {
            assert!(set.contains(label), "LabelSet does not contain {}", label);
        }
    }

    #[test]
    fn set_creation() {
        let set = LabelSet::<usize>::new(50);
        assert_set(&set, &[]);
    }

    #[test]
    fn set_addition() {
        let mut set = LabelSet::<usize>::new(50);
        set.add(Label::from(23));
        set.add(Label::from(3));
        set.add(Label::from(8));
        set.add(Label::from(16));
        set.add(Label::from(21));
        set.add(Label::from(8));
        assert_set(&set, &[23, 3, 8, 16, 21]);
        assert_eq!(set.len(), 5)
    }

    #[test]
    fn set_contains() {
        let mut set = LabelSet::<usize>::new(50);
        set.add(Label::from(23));
        set.add(Label::from(3));
        set.add(Label::from(8));
        set.add(Label::from(16));
        set.add(Label::from(21));
        set.add(Label::from(8));
        assert!(set.contains(Label::from(8)));
        assert!(set.contains(Label::from(16)));
        assert!(set.contains(Label::from(23)));
    }

    #[test]
    fn set_remove() {
        let mut set = LabelSet::<usize>::new(50);
        set.add(Label::from(23));
        set.add(Label::from(3));
        set.add(Label::from(8));
        set.add(Label::from(16));
        set.add(Label::from(21));
        assert_set(&set, &[23, 3, 8, 16, 21]);
        set.remove(Label::from(3));
        assert_set(&set, &[23, 21, 8, 16]);
        assert!(!set.contains(Label::from(3)));
        set.add(Label::from(3));
        assert_set(&set, &[23, 21, 8, 16, 3]);
        assert!(set.contains(Label::from(3)));
    }

    #[test]
    fn set_sample() {
        let mut set = LabelSet::<usize>::new(50);
        set.add(Label::from(23));
        set.add(Label::from(3));
        set.add(Label::from(8));
        set.add(Label::from(16));
        set.add(Label::from(21));

        // Use reliable reproducable rng to check that sampling works
        let mut rng = Xoshiro256StarStar::seed_from_u64(5);
        assert_eq!(set.sample(&mut rng), Label::from(3));
        assert_eq!(set.sample(&mut rng), Label::from(16));
        assert_eq!(set.sample(&mut rng), Label::from(16));
        assert_eq!(set.sample(&mut rng), Label::from(21));
        assert_eq!(set.sample(&mut rng), Label::from(8));
        assert_eq!(set.sample(&mut rng), Label::from(8));
        assert_eq!(set.sample(&mut rng), Label::from(21));
    }

    #[test]
    fn set_from() {
        let set1 = LabelSet::<usize>::from([
            Label::from(1),
            Label::from(7),
            Label::from(4),
            Label::from(2),
            Label::from(7),
        ]);
        // println!("{}: {}", set1.len(), set1);
        assert_set(&set1, &[1, 7, 4, 2]);

        let set2 = LabelSet::<usize>::from(vec![
            Label::from(1),
            Label::from(9),
            Label::from(4),
            Label::from(2),
            Label::from(7),
        ]);
        // println!("{}: {}", set2.len(), set2);
        assert_set(&set2, &[1, 7, 4, 2, 9]);
    }

    #[test]
    fn set_toggle() {
        let mut set = LabelSet::<usize>::new(50);
        set.add(Label::from(23));
        set.add(Label::from(3));
        set.add(Label::from(8));
        set.add(Label::from(16));
        set.add(Label::from(21));
        assert_set(&set, &[23, 3, 8, 16, 21]);
        set.toggle(Label::from(7));
        assert_set(&set, &[23, 3, 8, 16, 21, 7]);
        set.toggle(Label::from(23));
        assert_set(&set, &[3, 8, 16, 21, 7]);
    }
}
