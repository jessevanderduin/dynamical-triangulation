//! The `collections` module contains all custom collection types
//! used in the Dynamical Triangulation simulations.
//! Most notably: [`LabelSet`] and [`LabelledList`]

#![warn(missing_docs)]

mod label;
mod label_set;
mod labelled_list;
pub mod labelled_array;
pub mod labelled_array_legacy;

// Make central collection data structures directly visibly in collections module
pub use label::Label;
pub use label_set::LabelSet;
pub use labelled_list::LabelledList;
pub use labelled_array::LabelledArray;
pub use labelled_array::Label as TypedLabel;
