//! Module containting all observables used to make measurements on the `Triangulation`
//! For details on the `Triangulation` and its manipulation, see module [`triangulation`](crate::triangulation).

mod embedding;
mod graph;
mod length_profile;
mod toroidal_graph;
mod vertex_order;

use serde::Serialize;
use std::fmt::Display;

pub use embedding::EmbeddedTriangulation;
pub use graph::{dual_graph, vertex_graph, DistanceProfile, Graph};
pub use length_profile::{length_profile, length_profile_origin, LengthProfile};
pub use toroidal_graph::averaged_fields::{Field, VertexDegree};
pub use toroidal_graph::correlation::{
    area_correlation_profile, asd_center_correlation, 
    asd_correlation, asd_correlation_profile, field_correlation_profile, osd_correlation_profile,
    random_correlation_profile, ASDCorrelationProfile, CorrelationProfile,
};
pub use toroidal_graph::curvature::{
    average_sphere_distance, average_sphere_distance_center, average_sphere_distance_profile,
    overlapping_sphere_distance, overlapping_sphere_distance_profile, AverageSphereDistance,
    AverageSphereDistanceProfile,
};
pub use toroidal_graph::{
    diffusion, dot, Graph as GraphTrait, StackedCylinderGraph, TiledPlanarGraph, ToroidalGraph,
};
pub use vertex_order::{
    vertex_order_distribution, VertexOrderDistribution, MAX_EXPECTED_VERTEX_ORDER, MIN_VERTEX_ORDER,
};

/// Trait implemented by all measureable observables
///
/// This trait ensures the Observables can be displayed in
/// terminal and written to files.
pub trait Observable: Display + Serialize {}
