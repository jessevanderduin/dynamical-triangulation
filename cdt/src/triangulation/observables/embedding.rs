//! Module implementing possible embeddings of graphs/triangulations
//! for visualisation purposes.

use std::collections::HashMap;
use std::fmt::Debug;
use std::io::Write;
use std::ops::{AddAssign, Mul, Sub};

use argmin::core::{CostFunction, Error, Executor, Gradient, State};
use argmin::solver::goldensectionsearch::GoldenSectionSearch;
use argmin::solver::gradientdescent::SteepestDescent;
use argmin::solver::linesearch::MoreThuenteLineSearch;

use crate::collections::{LabelledArray, TypedLabel as Label};
use crate::triangulation::full_triangulation::Vertex;
use crate::triangulation::FullTriangulation;
use std::f64::consts::{FRAC_PI_3, PI, TAU};

use super::toroidal_graph::{Patch2D, TiledPlanarLabel};
use super::{overlapping_sphere_distance, TiledPlanarGraph};

const TIMESLICE_ROTATION_MAX_ITERATIONS: u64 = 50;
const TIMESLICE_ROTATION_TOLERANCE: f64 = 1e-3;
const EDGEDISTANCE_MINIMALIZATION_MAX_ITERATION: u64 = 1000;

#[derive(Clone, Copy)]
pub struct Point3D {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl AddAssign for Point3D {
    fn add_assign(&mut self, rhs: Self) {
        self.x += rhs.x;
        self.y += rhs.y;
        self.z += rhs.z;
    }
}

impl Sub for Point3D {
    type Output = Point3D;

    fn sub(self, rhs: Self) -> Self::Output {
        Point3D {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
        }
    }
}

impl Mul<Point3D> for f64 {
    type Output = Point3D;

    fn mul(self, mut rhs: Point3D) -> Self::Output {
        rhs.x *= self;
        rhs.y *= self;
        rhs.z *= self;
        rhs
    }
}

impl Default for Point3D {
    fn default() -> Self {
        Point3D {
            x: 0.0,
            y: 0.0,
            z: 0.0,
        }
    }
}

impl Point3D {
    /// Returns the square norm of a vector represented by the 3D point,
    /// here Euclidean square length of the vector/distance of point from origin.
    #[inline]
    fn norm2(&self) -> f64 {
        self.x * self.x + self.y * self.y + self.z * self.z
    }

    /// Returns the norm of the vector; the distance of the point from origin.
    fn norm(&self) -> f64 {
        self.norm2().sqrt()
    }
}

impl Point3D {
    fn new(x: f64, y: f64, z: f64) -> Self {
        Point3D { x, y, z }
    }
}

impl Debug for Point3D {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Point3D({}, {}, {})", self.x, self.y, self.z)
    }
}

/// Struct for holding the embedding of a [`FullTriangulation`]
pub struct EmbeddedTriangulation<'a> {
    triangulation: &'a FullTriangulation,
    coordinates: LabelledArray<Point3D, Label<Vertex>>,
    vertex_times: LabelledArray<usize, Label<Vertex>>,
    final_remapping: HashMap<Label<Vertex>, Label<Vertex>>,
}

impl EmbeddedTriangulation<'_> {
    /// Returns a list of all the vertex coordinates in the
    /// [EmbeddedTriangulation] as `(x, y, z)`
    pub fn get_coordinates(&self) -> impl Iterator<Item = &Point3D> {
        self.coordinates.iter()
    }

    pub fn get_vertex_times(&self) -> impl Iterator<Item = &usize> {
        self.vertex_times.iter()
    }

    pub fn get_max_vertex_time(&self) -> usize {
        self.triangulation.time_len()
    }

    fn remap_label(&self, label: Label<Vertex>) -> Label<Vertex> {
        if let Some(&new_label) = self.final_remapping.get(&label) {
            new_label
        } else {
            label
        }
    }

    /// Returns a list of all the vertices of the triangles
    /// in the [EmbeddedTriangulation] as (index0, index1, index2),
    /// where each index corresponds to the position of the vertex in
    /// the list returned by [`get_coordinates()`](EmbeddedTriangulation::get_coordinates())
    pub fn get_vertex_triangles(&self) -> (Vec<(usize, usize, usize)>, Vec<usize>) {
        let tlast = self.triangulation.time_len() - 1;
        self.triangulation
            .triangles
            .iter()
            .map(|triangle| {
                let t = triangle.time();
                (
                    if t < tlast {
                        let vertices = triangle.vertices();
                        (vertices.0.into(), vertices.1.into(), vertices.2.into())
                    } else {
                        let vertices = triangle.vertices();
                        (
                            self.remap_label(vertices.0).into(),
                            self.remap_label(vertices.1).into(),
                            self.remap_label(vertices.2).into(),
                        )
                    },
                    t,
                )
            })
            .unzip()
    }

    pub fn get_osd_field(&self, delta: usize) -> Vec<f64> {
        let graph = TiledPlanarGraph::from_stacked_graph(
            self.triangulation.construct_stacked_vertex_graph(),
        );
        let vertex_count = self.triangulation.vertices.len();
        self.coordinates
            .labels()
            .map(|mut clabel| {
                if usize::from(clabel) >= vertex_count {
                    clabel = *self
                        .final_remapping
                        .iter()
                        .find(|&(_, &label)| label == clabel)
                        .expect("Vertex coordinate label could not be found in remapping")
                        .0
                }
                let label = TiledPlanarLabel {
                    label: clabel.into(),
                    patch: Patch2D::origin(),
                };
                overlapping_sphere_distance(&graph, label, delta).asd
            })
            .collect()
    }
}

struct SliceDistance<'a> {
    t: usize,
    triangulation: &'a FullTriangulation,
    coordinates: &'a LabelledArray<Option<Point3D>, Label<Vertex>>,
    length: usize,
}

impl<'a> SliceDistance<'a> {
    fn new(
        t: usize,
        triangulation: &'a FullTriangulation,
        coordinates: &'a LabelledArray<Option<Point3D>, Label<Vertex>>,
    ) -> Self {
        SliceDistance {
            t,
            triangulation,
            coordinates,
            length: triangulation.timeslice_length(t),
        }
    }
}

impl CostFunction for SliceDistance<'_> {
    type Param = f64;
    type Output = f64;

    fn cost(&self, offset: &Self::Param) -> Result<Self::Output, Error> {
        let length = self.length;
        let mut sqr_distance_sum = 0.0;
        for (i, vertex) in self.triangulation.iter_time(self.t).enumerate() {
            let point = EmbeddedTriangulation::compute_point_offset(self.t, i, length, *offset);
            for &nbr in self.triangulation.get_vertex(vertex).down.iter() {
                let nbr_point = self.coordinates[nbr]
                    .unwrap_or_else(|| panic!("Previous timeslice coordinates at {} are not yet completely set for this timeslice {}.", self.triangulation.get_vertex(nbr).time(), self.t));
                sqr_distance_sum += (point - nbr_point).norm2();
            }
        }
        Ok(sqr_distance_sum)
    }
}

impl<'a> EmbeddedTriangulation<'a> {
    /// Returns the coordinates of a vertex identified by (t, i)
    fn compute_point(t: usize, i: usize, length: usize) -> Point3D {
        Self::compute_point_offset(t, i, length, 0.0)
    }

    /// Generates an simple cylindrical embedding with independent timeslices
    /// kept in equidistantly spaced planes.
    pub fn new_cylindrical_basic(triangulation: &'a FullTriangulation) -> Self {
        // Add the first timeslice of vertices again to split them up for visualisation
        let extra_vertices = triangulation.timeslice_length(0);
        let mut last_label = triangulation.vertices.last_label();
        let mut remapping = HashMap::with_capacity(extra_vertices);
        for vertex in triangulation.iter_time(0) {
            last_label.advance();
            remapping.insert(vertex, last_label);
        }

        // Create empty list of coordinates
        let mut coordinates: LabelledArray<Option<Point3D>, Label<Vertex>> =
            LabelledArray::fill(|| None, triangulation.vertex_count() + extra_vertices);
        // Create empty list of vertex times
        let mut vtimes: LabelledArray<Option<usize>, Label<Vertex>> =
            LabelledArray::fill(|| None, triangulation.vertex_count() + extra_vertices);

        // Compute the coordinates of all vertices by walking through the timeslices
        let tmax = triangulation.time_len();
        for t in 0..tmax {
            let length = triangulation.timeslice_length(t);
            for (i, vertex) in triangulation.iter_time(t).enumerate() {
                coordinates[vertex] = Some(Self::compute_point(t, i, length));
                vtimes[vertex] = Some(t)
            }
        }

        // Compute the coordinates of the extra added timeslice
        for (i, vertex) in triangulation.iter_time(0).enumerate() {
            let new_label = *remapping
                .get(&vertex)
                .expect("Somehow the remapping was not completely set");
            coordinates[new_label] = Some(Self::compute_point(tmax, i, extra_vertices));
            vtimes[new_label] = Some(tmax);
        }

        EmbeddedTriangulation {
            triangulation,
            final_remapping: remapping,
            coordinates: coordinates
                .map(|element| element.expect("A vertex coordinate was not set")),
            vertex_times: vtimes.map(|element| element.expect("A vertex time was not set")),
        }
    }

    /// Returns the coordinates of a vertex identified by (t, i),
    /// where all points get an offset angle of size `offset`
    fn compute_point_offset(t: usize, i: usize, length: usize, offset: f64) -> Point3D {
        let t = t as f64;
        let i = i as f64;
        let length = length as f64;

        let th = (i as f64 / length) * TAU + offset;
        // Choose `r` such that distance between vertices is 1.0
        let r = 0.5 * length / (length * (PI / length).sin());

        Point3D::new(
            r * th.cos(),
            r * th.sin(),
            // Choose `z` such that timelike distance is 1 for flat triangulation
            t as f64 * FRAC_PI_3.sin(),
        )
    }
    /// Finds the best offset for the timeslice given by `t`
    fn find_best_offset(
        t: usize,
        triangulation: &'a FullTriangulation,
        coordinates: &LabelledArray<Option<Point3D>, Label<Vertex>>,
    ) -> f64 {
        let slice_distance = SliceDistance::new(t, triangulation, coordinates);

        let length = slice_distance.length;
        let half_edge_angle = (0.5 / length as f64) * TAU;

        let mut min_distance = f64::MAX;
        let mut best_starting_offset = 0.0;
        for i in 0..length {
            let offset = (i as f64 / length as f64) * TAU + half_edge_angle;
            let distance = slice_distance.cost(&offset).unwrap();
            if distance < min_distance {
                min_distance = distance;
                best_starting_offset = offset;
            }
        }

        let gs = GoldenSectionSearch::new(
            best_starting_offset - half_edge_angle,
            best_starting_offset + half_edge_angle,
        )
        .unwrap()
        .with_tolerance(TIMESLICE_ROTATION_TOLERANCE)
        .unwrap();
        let res = Executor::new(slice_distance, gs)
            .configure(|state| {
                state
                    .param(best_starting_offset)
                    .max_iters(TIMESLICE_ROTATION_MAX_ITERATIONS)
            })
            .run()
            .expect("Could not find minimum.");
        *res.state().get_best_param().unwrap()
    }

    /// Generates an simple cylindrical embedding with timeslices
    /// kept in equidistantly spaced planes.
    /// The rotation of each timeslice is chosen such that the total square distance
    /// of the timelike links is closest to `1.0`
    pub fn new_cylindrical_minimal(triangulation: &'a FullTriangulation) -> Self {
        // Add the first timeslice of vertices again to split them up for visualisation
        let extra_vertices = triangulation.timeslice_length(0);
        let mut last_label = triangulation.vertices.last_label();
        let mut remapping = HashMap::with_capacity(extra_vertices);
        for vertex in triangulation.iter_time(0) {
            last_label.advance();
            remapping.insert(vertex, last_label);
        }

        // Create empty list of coordinates
        let mut coordinates: LabelledArray<Option<Point3D>, Label<Vertex>> =
            LabelledArray::fill(|| None, triangulation.vertex_count() + extra_vertices);
        // Create empty list of vertex times
        let mut vtimes: LabelledArray<Option<usize>, Label<Vertex>> =
            LabelledArray::fill(|| None, triangulation.vertex_count() + extra_vertices);

        // Compute the coordinates of the first timeslice
        let length = triangulation.timeslice_length(0);
        for (i, vertex) in triangulation.iter_time(0).enumerate() {
            coordinates[vertex] = Some(Self::compute_point(0, i, length));
            vtimes[vertex] = Some(0);
        }

        // Compute the coordinates of all vertices by walking through the rest of the timeslices
        let tmax = triangulation.time_len();
        for t in 1..tmax {
            let best_offset = Self::find_best_offset(t, triangulation, &coordinates);
            let length = triangulation.timeslice_length(t);
            for (i, vertex) in triangulation.iter_time(t).enumerate() {
                coordinates[vertex] = Some(Self::compute_point_offset(t, i, length, best_offset));
                vtimes[vertex] = Some(t);
            }
        }

        // Compute the coordinates of the extra added timeslice
        let best_offset = Self::find_best_offset(0, triangulation, &coordinates);
        let length = extra_vertices;
        for (i, vertex) in triangulation.iter_time(0).enumerate() {
            let new_label = *remapping
                .get(&vertex)
                .expect("Somehow the remapping was not completely set");
            coordinates[new_label] = Some(Self::compute_point_offset(tmax, i, length, best_offset));
            vtimes[new_label] = Some(tmax);
        }

        EmbeddedTriangulation {
            triangulation,
            final_remapping: remapping,
            coordinates: coordinates
                .map(|element| element.expect("A vertex coordinate was not set")),
            vertex_times: vtimes.map(|element| element.expect("A vertex time was not set")),
        }
    }

    /// Generate a cylindrical embedding where all connecting edges are minimized,
    /// such that the shape of the triangles should be close to equilateral; reflecting
    /// the true geometry of the 2D triangulation more closely
    pub fn new_cylindrical_warped(triangulation: &'a FullTriangulation) -> Self {
        let start_embedding = Self::new_cylindrical_minimal(triangulation);
        let embedding_points = start_embedding.coordinates.len();
        let start_coordinates: Vec<f64> = start_embedding
            .get_coordinates()
            .flat_map(|point| [point.x, point.y, point.z])
            .collect();
        let (triangles, _) = start_embedding.get_vertex_triangles();

        let problem = EdgeDistance {
            triangles,
            power: 2,
        };
        eprintln!("Start cost: {}", problem.cost(&start_coordinates).unwrap());
        std::io::stderr().flush().unwrap();
        eprintln!();
        eprintln!();
        std::io::stderr().flush().unwrap();
        let ls = MoreThuenteLineSearch::new();
        let solver = SteepestDescent::new(ls);
        let res = Executor::new(problem, solver)
            .configure(|state| {
                state
                    .param(start_coordinates)
                    .target_cost(0.0)
                    .max_iters(EDGEDISTANCE_MINIMALIZATION_MAX_ITERATION)
            })
            .run()
            .expect("Could not find minimum.");
        eprintln!("Best cost: {}", res.state.get_best_cost());
        std::io::stderr().flush().unwrap();
        eprintln!();
        eprintln!();
        std::io::stderr().flush().unwrap();
        let flattened_coordinates = res.state.get_best_param().unwrap();
        let mut coordinates: Vec<Point3D> = Vec::with_capacity(embedding_points);
        let mut temp_point = Point3D::default();
        for (i, &value) in flattened_coordinates.iter().enumerate() {
            match i % 3 {
                0 => temp_point.x = value,
                1 => temp_point.y = value,
                2 => {
                    temp_point.z = value;
                    coordinates.push(temp_point);
                }
                _ => panic!("i % 3 somehow gave a result that was not 0, 1 or 2"),
            }
        }

        EmbeddedTriangulation {
            coordinates: LabelledArray::from(coordinates),
            ..start_embedding
        }
    }
}

struct Coordinates<'a> {
    flattened: &'a Vec<f64>,
}

impl<'a> Coordinates<'a> {
    fn from_flattened_vec(flattened: &'a Vec<f64>) -> Self {
        Self { flattened }
    }

    fn len(&self) -> usize {
        self.flattened.len() / 3
    }

    fn get(&self, index: usize) -> Point3D {
        let i = 3 * index;
        Point3D {
            x: self.flattened[i],
            y: self.flattened[i + 1],
            z: self.flattened[i + 2],
        }
    }
}

struct CoordinatesInto {
    flattened: Vec<f64>,
    sum: Point3D,
}

impl CoordinatesInto {
    fn zero(size: usize) -> Self {
        let flattened = std::iter::repeat(0.0).take(3 * size).collect();
        Self {
            flattened,
            sum: Point3D::default(),
        }
    }

    fn flatten(self) -> Vec<f64> {
        self.flattened
    }

    fn add(&mut self, index: usize, point: Point3D) {
        let i = 3 * index;
        self.sum += point;
        self.flattened[i] += point.x;
        self.flattened[i + 1] += point.y;
        self.flattened[i + 2] += point.z;
    }

    fn in_cm_frame(&mut self) {
        for (i, coor) in self.flattened.iter_mut().enumerate() {
            match i % 3 {
                0 => *coor -= self.sum.x,
                1 => *coor -= self.sum.y,
                2 => *coor -= self.sum.z,
                _ => panic!("i % 3 gave a non 0, 1, 2 integer"),
            }
        }
    }
}

struct EdgeDistance {
    triangles: Vec<(usize, usize, usize)>,
    power: i32,
}

impl CostFunction for EdgeDistance {
    type Param = Vec<f64>;
    type Output = f64;

    fn cost(&self, param: &Self::Param) -> Result<Self::Output, Error> {
        let nu = self.power;
        let coordinates = Coordinates::from_flattened_vec(param);
        let mut cost = 0.0;
        for triangle in &self.triangles {
            let c0 = coordinates.get(triangle.0);
            let c1 = coordinates.get(triangle.1);
            let c2 = coordinates.get(triangle.2);
            cost += ((c0 - c1).norm() - 1.0).powi(nu);
            cost += ((c1 - c2).norm() - 1.0).powi(nu);
            cost += ((c2 - c0).norm() - 1.0).powi(nu);
        }
        Ok(0.5 * cost)
    }
}
impl Gradient for EdgeDistance {
    type Param = Vec<f64>;
    type Gradient = Vec<f64>;

    fn gradient(&self, param: &Self::Param) -> Result<Self::Gradient, Error> {
        let nu = self.power;
        let coordinates = Coordinates::from_flattened_vec(param);
        let mut gradient = CoordinatesInto::zero(coordinates.len());
        for triangle in self.triangles.iter() {
            let c0 = coordinates.get(triangle.0);
            let c1 = coordinates.get(triangle.1);
            let c2 = coordinates.get(triangle.2);
            let d0 = c0 - c1;
            gradient.add(triangle.0, (d0.norm() - 1.0).powi(nu - 1) / d0.norm() * d0);
            let d1 = c1 - c2;
            gradient.add(triangle.1, (d1.norm() - 1.0).powi(nu - 1) / d1.norm() * d1);
            let d2 = c2 - c0;
            gradient.add(triangle.2, (d2.norm() - 1.0).powi(nu - 1) / d2.norm() * d2);
        }
        gradient.in_cm_frame();
        Ok(gradient.flatten())
    }
}
