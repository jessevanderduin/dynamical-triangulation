use crate::collections::{LabelledArray, TypedLabel as Label};

use super::{
    Graph, Node, NodeLabel, SemiLinkLabel, TiledPlanarGraph, TiledPlanarLabel, ToroidalGraph,
};

const DEFAULT_PATCHVEC_CAPACITY: usize = 1;

pub trait FieldCollection<F, G: Graph + ?Sized> {
    fn get(&mut self, node: G::Label) -> &F {
        self.get_mut(node)
    }

    fn get_mut(&mut self, node: G::Label) -> &mut F;

    fn set(&mut self, node: G::Label, value: F) {
        *self.get_mut(node) = value;
    }
}

// TODO: Generic `F` is not ideal here, as this means only
// a single field type can be constructed at a time for a
// given `FieldGraph<F>`
pub trait FieldGraph<F>: Graph {
    type Field: FieldCollection<F, Self>;

    fn new_field(&self) -> Self::Field;
}

pub struct ToroidalGraphField<F>(LabelledArray<F, Label<Node>>);

impl<F> FieldCollection<F, ToroidalGraph> for ToroidalGraphField<F> {
    fn get_mut(&mut self, node: <ToroidalGraph as Graph>::Label) -> &mut F {
        &mut self.0[node.get_label()]
    }
}

impl<F: Default> FieldGraph<F> for ToroidalGraph {
    type Field = ToroidalGraphField<F>;

    fn new_field(&self) -> Self::Field {
        ToroidalGraphField(LabelledArray::fill(F::default, self.len()))
    }
}

/// Map `i8`s to `usize` in such a way that:
/// (0, -1, 1, -2, 2, -3, ...) -> (0, 1, 2, 3, 4, 5, 6, ...)
/// This is useful to map indices for lists in such a way that a list
/// growing in both directions now only needs to grow in one; allowing
/// the list to remain small if typical elements are close to 0.
fn i8usize_central_mapping(int: i8) -> usize {
    let double_int = int << 1; // Equivalent to: 2 * int
    let transformed = if int.is_negative() {
        !(double_int) // Equivalent to: -double_int - 1
    } else {
        double_int
    };
    transformed as usize
}

struct PatchVec<T>(Vec<T>);

impl<T> Default for PatchVec<T> {
    fn default() -> Self {
        PatchVec(Vec::with_capacity(DEFAULT_PATCHVEC_CAPACITY))
    }
}

impl<T: Default> PatchVec<T> {
    fn get_mut(&mut self, index: i8) -> &mut T {
        let index_mapped = i8usize_central_mapping(index);
        if index_mapped >= self.0.len() {
            self.0.resize_with(index_mapped + 1, Default::default)
        }
        &mut self.0[index_mapped]
    }
}

pub struct TiledGraphField<F>(LabelledArray<PatchVec<PatchVec<F>>, Label<Node>>);

impl<F: Default> FieldCollection<F, TiledPlanarGraph> for TiledGraphField<F> {
    fn get_mut(&mut self, node: TiledPlanarLabel) -> &mut F {
        let label = node.label;
        let patch = node.patch;
        self.0[label].get_mut(patch.0).get_mut(patch.1)
    }
}

impl<F: Default> FieldGraph<F> for TiledPlanarGraph {
    type Field = TiledGraphField<F>;

    fn new_field(&self) -> Self::Field {
        TiledGraphField(LabelledArray::fill(PatchVec::default, self.len()))
    }
}

/// Compute the heat kernel of the graph, specifically the return probabilities
/// of diffusion for different diffusion times (starting at sigma=0)
pub fn diffusion<G: FieldGraph<Option<f64>>>(
    graph: &G,
    origin: G::Label,
    max_diffusion_time: usize,
    diffusion_coefficient: f64,
) -> Vec<f64> {
    // List storing the return probabilities
    let mut diffusion_profile: Vec<f64> = Vec::with_capacity(max_diffusion_time);
    // Create starting probability of 1.0 at origin and 0.0 elsewhere
    let mut probability_field = graph.new_field();
    probability_field.set(origin, Some(1.0));
    // Set return probability at diffusion time zero (sigma = 0)
    diffusion_profile.push(
        probability_field
            .get(origin)
            .expect("Origin probability was not set"),
    );

    // Empty probability field for storing updated diffusion probabilities
    let mut new_probs = graph.new_field();
    for sigma in 1..max_diffusion_time {
        // Diffuse probabilities (from origin outward)
        for node in graph.iter_breadth_first(origin) {
            // If distance is larger or equal to the diffusions steps that
            // already took place changes cannot have reached that area yet.
            // Also if the distance is larger than the diffusion steps that
            // will take place, changes cannot reach the origin anymore.
            // In both case the resulting return probabilities will not be
            // affected so stop diffusion for this step.
            // TODO: Check that this restriction really doesn't miss any relevant points
            if node.distance >= sigma || node.distance > max_diffusion_time - sigma {
                break;
            }
            // Get probability
            let label = node.label;
            let prob = if let Some(prob) = probability_field.get(label) {
                prob
            } else {
                // TODO: This should never occur, as the distance checks beforehand
                // guarantee no irrelevant points are looped over. Check this explicitly
                continue;
            };
            // Diffuse probability to neighbours and itself ($D*prob$ divided over
            // neighbours and $(1-D)*prob$ to itself)
            *new_probs.get_mut(label).get_or_insert(0.0) += (1.0 - diffusion_coefficient) * prob;
            let nbr_prob = diffusion_coefficient * prob / graph.get_vertex_order(label) as f64;
            for neighbour in get_neighbours(graph, label) {
                *new_probs.get_mut(neighbour).get_or_insert(0.0) += nbr_prob;
            }
        }
        // Set new found probability field
        probability_field = new_probs;
        // Reset temporary probabilities
        new_probs = graph.new_field();
        // Set return probability for current `sigma`
        diffusion_profile.push(
            probability_field
                .get(origin)
                .expect("Origin probability was not set"),
        );
    }

    diffusion_profile
}

/// Returns an iterator over the neighbours of the [`Node`] marked by `label`
fn get_neighbours<G: Graph>(graph: &G, label: G::Label) -> impl Iterator<Item = G::Label> + '_ {
    graph
        .get_links(label)
        .map(move |semilink| graph.get_neighbour(SemiLinkLabel::new(label, semilink)))
}
