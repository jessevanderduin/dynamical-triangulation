//! This module is used to generate DOT graph formats from my custom Graph types

use std::{collections::HashSet, fmt::Debug};

use super::{
    Graph, Patch2D, SemiLink, SemiLinkLabel, TiledPlanarBound, TiledPlanarGraph, TiledPlanarLabel,
};

/// Main structure for exporting to DOT graph format
pub struct Dot<L> {
    directed: bool,
    edges: HashSet<(L, L)>,
    marked: HashSet<L>,
}

impl Dot<TiledPlanarLabel> {
    /// Construct a new DOT struct from a [`TiledPlanarGraph`]
    pub fn from_tiled_graph(graph: TiledPlanarGraph) -> Self {
        let mut edges: HashSet<(TiledPlanarLabel, TiledPlanarLabel)> =
            HashSet::with_capacity(4 * graph.len());
        let mut marked: HashSet<TiledPlanarLabel> = HashSet::with_capacity(graph.len());
        for label in graph.nodes.labels() {
            let tiled_label = TiledPlanarLabel {
                patch: Patch2D::origin(),
                label,
            };
            for semilink in graph.get_links(tiled_label) {
                let semilabel = SemiLinkLabel {
                    node: tiled_label,
                    semilink,
                };
                if let Some(&bound) = graph.bounds.get(semilabel) {
                    match bound {
                        TiledPlanarBound::SW => {
                            marked.insert(tiled_label);
                            marked.insert(TiledPlanarLabel {
                                patch: Patch2D(1, 1),
                                label,
                            });
                            continue;
                        }
                        TiledPlanarBound::S => {
                            marked.insert(tiled_label);
                            marked.insert(TiledPlanarLabel {
                                patch: Patch2D(1, 0),
                                label,
                            });
                            continue;
                        }
                        TiledPlanarBound::W => {
                            marked.insert(tiled_label);
                            marked.insert(TiledPlanarLabel {
                                patch: Patch2D(0, 1),
                                label,
                            });
                            continue;
                        }
                        _ => (),
                    }
                }
                match semilink {
                    SemiLink::Up(_) | SemiLink::Right => {
                        edges.insert((tiled_label, graph.get_neighbour(semilabel)));
                    }
                    SemiLink::Down(_) | SemiLink::Left => {
                        edges.insert((graph.get_neighbour(semilabel), tiled_label));
                    }
                }
            }
        }

        for &bound_label in marked.iter() {
            for nbr_link in graph.get_links(bound_label) {
                let nbr_label = graph.get_neighbour(SemiLinkLabel {
                    node: bound_label,
                    semilink: nbr_link,
                });
                if marked.contains(&nbr_label) {
                    match nbr_link {
                        SemiLink::Up(_) | SemiLink::Right => {
                            edges.insert((bound_label, nbr_label));
                        }
                        SemiLink::Down(_) | SemiLink::Left => {
                            edges.insert((nbr_label, bound_label));
                        }
                    }
                }
            }
        }

        Dot {
            directed: true,
            edges,
            marked,
        }
    }
}

impl<L> Dot<L> {
    fn graph_name(&self) -> &str {
        if self.directed {
            "digraph"
        } else {
            "graph"
        }
    }

    fn connector_symbol(&self) -> &str {
        if self.directed {
            "->"
        } else {
            "--"
        }
    }
}

impl<L: Debug> Debug for Dot<L> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "{} {{", self.graph_name())?;
        writeln!(
            f,
            "node[shape=circle, label=\"\", fixedsize=true, height=0.1]"
        )?;
        for node in self.marked.iter() {
            writeln!(f, "\"{:?}\" [color=red]", node)?;
        }
        for edge in self.edges.iter() {
            writeln!(
                f,
                "\"{:?}\" {} \"{:?}\";",
                edge.0,
                self.connector_symbol(),
                edge.1
            )?;
        }

        writeln!(f, "}}")
    }
}
