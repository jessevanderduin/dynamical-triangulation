use crate::triangulation::observables::Observable;
use serde::Serialize;
use std::fmt::Display;

/// Distance profile of a graph giving the amount of vertices
/// at a given graph distance from some origin.
#[derive(Serialize)]
pub struct DistanceProfile {
    profile: Box<[usize]>,
}

impl DistanceProfile {
    /// Returns the maximal distance that is measured in the [`DistanceProfile`]
    pub fn max_distance(&self) -> usize {
        self.profile.len()
    }

    /// Returns the amount of nodes at the given `distance`
    pub fn get_count(&self, distance: usize) -> usize {
        self.profile[distance]
    }
}

impl DistanceProfile {
    /// Create a [`DistanceProfile`] from a [`Vec<usize>`]
    /// 
    /// Note that this assumes that the position in the vector
    /// is the distance for that count.
    pub fn from_vec(mut list: Vec<usize>, max_distance: usize) -> Self {
        let padding_length = max_distance + 1 - list.len();
        list.append(&mut vec![0; padding_length]);
        DistanceProfile { profile: list.into_boxed_slice() }
    }

    /// Check if two [`DistanceProfile`]s are the same up to some distance `upto`
    pub fn equality_up_to(left: &Self, right: &Self, upto: usize) -> bool {
        for i in 0..upto {
            if left.profile[i] != right.profile[i] {
                return false;
            }
        }
        true
    }
}

impl Observable for DistanceProfile {}

const DEFAULT_MAXIMUM_DISPLAY_LENGTH_DISTANCE_PROFILE: usize = 30;

impl Display for DistanceProfile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let max_length = match f.width() {
            Some(width) => width,
            None => DEFAULT_MAXIMUM_DISPLAY_LENGTH_DISTANCE_PROFILE,
        };
        let max_count = *self
            .profile
            .iter()
            .max()
            .expect("Distance profile is empty") as f32;
        for (i, &count) in self.profile.iter().enumerate() {
            writeln!(
                f,
                "{:>2} ({:>3$}): {:=<4$}",
                i,
                count,
                "",
                max_count.log10() as usize + 1,
                ((count as f32) / max_count * (max_length as f32)).round() as usize
            )?;
        }
        Ok(())
    }
}
