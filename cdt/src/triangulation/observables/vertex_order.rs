//! This module contains implementaitons for making and viewing Vertex order measurements.
//! It could definitely use a refactoring

use serde::{ser::SerializeSeq, Serialize};

use crate::triangulation::{triangle::Orientation, FullTriangulation, Triangulation};
use std::{collections::HashMap, fmt::Display, ops::AddAssign};

use super::Observable;

/// Maximimally expected vertex order.
/// This is used to pre-allocate memory for the vertex orders,
/// if there turn out to be more vertex orders than this new
/// memory is allocated which may make it slightly slower.
///
/// This is also used to set the maximum vertex order that is
/// written to file when measured using serialization.
pub const MAX_EXPECTED_VERTEX_ORDER: usize = 30;
/// The vertex order cannot be lower than 4 by geometrical properties
pub const MIN_VERTEX_ORDER: usize = 4;

/// The minimal fraction that a vertex order needs to have to be displayed if formatted
const MIN_VERTEX_ORDER_FRACTION: f32 = 0.1;

/// Measure the vertex order distribution of a given `Triangulation`
pub fn vertex_order_distribution<'a>(
    triangulation: &'a impl Triangulation<'a>,
) -> VertexOrderDistribution {
    let mut dist = HashMap::with_capacity(MAX_EXPECTED_VERTEX_ORDER);
    for label in triangulation
        .labels()
        .filter(|&label| matches!(triangulation.get(label).orientation, Orientation::Up))
    {
        *dist
            .entry(VertexOrder(triangulation.vertex_order(label)))
            .or_insert(0) += 1;
    }
    VertexOrderDistribution { dist }
}

impl FullTriangulation {
    /// Return the [`VertexOrderDistribution`] of the [`FullTriangulation`]
    pub fn vertex_order_distribution(&self) -> VertexOrderDistribution {
        let mut dist = HashMap::with_capacity(MAX_EXPECTED_VERTEX_ORDER);
        for vertex in self.vertices.iter() {
            *dist.entry(VertexOrder(vertex.vertex_order())).or_insert(0) += 1;
        }
        VertexOrderDistribution { dist }
    }
}

/// Struct signifying the vertex order.
/// Implemented as a simple wrapper over an `usize`,
/// as vertex order can be a number from 4 up.
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, PartialOrd, Ord, Serialize)]
struct VertexOrder(usize);

/// Struct containting the distribtution of vertex orders in a HashMap
#[derive(Debug)]
pub struct VertexOrderDistribution {
    dist: HashMap<VertexOrder, u64>,
}

impl PartialEq for VertexOrderDistribution {
    fn eq(&self, other: &Self) -> bool {
        if self.dist.len() != other.dist.len() {
            return false;
        }

        for (vo, &count) in self.dist.iter() {
            if let Some(&other_count) = other.dist.get(vo) {
                if count == other_count {
                    continue;
                }
            }
            return false;
        }
        true
    }
}

impl AddAssign for VertexOrderDistribution {
    fn add_assign(&mut self, rhs: Self) {
        for (vo, count) in rhs.dist {
            *self.dist.entry(vo).or_insert(count) += count;
        }
    }
}

impl Display for VertexOrder {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl VertexOrderDistribution {
    fn total_count(&self) -> u64 {
        self.dist.values().sum()
    }
}

impl Observable for VertexOrderDistribution {}

impl Display for VertexOrderDistribution {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let max_count = match self.dist.values().max() {
            Some(&max) => max,
            None => return write!(f, "| "),
        }; // Find largest amount of a single vertex order

        let total = self.total_count(); // Total amount of vertices
        writeln!(f)?; // Start at newline

        // First sort the distribution on vertex orders
        let mut vertex_order_dist: Vec<(&VertexOrder, &u64)> = self.dist.iter().collect();
        vertex_order_dist.sort_by_key(|entry| entry.0);

        for (vo, &count) in vertex_order_dist {
            if let Some(max) = f.width() {
                let fraction = (100 * count) as f32 / total as f32;
                if fraction < MIN_VERTEX_ORDER_FRACTION {
                    continue;
                }
                let bin_size = ((count * max as u64) as f32 / max_count as f32).round() as usize;
                writeln!(f, "|{:>2} ({:>4.1}%): {:=<3$}", vo, fraction, "", bin_size)?;
            } else {
                writeln!(f, "|{:>2} ({}): {:=<3$}", vo, count, "", count as usize)?;
            }
        }
        Ok(())
    }
}

impl Serialize for VertexOrderDistribution {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut seq =
            serializer.serialize_seq(Some(MAX_EXPECTED_VERTEX_ORDER - MIN_VERTEX_ORDER + 1))?;
        for i in 4..MAX_EXPECTED_VERTEX_ORDER + 1 {
            seq.serialize_element(self.dist.get(&VertexOrder(i)).unwrap_or(&0))?;
        }
        seq.end()
    }
}
