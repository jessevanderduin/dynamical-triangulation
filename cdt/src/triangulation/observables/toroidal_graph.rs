use crate::collections::{LabelledArray, TypedLabel as Label};
use crate::triangulation::full_triangulation::{Triangle, Vertex};
use crate::triangulation::triangle::Orientation;
use crate::triangulation::FullTriangulation;
use bitvec::bits;
use bitvec::vec::BitVec;
use rand::Rng;
use serde::Serialize;
use std::cmp::Ordering;
use std::collections::{HashMap, VecDeque};
use std::fmt::{Debug, Display};
use std::hash::Hash;
use std::ops::Add;

use super::DistanceProfile;
use path::{Path, PathNode};

pub mod averaged_fields;
pub mod correlation;
pub mod curvature;
pub mod diffusion;
pub mod dot;

const EXPECTED_MAX_PATCH_TIME_EXTENSION: usize = 2;
const EXPECTED_MAX_PATCH_SPACE_EXTENSION: usize = 2;

/// General trait for [`Graph`] types, for generalising observables
/// for all graph types.
pub trait Graph {
    /// Type of the [`NodeLabel`] used for labelling/indexing the [`Graph`]
    type Label: NodeLabel;

    /// Returns the amount of nodes in the [`Graph`]
    fn len(&self) -> usize;

    /// Returns whether the graph is empty, e.g. contains no [[`Node`]]s
    fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Returns a reference to the [`Node`] at `label`
    fn get_node(&self, label: Self::Label) -> &Node;

    /// Returns the vertex degree, coordination number, vertex order
    fn get_vertex_order(&self, label: Self::Label) -> usize {
        let node = self.get_node(label);
        node.up_count() + node.down_count() + 2
    }

    /// Returns the neighbour the label points to
    fn get_neighbour(&self, label: SemiLinkLabel<Self::Label>) -> Self::Label;

    /// Returns the connected [`SemiLinkLabel`] of the given `semilabel`
    fn get_neighbour_semilink(
        &self,
        semilabel: SemiLinkLabel<Self::Label>,
    ) -> SemiLinkLabel<Self::Label> {
        let nbr_label = self.get_neighbour(semilabel);
        let semilink = match semilabel.semilink {
            SemiLink::Left => SemiLink::Right,
            SemiLink::Right => SemiLink::Left,
            SemiLink::Up(_) => {
                let nbr = self.get_node(nbr_label);
                let i = nbr.down.iter().position(|&label| label == semilabel.node.into()).expect("Neighbour has no link to source, this should never happen, the graph must be broken!");
                SemiLink::new_down(i)
            }
            SemiLink::Down(_) => {
                let nbr = self.get_node(nbr_label);
                let i = nbr.up.iter().position(|&label| label == semilabel.node.into()).expect("Neighbour has no link to source, this should never happen, the graph must be broken!");
                SemiLink::new_up(i)
            }
        };
        SemiLinkLabel {
            node: nbr_label,
            semilink,
        }
    }

    /// Returns an interator over the semilinks of the [`Node`] given by `label`
    fn get_links(&self, label: Self::Label) -> LinkIter {
        self.get_node(label).get_links()
    }

    /// Returns an iterator over the forward timelike and spacelike semilinks
    fn get_forward_links(&self, label: Self::Label) -> LinkIter {
        self.get_node(label).get_forward_links()
    }

    // /// Returns an iterator over the neigbours of
    // /// the [`Node`] marked by `label`
    // fn get_neighbours(&self, label: Self::Label) -> impl Iterator<Type = Self::Label> {
    //     self.get_links(label).map(|semilink| self.get_neighbour(SemiLinkLabel::new(label, semilink)))
    // }
    
    /// Returns an iterator over the neigbours of
    /// the [`Node`] marked by `label`
    // fn get_neighbours(&self, label: Self::Label) -> Neighbours<Self> {
    //     Neighbours::from_graph(self, label)
    // }

    // /// Returns an iterator over the neigbours in spacelike and foward
    // /// timelike directions (clockwise order)
    // fn get_forward_neighbours(&self, label: Self::Label) -> FowardNeighbours<Self> {
    //     FowardNeighbours::from_graph(self, label)
    // }

    /// Returns a breadth first traversal iterator
    fn iter_breadth_first(&self, origin: Self::Label) -> BreadthFirstSearch<Self> {
        BreadthFirstSearch::from_graph(self, origin)
    }

    /// Return a foward breadth first traversal iterator
    fn iter_forward_breadth_first(&self, origin: Self::Label) -> ForwardBreadthFirstSearch<Self> {
        ForwardBreadthFirstSearch::from_graph(self, origin)
    }

    /// Sample a node in the graph by label
    fn sample_node<R: Rng + ?Sized>(&self, rng: &mut R) -> Self::Label;

    /// Compute the distance profile of the graph from a given origin
    fn distance_profile(&self, origin: Self::Label, max_depth: usize) -> DistanceProfile {
        let mut profile = Vec::new();
        for node in self.iter_breadth_first(origin) {
            if node.distance > max_depth {
                break;
            }

            if profile.len() < node.distance + 1 {
                profile.push(0);
            }
            profile[node.distance] += 1;
        }
        DistanceProfile::from_vec(profile, max_depth)
    }

    /// Find a shortest path from `begin` to `end`
    fn shortest_path(&self, start: Self::Label, end: Self::Label) -> Path<Self::Label> {
        // Check if path of zero length
        if start == end {
            return Path::from_vec(vec![PathNode::Single { node: end }]);
        }

        // Perform breadth first search recording the path steps taken
        let mut visited: HashMap<Self::Label, Option<SemiLinkLabel<Self::Label>>> =
            HashMap::with_capacity(self.len());
        for node in self.iter_breadth_first(start) {
            visited.insert(node.label, node.source);
            // When reached the end, break
            if node.label == end {
                break;
            }
        }

        reconstruct_path(self, visited, end)
    }

    /// Find a shortest forward pointing path from `begin` to `end`
    fn shortest_forward_path(&self, start: Self::Label, end: Self::Label) -> Path<Self::Label> {
        // Check if path of zero length
        if start == end {
            return Path::from_vec(vec![PathNode::Single { node: end }]);
        }

        let mut visited = HashMap::with_capacity(self.len());
        for node in self.iter_forward_breadth_first(start) {
            visited.insert(node.label, node.source);
            if node.label == end {
                break;
            }
        }

        reconstruct_path(self, visited, end)
    }
}

/// Reconstruct path from visited HashMap and some end point
fn reconstruct_path<G: Graph + ?Sized>(
    graph: &'_ G,
    visited: HashMap<G::Label, Option<SemiLinkLabel<G::Label>>>,
    end: G::Label,
) -> Path<G::Label> {
    // This method could possibly be optimized as we know the length of the path
    let mut path = Vec::new();
    let mut source = *visited.get(&end).unwrap();
    path.push(PathNode::End {
        node: end,
        in_link: graph
            .get_neighbour_semilink(source.expect("Somehow the end has no source"))
            .semilink,
    });
    loop {
        let semilabel = source.unwrap();
        source = *visited.get(&semilabel.node).unwrap();
        if source.is_none() {
            path.push(PathNode::Begin {
                node: semilabel.node,
                out_link: semilabel.semilink,
            });
            break;
        }
        path.push(PathNode::Middle {
            node: semilabel.node,
            in_link: graph.get_neighbour_semilink(source.unwrap()).semilink,
            out_link: semilabel.semilink,
        })
    }

    Path::from_vec_reversed(path)
}

/// Node in graph containing all links to other nodes
///
/// Since this concerns a toroidal graph, one can distinguish
/// a time and space direction.
/// Convention is here chosen with `left` and `right` in space,
/// and `up` and `down` as forward and backward in time respectively.
/// The same could be represented with directed edges, but making use
/// of this additional structure for Graphs belonging to triangulations
/// helps.
#[derive(Debug, Clone)]
pub struct Node {
    // The links stored in the `Node`, should be ordered clockwise;
    // thus the links in `up` are from *left* to *right*; the links
    // in `down` are from *right* to *left*.
    // With this ordering one can easily identify a link connections
    // by origin's `Label<Node>` and some index (TODO)
    left: Label<Node>,
    up: Box<[Label<Node>]>,
    right: Label<Node>,
    down: Box<[Label<Node>]>,
}

impl Node {
    /// Returns the amount of forward time pointing links
    fn up_count(&self) -> usize {
        self.up.len()
    }

    /// Returns the amount of backward time pointing links
    fn down_count(&self) -> usize {
        self.down.len()
    }

    /// Returns an iterator over the [`SemiLink`]s of the [`Node`]
    fn get_links(&self) -> LinkIter {
        LinkIter::new(self)
    }

    fn get_links_from_to_exclusive(&self, from: SemiLink, to: SemiLink) -> LinkIter {
        LinkIter::from_to_exclusive(self, from, to)
    }

    /// Returns an iterator over the forward timelike and spacelike
    /// [`SemiLink`]s of the [`Node`]
    fn get_forward_links(&self) -> LinkIter {
        LinkIter::from_to_inclusive(self, SemiLink::Left, SemiLink::Right)
    }

    /// Return the [`Label`] of the [`Node`] the [`SemiLink`] is pointing to
    ///
    /// Note: this method `panic`s if the semilink does not exist.
    fn get_neighbour(&self, semilink: SemiLink) -> Label<Node> {
        match semilink {
            SemiLink::Left => self.left,
            SemiLink::Up(i) => self.up[i as usize],
            SemiLink::Right => self.right,
            SemiLink::Down(i) => self.down[i as usize],
        }
    }

    /// Returns the next [`SemiLink`] that is present in the [`Node`]
    fn next_semilink(&self, semilink: SemiLink) -> SemiLink {
        match semilink {
            SemiLink::Left => {
                if self.up_count() > 0 {
                    SemiLink::Up(0)
                } else {
                    SemiLink::Right
                }
            }
            SemiLink::Up(i) => {
                if self.up_count() > (i + 1) as usize {
                    SemiLink::Up(i + 1)
                } else {
                    SemiLink::Right
                }
            }
            SemiLink::Right => {
                if self.down_count() > 0 {
                    SemiLink::Down(0)
                } else {
                    SemiLink::Left
                }
            }
            SemiLink::Down(i) => {
                if self.down_count() > (i + 1) as usize {
                    SemiLink::Down(i + 1)
                } else {
                    SemiLink::Left
                }
            }
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Serialize)]
pub enum NodeLabelMatch {
    Equal = 2,
    DifferentPatch = 1,
    Different = 0,
}

impl NodeLabelMatch {
    /// Combine the [NodeLabelMatch] with other value only when the other value is
    /// 'more equal; so Different -> DifferentPatch -> Equal
    fn combine(self, other: Self) -> Self {
        self.max(other)
    }

    /// Update the [NodeLabelMatch] with other value only when the other value is
    /// 'more equal; so Different -> DifferentPatch -> Equal
    fn update(&mut self, other: Self) {
        *self = self.combine(other)
    }

    fn violation_string(&self) -> &'static str {
        match self {
            NodeLabelMatch::Equal => "🗷",
            NodeLabelMatch::DifferentPatch => "✘",
            NodeLabelMatch::Different => "🗸",
        }
    }
}

/// The PatchCollection trait is designed to allow for an efficient implementation
/// of storing the possible patches
pub trait PatchCollection {
    type Patch: Eq + Ord + Debug + Copy + Hash;

    /// Create a new empty [PatchCollection]
    fn empty() -> Self;

    /// Insert a patch in the [PatchCollection], returning `true` if it was inserted and not yet
    /// present and `false` it it was not inserted because the patch was already present.
    fn insert(&mut self, patch: Self::Patch) -> bool;
}

type LabelPatch<L> = <<L as NodeLabel>::PatchCollection as PatchCollection>::Patch;

pub trait NodeLabel: Into<Label<Node>> + Copy + Eq + Hash {
    type PatchCollection: PatchCollection;

    fn get_patch(&self) -> LabelPatch<Self>;

    fn get_label(&self) -> Label<Node> {
        (*self).into()
    }

    fn cmp_label(self, other: Self) -> NodeLabelMatch {
        let label: Label<Node> = self.get_label();
        if label == other.get_label() {
            if self.get_patch() == other.get_patch() {
                NodeLabelMatch::Equal
            } else {
                NodeLabelMatch::DifferentPatch
            }
        } else {
            NodeLabelMatch::Different
        }
    }
}

impl NodeLabel for Label<Node> {
    type PatchCollection = EmptyPatchCollection;
    fn get_patch(&self) -> LabelPatch<Self> {}
}

pub struct EmptyPatchCollection(bool);

impl PatchCollection for EmptyPatchCollection {
    type Patch = ();

    fn empty() -> Self {
        EmptyPatchCollection(false)
    }

    fn insert(&mut self, _: Self::Patch) -> bool {
        if self.0 {
            false
        } else {
            self.0 = true;
            true
        }
    }
}

/// Graph structure holding all connectivity information
/// of the Graph.
///
/// This Graph is a [`ToroidalGraph`] in the sense that
/// it can be embedded without crossing links on a *torus*.
/// This graph is used to analyse the structure of
/// triangulations of the torus.
/// Due to this additional structure links can be identified
/// to be time-like or space-like.
#[derive(Debug, Clone)]
pub struct ToroidalGraph {
    nodes: LabelledArray<Node>,
}

impl Graph for ToroidalGraph {
    type Label = Label<Node>;

    fn len(&self) -> usize {
        self.nodes.len()
    }

    fn get_node(&self, label: Label<Node>) -> &Node {
        &self.nodes[label]
    }

    fn get_neighbour(&self, label: SemiLinkLabel<Self::Label>) -> Self::Label {
        self.get_node(label.node).get_neighbour(label.semilink)
    }

    fn sample_node<R: Rng + ?Sized>(&self, rng: &mut R) -> Self::Label {
        self.nodes.sample_label(rng)
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct StackedCylinderLabel {
    patch: Patch1D,
    label: Label<Node>,
}

impl Debug for StackedCylinderLabel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({:}).{:?}", self.patch, self.label)
    }
}

impl NodeLabel for StackedCylinderLabel {
    type PatchCollection = PatchCollection1D;
    fn get_patch(&self) -> LabelPatch<Self> {
        self.patch
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Patch1D(i8);

impl Display for Patch1D {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:}", self.0)
    }
}

impl Add for Patch1D {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        Patch1D(self.0.add(rhs.0))
    }
}

fn i8usize_central_mapping(int: i8) -> usize {
    let double_int = int << 1; // Equivalent to: 2 * int
    let transformed = if int.is_negative() {
        !(double_int) // Equivalent to: -double_int - 1
    } else {
        double_int
    };
    transformed as usize
}

/// One-dimensional structure containing a list of patches marked by a single i8
pub struct PatchCollection1D {
    // Implementation is achieved by mapping `i8` to `usize` in such a way that:
    // (0, -1, 1, -2, 2, -3, ...) -> (0, 1, 2, 3, 4, 5, 6, ...)
    // This results in the internal vector being able to be small since patches will usually be
    // only -1, 0, or 1; and it will grow in both directions.
    list: Vec<bool>,
}

impl PatchCollection for PatchCollection1D {
    type Patch = Patch1D;

    fn empty() -> Self {
        let list = vec![false; 2 * EXPECTED_MAX_PATCH_TIME_EXTENSION + 1];
        Self { list }
    }

    fn insert(&mut self, patch: Self::Patch) -> bool {
        let index = i8usize_central_mapping(patch.0);

        // Resize the list is there are to little elements
        if index >= self.list.len() {
            self.list.resize(index + 1, false);
        }

        // Check if already `true`/patch already present -> return `false`
        if self.list[index] {
            false
        } else {
            // If not present -> make `true` and return `true`
            self.list[index] = true;
            true
        }
    }
}

impl From<StackedCylinderLabel> for Label<Node> {
    fn from(stacked_label: StackedCylinderLabel) -> Self {
        stacked_label.label
    }
}

#[derive(Debug, Clone, Copy)]
enum StackedCylinderBound {
    Up,
    Down,
}

/// A [`Graph`] struct that represents an infinitely repeating cylinder graph
/// in the temporal direction.
#[derive(Debug, Clone)]
pub struct StackedCylinderGraph {
    nodes: LabelledArray<Node>,
    bounds: LabelledArray<Bound<StackedCylinderBound>, Label<Node>>,
}

impl Graph for StackedCylinderGraph {
    type Label = StackedCylinderLabel;

    fn len(&self) -> usize {
        self.nodes.len()
    }

    fn get_node(&self, label: Self::Label) -> &Node {
        &self.nodes[label.into()]
    }

    fn get_neighbour(&self, label: SemiLinkLabel<Self::Label>) -> Self::Label {
        let new_patch = label.node.patch
            + if let Some(&bound) = self.bounds[label.node.into()].get(label.semilink) {
                match bound {
                    StackedCylinderBound::Up => Patch1D(1),
                    StackedCylinderBound::Down => Patch1D(-1),
                }
            } else {
                Patch1D(0)
            };
        let nbr_label = self.get_node(label.node).get_neighbour(label.semilink);
        StackedCylinderLabel {
            patch: new_patch,
            label: nbr_label,
        }
    }

    fn sample_node<R: Rng + ?Sized>(&self, rng: &mut R) -> Self::Label {
        StackedCylinderLabel {
            patch: Patch1D(0),
            label: self.nodes.sample_label(rng),
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct TiledPlanarLabel {
    pub patch: Patch2D,
    pub label: Label<Node>,
}

impl Debug for TiledPlanarLabel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({:},{:}).{:?}", self.patch.0, self.patch.1, self.label)
    }
}

impl NodeLabel for TiledPlanarLabel {
    type PatchCollection = PatchCollection2D;

    #[inline]
    fn get_patch(&self) -> LabelPatch<Self> {
        self.patch
    }

    #[inline]
    fn get_label(&self) -> Label<Node> {
        self.label
    }
}

impl From<TiledPlanarLabel> for Label<Node> {
    fn from(label: TiledPlanarLabel) -> Self {
        label.label
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum TiledPlanarBound {
    N,
    NE,
    E,
    SE,
    S,
    SW,
    W,
    NW,
}

/// A [`Graph`] struct that represent an infinitely repeating planar graph
///
/// This is equivalent to a toroidal graph.
#[derive(Debug, Clone)]
pub struct TiledPlanarGraph {
    nodes: LabelledArray<Node>,
    bounds: Bounds<TiledPlanarBound>,
}
impl Graph for TiledPlanarGraph {
    type Label = TiledPlanarLabel;

    fn len(&self) -> usize {
        self.nodes.len()
    }

    #[inline]
    fn get_node(&self, label: Self::Label) -> &Node {
        &self.nodes[label.label]
    }

    fn get_neighbour(&self, label: SemiLinkLabel<Self::Label>) -> Self::Label {
        let nbr_label = self.get_node(label.node).get_neighbour(label.semilink);
        let new_patch = if let Some(&bound) = self.bounds.get(label) {
            label.node.patch
                + match bound {
                    TiledPlanarBound::N => Patch2D(1, 0),
                    TiledPlanarBound::NE => Patch2D(1, 1),
                    TiledPlanarBound::E => Patch2D(0, 1),
                    TiledPlanarBound::SE => Patch2D(-1, 1),
                    TiledPlanarBound::S => Patch2D(-1, 0),
                    TiledPlanarBound::SW => Patch2D(-1, -1),
                    TiledPlanarBound::W => Patch2D(0, -1),
                    TiledPlanarBound::NW => Patch2D(1, -1),
                }
        } else {
            label.node.patch
        };
        TiledPlanarLabel {
            patch: new_patch,
            label: nbr_label,
        }
    }

    fn sample_node<R: Rng + ?Sized>(&self, rng: &mut R) -> Self::Label {
        TiledPlanarLabel {
            patch: Patch2D::origin(),
            label: self.nodes.sample_label(rng),
        }
    }
}

mod from_full_triangulation {
    use super::*;

    impl From<Label<Vertex>> for Label<Node> {
        fn from(label: Label<Vertex>) -> Self {
            let raw_label: usize = label.into();
            Label::from(raw_label)
        }
    }

    impl From<&Vertex> for Node {
        fn from(vertex: &Vertex) -> Self {
            let up: Vec<Label<Node>> = vertex.up.iter().map(|&label| label.into()).collect();
            let down: Vec<Label<Node>> = vertex.down.iter().map(|&label| label.into()).collect();
            Node {
                left: vertex.left.into(),
                up: up.into_boxed_slice(),
                right: vertex.right.into(),
                down: down.into_boxed_slice(),
            }
        }
    }

    impl From<Label<Triangle>> for Label<Node> {
        fn from(label: Label<Triangle>) -> Self {
            let raw_label: usize = label.into();
            Label::from(raw_label)
        }
    }

    impl From<&Triangle> for Node {
        fn from(triangle: &Triangle) -> Self {
            // Create connections, when pointing Up, link is downwards and visa versa
            let (up, down) = match triangle.orientation {
                Orientation::Up => (vec![], vec![triangle.center.into()]),
                Orientation::Down => (vec![triangle.center.into()], vec![]),
            };
            Node {
                left: triangle.left.into(),
                up: up.into_boxed_slice(),
                right: triangle.right.into(),
                down: down.into_boxed_slice(),
            }
        }
    }

    /// Implementation of constructing a [`ToroidalGraph`] from a [`FullTriangulation`]
    impl FullTriangulation {
        /// Construct a [`ToroidaGraph`] representing the vertex
        /// connections of the [`FullTriangulation`]
        pub fn construct_vertex_graph(&self) -> ToroidalGraph {
            let nodes = self.vertices.map(|vertex| vertex.into());
            ToroidalGraph { nodes }
        }

        /// Construct a [`ToroidaGraph`] representing the triangle
        /// connections of the [`FullTriangulation`]. Also called
        /// the dual graph of the vertex graph.
        pub fn construct_dual_graph(&self) -> ToroidalGraph {
            let nodes = self.triangles.map(|triangle| triangle.into());
            ToroidalGraph { nodes }
        }
    }

    /// Timeslice to use for construction of the StackedCylinderGraph,
    /// this is completely arbitrary and only changes where the cut is made.
    const CUTTING_TIMESLICE: usize = 0;

    impl FullTriangulation {
        /// Construct a [`StackedCylinderGraph`] representing the vertex
        /// connections of the [`FullTriangulation`]
        ///
        /// The [`StackedCylinderGraph`] represents the triangulation by cutting
        /// the triangulation open in time, and stacking it on top of oneother,
        /// creating an infinitly repeating graph in the time direction.
        pub fn construct_stacked_vertex_graph(&self) -> StackedCylinderGraph {
            // Convert vertices to nodes
            let nodes: LabelledArray<Node> = self.vertices.map(|vertex| vertex.into());
            let t_max = self.time_len();

            let mut bounds = LabelledArray::fill(Bound::new, self.vertex_count());

            for vertex in self.iter_time(CUTTING_TIMESLICE % t_max) {
                let node: Label<Node> = vertex.into();
                for i in 0..nodes[node].up_count() {
                    bounds[node].insert(SemiLink::new_up(i), StackedCylinderBound::Up);
                }
            }

            for vertex in self.iter_time((CUTTING_TIMESLICE + 1) % t_max) {
                let node: Label<Node> = vertex.into();
                for i in 0..nodes[node].down_count() {
                    bounds[node].insert(SemiLink::new_down(i), StackedCylinderBound::Down);
                }
            }

            StackedCylinderGraph { nodes, bounds }
        }

        /// Construct a [`StackedCylinderGraph`] representing the triangle
        /// connections of the [`FullTriangulation`]
        ///
        /// The [`StackedCylinderGraph`] represents the triangulation by cutting
        /// the triangulation open in time, and stacking it on top of oneother,
        /// creating an infinitly repeating graph in the time direction.
        pub fn construct_stacked_dual_graph(&self) -> StackedCylinderGraph {
            // Convert vertices to nodes
            let triangles = &self.triangles;
            let vertices = &self.vertices;
            let nodes: LabelledArray<Node> = triangles.map(|triangle| Node::from(triangle));
            let t_max = self.time_len();

            let mut bounds = LabelledArray::fill(Bound::new, self.size());
            for vertex_label in self.iter_time(CUTTING_TIMESLICE % t_max) {
                let up = vertices[vertex_label].triangle();
                bounds[up.into()].insert(SemiLink::new_down(0), StackedCylinderBound::Down);
                let down = triangles[up].center;
                bounds[down.into()].insert(SemiLink::new_up(0), StackedCylinderBound::Up);
            }

            StackedCylinderGraph { nodes, bounds }
        }
    }

    impl TiledPlanarGraph {
        /// Generate a [`TiledPlanarGraph`] from a [`StackedCylinderGraph`] by making an arbitrary vertical cut.
        pub fn from_stacked_graph(stacked_graph: StackedCylinderGraph) -> Self {
            let initial_label = Label::initial();
            let start_label = StackedCylinderLabel {
                patch: Patch1D(0),
                label: initial_label,
            };
            let end_label = StackedCylinderLabel {
                patch: Patch1D(1),
                label: initial_label,
            };

            let mut bounds = Bounds::new(stacked_graph.len());

            // Find path
            let path = stacked_graph.shortest_forward_path(start_label, end_label);

            // Add initial (to be four way cutting point)
            if let (
                PathNode::Begin {
                    node: begin_node,
                    out_link,
                },
                PathNode::End {
                    node: end_node,
                    in_link,
                },
            ) = (path.begin(), path.end())
            {
                assert_eq!(
                    begin_node.label, end_node.label,
                    "Begin and end point are not the same, no cutting path"
                );
                insert_tiled_bounds(&mut bounds, &stacked_graph, begin_node, in_link, out_link);
            } else {
                panic!("Path has been built incorrectly: it has no beginning and/or end");
            }

            // Add the rest of vertical bounds
            for pathnode in path.iter_middle() {
                if let PathNode::Middle {
                    node: label,
                    in_link,
                    out_link,
                } = pathnode
                {
                    insert_tiled_bounds(&mut bounds, &stacked_graph, label, in_link, out_link)
                } else {
                    panic!("Path has been built incorrectly: central pathnodes are not all of type middle")
                }
            }

            // Lazy search through all old bounds and remapping to the new bounds,
            // combining with the vertical bounds as well.
            for (label, stacked_bounds) in stacked_graph.bounds.enumerate() {
                for (semilink, bound) in stacked_bounds.iter() {
                    let semilabel = SemiLinkLabel {
                        node: label,
                        semilink,
                    };
                    if let Some(&horizontal_bound) = bounds.get(semilabel) {
                        match (bound, horizontal_bound) {
                            (StackedCylinderBound::Up, TiledPlanarBound::W) => bounds.insert(semilabel, TiledPlanarBound::NW),
                            (StackedCylinderBound::Up, TiledPlanarBound::E) => bounds.insert(semilabel, TiledPlanarBound::NE),
                            (StackedCylinderBound::Down, TiledPlanarBound::W) => bounds.insert(semilabel, TiledPlanarBound::SW),
                            (StackedCylinderBound::Down, TiledPlanarBound::E) => bounds.insert(semilabel, TiledPlanarBound::SE),
                            _ => panic!("Somehow previous bounds have been set to something else than `E` or `W`")
                        };
                    } else {
                        match bound {
                            StackedCylinderBound::Up => {
                                bounds.insert(semilabel, TiledPlanarBound::N)
                            }
                            StackedCylinderBound::Down => {
                                bounds.insert(semilabel, TiledPlanarBound::S)
                            }
                        };
                    }
                }
            }

            TiledPlanarGraph {
                nodes: stacked_graph.nodes,
                bounds,
            }
        }
    }

    // Insert bounds to the right side of the chosen nodes, and add the bounds coming back
    fn insert_tiled_bounds<G: Graph>(
        bounds: &mut Bounds<TiledPlanarBound>,
        graph: &G,
        label: G::Label,
        in_link: SemiLink,
        out_link: SemiLink,
    ) {
        let node = graph.get_node(label);
        for link in node.get_links_from_to_exclusive(out_link, in_link) {
            let semilabel = SemiLinkLabel {
                node: label,
                semilink: link,
            };
            bounds.insert(semilabel, TiledPlanarBound::E);
            let semilabel_back = graph.get_neighbour_semilink(semilabel);
            bounds.insert(semilabel_back, TiledPlanarBound::W);
        }
    }
}

/// Patch representation in 2D, the first field is used as time coordinate,
/// and the second field is used for the space patch coordinate.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Patch2D(pub i8, pub i8);

pub struct PatchCollection2D {
    list: BitVec,
}

const TIMELENGTH: usize = 2 * EXPECTED_MAX_PATCH_TIME_EXTENSION + 1;
const SPACELENGTH: usize = 2 * EXPECTED_MAX_PATCH_SPACE_EXTENSION + 1;
const TIME_EXTENSION: i8 = EXPECTED_MAX_PATCH_TIME_EXTENSION as i8;

impl PatchCollection for PatchCollection2D {
    type Patch = Patch2D;

    fn empty() -> Self {
        let list = bits![0; TIMELENGTH * SPACELENGTH];
        Self {
            list: BitVec::from_bitslice(list),
        }
    }

    fn insert(&mut self, patch: Self::Patch) -> bool {
        let index = TIMELENGTH * i8usize_central_mapping(patch.1)
            + usize::try_from(patch.0 + TIME_EXTENSION).unwrap_or_else(|_| {
                panic!(
                    "The patches went further in time ({}) than expected ({})",
                    patch.0, TIME_EXTENSION
                )
            });

        // Resize the list is there are to little elements
        if index >= self.list.len() {
            self.list.resize(index + 1, false);
        }

        // Check if already `true`/patch already present -> return `false`
        if self.list[index] {
            false
        } else {
            // If not present -> make `true` and return `true`
            self.list.set(index, true);
            true
        }
    }
}

impl Add for Patch2D {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Patch2D(self.0 + rhs.0, self.1 + rhs.1)
    }
}

impl Patch2D {
    pub fn origin() -> Self {
        Patch2D(0, 0)
    }
}

#[derive(Debug, Clone)]
struct Bound<B> {
    list: Vec<Option<B>>,
}

fn semilink_to_usize(semilink: SemiLink) -> usize {
    (match semilink {
        SemiLink::Left => 0,
        SemiLink::Right => 1,
        SemiLink::Up(i) => 2 + 2 * i,
        SemiLink::Down(i) => 3 + 2 * i,
    }) as usize
}

fn usize_to_semilink(index: usize) -> SemiLink {
    match index {
        0 => SemiLink::Left,
        1 => SemiLink::Right,
        j => {
            let u = j - 2;
            let i = u / 2;
            if u % 2 == 0 {
                SemiLink::Up(i as u8)
            } else {
                SemiLink::Down(i as u8)
            }
        }
    }
}

impl<B> Bound<B> {
    fn new() -> Self {
        Bound { list: Vec::new() }
    }

    fn get(&self, semilink: SemiLink) -> Option<&B> {
        if let Some(bound) = self.list.get(semilink_to_usize(semilink)) {
            bound.as_ref()
        } else {
            None
        }
    }

    fn insert(&mut self, semilink: SemiLink, bound: B) -> Option<B> {
        let index = semilink_to_usize(semilink);
        if index >= self.list.len() {
            self.list.resize_with(index + 1, || None);
        }
        std::mem::replace(&mut self.list[index], Some(bound))
    }

    fn iter(&self) -> impl Iterator<Item = (SemiLink, &B)> {
        self.list
            .iter()
            .enumerate()
            .filter_map(|(i, bound)| bound.as_ref().map(|bound| (usize_to_semilink(i), bound)))
    }
}

#[derive(Debug, Clone)]
struct Bounds<B>(LabelledArray<Bound<B>, Label<Node>>);

impl<B> Bounds<B> {
    fn new(len: usize) -> Self {
        Bounds(LabelledArray::fill(
            || Bound {
                list: Vec::with_capacity(1),
            },
            len,
        ))
    }

    /// If the [`Bounds`] did not have the `semilabel` present `None` is returned.
    ///
    /// If the [`Bounds`] did have the `semilabel` the value is replaced by `bounds` and the
    /// old value is returned
    fn insert<L: NodeLabel>(&mut self, semilabel: SemiLinkLabel<L>, bound: B) -> Option<B> {
        self.0[semilabel.node.into()].insert(semilabel.semilink, bound)
    }

    fn get<L: NodeLabel>(&self, semilabel: SemiLinkLabel<L>) -> Option<&B> {
        self.0[semilabel.node.into()].get(semilabel.semilink)
    }
}

/// Label of a connection to a [Node] w.r.t another [Node]
///
/// Internally used to index the neighbour lists held by [Node]
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd)]
pub enum SemiLink {
    Left,
    Up(u8),
    Right,
    Down(u8),
}

impl SemiLink {
    fn new_up(index: impl TryInto<u8, Error = impl Debug>) -> Self {
        SemiLink::Up(
            index
                .try_into()
                .expect("Tried to create a SemiLink label to an index larger than 255"),
        )
    }

    fn new_down(index: impl TryInto<u8, Error = impl Debug>) -> Self {
        SemiLink::Down(
            index
                .try_into()
                .expect("Tried to create a SemiLink label to an index larger than 255"),
        )
    }

    /// Returns whether the [`SemiLink`] is in the range [from, to), using **cyclic** ordering.
    ///
    /// Note that this is inclusive of the inital link and exclusive of the final link, and due to the
    /// cyclic nature [a, a) will result in a range selecting the full range of values.
    fn in_range(self, from: Self, to: Self) -> bool {
        match from.partial_cmp(&to).unwrap() {
            Ordering::Equal => true,
            Ordering::Less => from <= self && self < to,
            Ordering::Greater => !(to <= self && self < from),
        }
    }
}

impl Display for SemiLink {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            SemiLink::Left => write!(f, "←"),
            SemiLink::Up(i) => write!(f, "↑{}", i),
            SemiLink::Right => write!(f, "→"),
            SemiLink::Down(i) => write!(f, "↓{}", i),
        }
    }
}

/// A unique label for every semilink or half-link
///
/// It is labelled by the [NodeLabel] it is attached to,
/// in addition to the label of the link w.r.t the [Node].
#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct SemiLinkLabel<L> {
    node: L,
    semilink: SemiLink,
}

impl<L> SemiLinkLabel<L> {
    pub fn new(nodelabel: L, semilink: SemiLink) -> Self {
        Self {
            node: nodelabel,
            semilink,
        }
    }
}

impl<L: Debug> Debug for SemiLinkLabel<L> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?} {}", self.node, self.semilink)
    }
}

#[derive(Debug, Clone, Copy)]
/// Iterator over the [`SemiLink`]s of a [`Node`]
pub struct LinkIter<'a> {
    node: &'a Node,
    marker: Option<SemiLink>,
    from_link: SemiLink,
    to_link: SemiLink,
}

impl<'a> LinkIter<'a> {
    /// Creates iterator over full range
    fn new(node: &'a Node) -> LinkIter {
        Self::from_to(node, SemiLink::Left, SemiLink::Left)
    }

    /// Creates iterator [from, to)
    ///
    /// Note [a, a) would be over the full range
    fn from_to(node: &'a Node, from: SemiLink, to: SemiLink) -> Self {
        let next = node.next_semilink(from);
        if next == to {
            LinkIter {
                node,
                marker: Some(from),
                from_link: from,
                to_link: to,
            }
        } else {
            LinkIter {
                node,
                marker: Some(from),
                from_link: next,
                to_link: to,
            }
        }
    }

    /// Creates iterator [from, to]
    ///
    /// Note that [a, a] makes no sense cyclically and panics
    fn from_to_inclusive(node: &'a Node, from: SemiLink, to: SemiLink) -> Self {
        assert_ne!(
            from, to,
            "Cannot iterate over inclusive range from to itself"
        );
        LinkIter::from_to(node, from, node.next_semilink(to))
    }

    /// Creates iterator (from, to)
    fn from_to_exclusive(node: &'a Node, from: SemiLink, to: SemiLink) -> Self {
        let from = node.next_semilink(from);
        if from == to {
            return Self::empty(node);
        }

        Self::from_to(node, from, to)
    }

    fn empty(node: &'a Node) -> Self {
        LinkIter {
            node,
            marker: None,
            from_link: SemiLink::Left,
            to_link: SemiLink::Left,
        }
    }
}

impl Iterator for LinkIter<'_> {
    type Item = SemiLink;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(marker) = self.marker {
            let next_link = self.node.next_semilink(marker);
            self.marker = if next_link.in_range(self.from_link, self.to_link) {
                Some(next_link)
            } else {
                None
            };
            Some(marker)
        } else {
            None
        }
    }
}

/// Structure representing the extracted information from
/// a [BreadthFirstSearch] (BFS)
///
/// `label`: the label of the [Node] itself;
/// `distance`: the distance from the `origin` node in the [BFS](BreadthFirstSearch);
/// `source`: a reference to source of the node in the [BFS](BreadthFirstSearch) exploration,
/// meaning that this node was explored as a neighbour of the source node. It is an [Option]
/// of a [SemiLinkLabel] as the origin has no source, and both the source node and semilink
/// are stored.
#[derive(Clone, Copy, Hash, PartialEq, Eq)]
pub struct TraversedNode<L> {
    label: L,
    distance: usize,
    source: Option<SemiLinkLabel<L>>,
}

impl<L: NodeLabel + Debug> Debug for TraversedNode<L> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if let Some(source) = self.source {
            write!(
                f,
                "{:?} from {:?} at {:}",
                self.label, source, self.distance
            )
        } else {
            write!(f, "origin {:?} at {:}", self.label, self.distance)
        }
    }
}

impl<L: NodeLabel> TraversedNode<L> {
    fn from_origin(label: L) -> Self {
        TraversedNode {
            label,
            distance: 0,
            source: None,
        }
    }

    /// Creates a [`TraversedNode`] from a source [`TraversedNode`] and the linking [`SemiLinkLabel`]
    fn from_source(source: Self, semilink: SemiLink, label: L) -> Self {
        TraversedNode {
            label,
            distance: source.distance + 1,
            source: Some(SemiLinkLabel {
                node: source.label,
                semilink,
            }),
        }
    }
}

type GraphPatchCollection<G> = <<G as Graph>::Label as NodeLabel>::PatchCollection;

pub struct BreadthFirstSearch<'a, G>
where
    G: Graph + ?Sized,
{
    graph: &'a G,
    explored: LabelledArray<GraphPatchCollection<G>, Label<Node>>,
    to_visit: VecDeque<TraversedNode<G::Label>>,
}

impl<G: Graph + ?Sized> BreadthFirstSearch<'_, G> {
    fn from_graph(graph: &G, origin: G::Label) -> BreadthFirstSearch<G> {
        let node_count = graph.len();
        // Preallocating (node_count / 2) capacity, this should be more than sufficient for finite
        // graphs, and likely sufficient for the reach considered of infinity graphs.
        let mut to_visit = VecDeque::with_capacity(node_count / 2);
        let mut explored = LabelledArray::fill(
            <GraphPatchCollection<G> as PatchCollection>::empty,
            node_count,
        );
        to_visit.push_back(TraversedNode::from_origin(origin));
        explored[origin.into()].insert(origin.get_patch());
        BreadthFirstSearch {
            graph,
            to_visit,
            explored,
        }
    }
}

impl<G: Graph + ?Sized> Iterator for BreadthFirstSearch<'_, G> {
    type Item = TraversedNode<G::Label>;

    fn next(&mut self) -> Option<Self::Item> {
        let traversed_node = self.to_visit.pop_front()?;
        self.to_visit.extend(
            self.graph
                .get_links(traversed_node.label)
                .map(|semilink| {
                    (
                        self.graph.get_neighbour(SemiLinkLabel {
                            node: traversed_node.label,
                            semilink,
                        }),
                        semilink,
                    )
                })
                .filter(|&(nbr, _)| self.explored[nbr.into()].insert(nbr.get_patch()))
                .map(|(nbr, semilink)| TraversedNode::from_source(traversed_node, semilink, nbr)),
        );
        Some(traversed_node)
    }
}

pub struct ForwardBreadthFirstSearch<'a, G>
where
    G: Graph + ?Sized,
{
    graph: &'a G,
    explored: LabelledArray<GraphPatchCollection<G>, Label<Node>>,
    to_visit: VecDeque<TraversedNode<G::Label>>,
}

impl<'a, G: Graph + ?Sized> ForwardBreadthFirstSearch<'a, G> {
    fn from_graph(graph: &'a G, origin: G::Label) -> Self {
        let node_count = graph.len();
        // Preallocating (node_count / 2) capacity, this should be more than sufficient for finite
        // graphs, and likely sufficient for the reach considered of infinity graphs.
        let mut to_visit = VecDeque::with_capacity(node_count / 2);
        let mut explored = LabelledArray::fill(
            <GraphPatchCollection<G> as PatchCollection>::empty,
            node_count,
        );
        to_visit.push_back(TraversedNode::from_origin(origin));
        explored[origin.into()].insert(origin.get_patch());
        ForwardBreadthFirstSearch {
            graph,
            to_visit,
            explored,
        }
    }
}

impl<G: Graph + ?Sized> Iterator for ForwardBreadthFirstSearch<'_, G> {
    type Item = TraversedNode<G::Label>;

    fn next(&mut self) -> Option<Self::Item> {
        let traversed_node = self.to_visit.pop_front()?;
        self.to_visit.extend(
            self.graph
                .get_forward_links(traversed_node.label)
                .map(|semilink| {
                    (
                        self.graph.get_neighbour(SemiLinkLabel {
                            node: traversed_node.label,
                            semilink,
                        }),
                        semilink,
                    )
                })
                .filter(|&(nbr, _)| self.explored[nbr.into()].insert(nbr.get_patch()))
                .map(|(nbr, semilink)| TraversedNode::from_source(traversed_node, semilink, nbr)),
        );
        Some(traversed_node)
    }
}

mod path {
    use super::*;

    #[derive(Clone, Copy)]
    pub enum PathNode<L> {
        Begin {
            node: L,
            out_link: SemiLink,
        },
        Middle {
            node: L,
            in_link: SemiLink,
            out_link: SemiLink,
        },
        End {
            node: L,
            in_link: SemiLink,
        },
        Single {
            node: L,
        },
    }

    impl<L: Debug> Debug for PathNode<L> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                PathNode::Begin { node, out_link } => write!(f, "{:?} {}", node, out_link),
                PathNode::Middle {
                    node,
                    in_link,
                    out_link,
                } => write!(f, "{} {:?} {}", in_link, node, out_link),
                PathNode::End { node, in_link } => write!(f, "{} {:?}", in_link, node),
                PathNode::Single { node } => write!(f, "{:?}", node),
            }
        }
    }

    pub struct Path<L> {
        path: Box<[PathNode<L>]>,
    }

    impl<L: Debug> Debug for Path<L> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            writeln!(f, "{:?}", self.path)
        }
    }

    impl<L: Copy> Path<L> {
        pub fn len(&self) -> usize {
            self.path.len() - 1
        }

        pub fn get(&self, index: usize) -> Option<PathNode<L>> {
            self.path.get(index).copied()
        }

        pub fn iter(&self) -> Iter<L> {
            Iter::new(self)
        }

        pub fn iter_middle(&self) -> IterMiddle<L> {
            IterMiddle::new(self)
        }

        pub fn begin(&self) -> PathNode<L> {
            *self.path.first().expect("Path was empty")
        }

        pub fn end(&self) -> PathNode<L> {
            *self.path.last().expect("Path was empty")
        }
    }

    impl<L> Path<L> {
        /// Creates a [`Path`] from a vector of [`PathNode`]
        ///
        /// This assumes that the first element is
        /// a [`PathNode::Begin`] the final a [`PathNode::End`]
        /// and [`PathNode::Middle`] inbetween.
        /// Alternatively if the path is only a single element
        /// it should be [`PathNode::Single`]
        pub fn from_vec(vec: Vec<PathNode<L>>) -> Self {
            Path {
                path: vec.into_boxed_slice(),
            }
        }

        /// Creates a [`Path`] from a vector of [`PathNode`],
        /// reversing the path direction.
        pub fn from_vec_reversed(vec: Vec<PathNode<L>>) -> Self {
            Self::from_vec(vec.into_iter().rev().collect())
        }
    }

    pub struct IterMiddle<'a, L> {
        path: &'a Path<L>,
        index: usize,
        last: usize,
    }

    impl<'a, L> IterMiddle<'a, L> {
        fn new(path: &'a Path<L>) -> Self {
            IterMiddle {
                path,
                index: 1,
                last: path.path.len() - 1,
            }
        }
    }

    impl<L: Copy> Iterator for IterMiddle<'_, L> {
        type Item = PathNode<L>;

        fn next(&mut self) -> Option<Self::Item> {
            if self.index >= self.last {
                return None;
            }

            let next = self.path.get(self.index);
            self.index += 1;
            next
        }
    }

    pub struct Iter<'a, L> {
        path: &'a Path<L>,
        index: usize,
    }

    impl<'a, L> Iter<'a, L> {
        fn new(path: &'a Path<L>) -> Self {
            Iter { path, index: 0 }
        }
    }

    impl<L: Copy> Iterator for Iter<'_, L> {
        type Item = PathNode<L>;

        fn next(&mut self) -> Option<Self::Item> {
            let next = self.path.get(self.index);
            self.index += 1;
            next
        }
    }
}

#[cfg(test)]
mod tests {
    use std::io::{self, Write};

    use rand::SeedableRng;
    use rand_xoshiro::Xoshiro256StarStar;

    use super::*;
    use crate::triangulation::DynTriangulation;

    #[test]
    fn link_ordering() {
        let a = SemiLink::Left;
        let b = SemiLink::Down(3);

        assert!(SemiLink::Left.in_range(a, a));
        assert!(SemiLink::Up(4).in_range(a, a));
        assert!(SemiLink::Right.in_range(a, a));
        assert!(SemiLink::Down(2).in_range(a, a));

        assert!(SemiLink::Right.in_range(a, b));
        assert!(SemiLink::Up(5).in_range(a, b));
        assert!(SemiLink::Down(2).in_range(a, b));
        assert!(SemiLink::Left.in_range(a, b));
        assert!(!SemiLink::Down(3).in_range(a, b));
        assert!(!SemiLink::Down(6).in_range(a, b));

        assert!(SemiLink::Down(3).in_range(b, a));
        assert!(SemiLink::Down(5).in_range(b, a));
        assert!(!SemiLink::Left.in_range(b, a));
        assert!(!SemiLink::Up(3).in_range(b, a));
        assert!(!SemiLink::Right.in_range(b, a));
    }

    #[test]
    fn link_iteration() {
        let a = Label::initial();
        let b = a.succ();
        let c = b.succ();
        let d = c.succ();
        let e = d.succ();

        let node = Node {
            left: a,
            up: vec![b].into_boxed_slice(),
            right: c,
            down: vec![d, e].into_boxed_slice(),
        };

        let mut iter = LinkIter::new(&node);
        assert_eq!(iter.next(), Some(SemiLink::Left));
        assert_eq!(iter.next(), Some(SemiLink::Up(0)));
        assert_eq!(iter.next(), Some(SemiLink::Right));
        assert_eq!(iter.next(), Some(SemiLink::Down(0)));
        assert_eq!(iter.next(), Some(SemiLink::Down(1)));
        assert_eq!(iter.next(), None);

        let mut iter = LinkIter::from_to(&node, SemiLink::Right, SemiLink::Up(0));
        assert_eq!(iter.next(), Some(SemiLink::Right));
        assert_eq!(iter.next(), Some(SemiLink::Down(0)));
        assert_eq!(iter.next(), Some(SemiLink::Down(1)));
        assert_eq!(iter.next(), Some(SemiLink::Left));
        assert_eq!(iter.next(), None);

        let mut iter = LinkIter::from_to(&node, SemiLink::Right, SemiLink::Down(0));
        assert_eq!(iter.next(), Some(SemiLink::Right));
        assert_eq!(iter.next(), None);

        let mut iter = LinkIter::from_to(&node, SemiLink::Right, SemiLink::Down(1));
        assert_eq!(iter.next(), Some(SemiLink::Right));
        assert_eq!(iter.next(), Some(SemiLink::Down(0)));
        assert_eq!(iter.next(), None);

        let mut iter = LinkIter::from_to_inclusive(&node, SemiLink::Right, SemiLink::Up(0));
        assert_eq!(iter.next(), Some(SemiLink::Right));
        assert_eq!(iter.next(), Some(SemiLink::Down(0)));
        assert_eq!(iter.next(), Some(SemiLink::Down(1)));
        assert_eq!(iter.next(), Some(SemiLink::Left));
        assert_eq!(iter.next(), Some(SemiLink::Up(0)));
        assert_eq!(iter.next(), None);

        let mut iter = LinkIter::from_to_inclusive(&node, SemiLink::Right, SemiLink::Down(1));
        assert_eq!(iter.next(), Some(SemiLink::Right));
        assert_eq!(iter.next(), Some(SemiLink::Down(0)));
        assert_eq!(iter.next(), Some(SemiLink::Down(1)));
        assert_eq!(iter.next(), None);

        let mut iter = LinkIter::from_to_exclusive(&node, SemiLink::Right, SemiLink::Up(0));
        assert_eq!(iter.next(), Some(SemiLink::Down(0)));
        assert_eq!(iter.next(), Some(SemiLink::Down(1)));
        assert_eq!(iter.next(), Some(SemiLink::Left));
        assert_eq!(iter.next(), None);

        let mut iter = LinkIter::from_to_exclusive(&node, SemiLink::Right, SemiLink::Down(0));
        assert_eq!(iter.next(), None);
    }

    fn generate_full_triangulation(vertex_length: usize, timeslices: usize) -> FullTriangulation {
        let mut triangulation = DynTriangulation::new(vertex_length, timeslices);

        let mut rng = Xoshiro256StarStar::seed_from_u64(42);

        for _ in 0..100 {
            for _ in 0..100 {
                triangulation.flip(triangulation.sample_flip(&mut rng));
            }
            for _ in 0..28 {
                triangulation.add(triangulation.sample(&mut rng));
            }
            for _ in 0..18 {
                triangulation.remove(triangulation.sample_order_four(&mut rng))
            }
            for _ in 0..300 {
                triangulation.flip(triangulation.sample_flip(&mut rng));
            }
            for _ in 0..10 {
                triangulation.remove(triangulation.sample_order_four(&mut rng))
            }
        }

        FullTriangulation::from_triangulation(&triangulation)
    }

    #[test]
    fn traverse_toroidal_graph() {
        let mut rng = Xoshiro256StarStar::seed_from_u64(13);
        let triangulation = generate_full_triangulation(10, 20);

        println!("====Vertex Graph=========");
        let vertex_graph = triangulation.construct_vertex_graph();
        assert_eq!(vertex_graph.len(), 200);
        for node in vertex_graph
            .iter_breadth_first(vertex_graph.sample_node(&mut rng))
            .take(20)
        {
            println!("{:?}", node);
        }

        println!("======Dual Graph=========");
        let dual_graph = triangulation.construct_dual_graph();
        assert_eq!(dual_graph.len(), 400);
        for node in dual_graph
            .iter_breadth_first(dual_graph.sample_node(&mut rng))
            .take(20)
        {
            println!("{:?}", node);
        }

        let mut vertex_count = 0;
        for _ in vertex_graph.iter_breadth_first(vertex_graph.sample_node(&mut rng)) {
            vertex_count += 1;
        }
        let mut triangle_count = 0;
        for _ in dual_graph.iter_breadth_first(dual_graph.sample_node(&mut rng)) {
            triangle_count += 1;
        }

        assert_eq!(vertex_count, 200);
        assert_eq!(triangle_count, 400);
    }

    #[test]
    fn traverse_stacked_graph() {
        let mut rng = Xoshiro256StarStar::seed_from_u64(13);
        let triangulation = generate_full_triangulation(10, 20);

        println!("====Vertex Graph=========");
        let vertex_graph = triangulation.construct_stacked_vertex_graph();
        assert_eq!(vertex_graph.len(), 200);
        for (_, node) in vertex_graph
            .iter_breadth_first(vertex_graph.sample_node(&mut rng))
            .take(400)
            .enumerate()
            .filter(|&(i, _)| !(20..=390).contains(&i) || i >= 399 || i > 100 - 10 && i < 100 + 10)
        {
            println!("{:?}", node);
        }
        println!("Reached the end");

        println!("======Dual Graph=========");
        let dual_graph = triangulation.construct_stacked_dual_graph();
        assert_eq!(dual_graph.len(), 400);
        for (_, node) in dual_graph
            .iter_breadth_first(dual_graph.sample_node(&mut rng))
            .take(800)
            .enumerate()
            .filter(|&(i, _)| !(20..=790).contains(&i) || i >= 799 || i > 100 - 10 && i < 100 + 10)
        {
            println!("{:?}", node);
        }
        println!("Reached the end");
    }

    #[test]
    fn distance_profile() {
        let rng = &mut Xoshiro256StarStar::seed_from_u64(13);
        let triangulation = generate_full_triangulation(10, 20);

        let graph = triangulation.construct_dual_graph();
        let stacked_graph = triangulation.construct_stacked_dual_graph();
        let tiled_graph = TiledPlanarGraph::from_stacked_graph(stacked_graph.clone());

        let origin = graph.sample_node(rng);
        let graph_dp = graph.distance_profile(origin, 40);
        let stacked_dp_0 = stacked_graph.distance_profile(
            StackedCylinderLabel {
                label: origin,
                patch: Patch1D(0),
            },
            40,
        );
        let stacked_dp_1 = stacked_graph.distance_profile(
            StackedCylinderLabel {
                label: origin,
                patch: Patch1D(1),
            },
            40,
        );
        let tiled_dp = tiled_graph.distance_profile(
            TiledPlanarLabel {
                patch: Patch2D::origin(),
                label: origin,
            },
            40,
        );
        println!("{:50}", graph_dp);
        println!("{:50}", stacked_dp_0);
        println!("{:50}", tiled_dp);

        assert!(DistanceProfile::equality_up_to(
            &stacked_dp_1,
            &stacked_dp_0,
            20
        ));
        assert!(DistanceProfile::equality_up_to(
            &graph_dp,
            &stacked_dp_0,
            20
        ));
        assert!(DistanceProfile::equality_up_to(&graph_dp, &tiled_dp, 4));
    }

    #[test]
    fn distance_profile_large() {
        let rng = &mut Xoshiro256StarStar::seed_from_u64(42);
        let triangulation = generate_full_triangulation(100, 40);

        let graph = triangulation.construct_vertex_graph();
        let stacked_graph = triangulation.construct_stacked_vertex_graph();
        let tiled_graph = TiledPlanarGraph::from_stacked_graph(stacked_graph);

        let origin = graph.sample_node(rng);
        let graph_dp = graph.distance_profile(origin, 30);
        let tiled_dp = tiled_graph.distance_profile(
            TiledPlanarLabel {
                patch: Patch2D::origin(),
                label: origin,
            },
            22,
        );
        println!("{:50}", graph_dp);
        println!("{:50}", tiled_dp);

        assert!(DistanceProfile::equality_up_to(&graph_dp, &tiled_dp, 20));
    }

    #[test]
    fn find_path() {
        let rng = &mut Xoshiro256StarStar::seed_from_u64(13);
        let triangulation = generate_full_triangulation(10, 20);

        let graph = triangulation.construct_dual_graph();

        let a = graph.sample_node(rng);
        let b = graph.sample_node(rng);

        println!("====Regular path=========");
        println!("{:?}", graph.shortest_path(a, a));
        assert_eq!(graph.shortest_path(a, a).len(), 0);
        println!("{:?}", graph.shortest_path(a, b));
        assert_eq!(graph.shortest_path(a, b).len(), 13);
        println!("{:?}", graph.shortest_path(b, a));
        assert_eq!(graph.shortest_path(b, a).len(), 13);

        println!("====Forward paths=========");
        println!("{:?}", graph.shortest_forward_path(a, a));
        assert_eq!(graph.shortest_path(a, a).len(), 0);
        println!("{:?}", graph.shortest_forward_path(b, a));
    }

    #[test]
    fn find_geodesic() {
        let rng = &mut Xoshiro256StarStar::seed_from_u64(13);
        let triangulation = generate_full_triangulation(10, 20);

        let graph = triangulation.construct_stacked_dual_graph();
        let a = graph.sample_node(rng);
        let b = StackedCylinderLabel {
            label: a.label,
            patch: a.patch + Patch1D(1),
        };

        let geodesic = graph.shortest_forward_path(a, b);
        println!("length: {}; {:?}", geodesic.len(), geodesic);
    }

    #[test]
    fn traverse_tiled_graph() {
        let mut rng = Xoshiro256StarStar::seed_from_u64(13);
        let triangulation = generate_full_triangulation(10, 20);

        println!("====Vertex Graph=========");
        let vertex_graph = triangulation.construct_stacked_vertex_graph();
        let vertex_graph = TiledPlanarGraph::from_stacked_graph(vertex_graph);
        assert_eq!(vertex_graph.len(), 200);
        let origin = vertex_graph.sample_node(&mut rng);
        for (_, node) in vertex_graph
            .iter_breadth_first(origin)
            .take(400)
            .enumerate()
            .filter(|&(i, _)| !(20..=390).contains(&i) || i >= 399 || i > 100 - 10 && i < 100 + 10)
        {
            println!("{:?}", node);
            io::stdout().flush().unwrap();
        }
        println!("Reached the end");

        println!("======Dual Graph=========");
        let dual_graph = triangulation.construct_stacked_dual_graph();
        let dual_graph = TiledPlanarGraph::from_stacked_graph(dual_graph);
        assert_eq!(dual_graph.len(), 400);
        for (_, node) in dual_graph
            .iter_breadth_first(dual_graph.sample_node(&mut rng))
            .take(800)
            .enumerate()
            .filter(|&(i, _)| !(20..=790).contains(&i) || i >= 799 || i > 100 - 10 && i < 100 + 10)
        {
            println!("{:?}", node);
        }
        println!("Reached the end");
    }
}
