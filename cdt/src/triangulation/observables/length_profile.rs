use std::fmt::Display;

use full_triangulation::FullTriangulation;
use serde::ser::SerializeSeq;
use serde::Serialize;

use crate::collections::TypedLabel;
use crate::triangulation::triangle::{Orientation, Triangle};
use crate::triangulation::{full_triangulation, Triangulation};

use super::Observable;

type Label = TypedLabel<Triangle>;

/// Struct storing the `LengthProfile of a `Triangulation`.
///
/// A lengthprofile is the amount of vertices of each timeslice in the triangulation
#[derive(Debug)]
pub struct LengthProfile {
    // Stores the amount of triangles per slice in a simple array
    ups: Box<[usize]>, // Amount of up triangles in every slice
}

impl LengthProfile {
    fn empty() -> Self {
        LengthProfile { ups: Box::new([]) }
    }
}

/// Determine the `LengthProfile` starting at a given starting point `origin`
pub fn length_profile_origin<'a>(
    triangulation: &'a impl Triangulation<'a>,
    origin: Label,
) -> LengthProfile {
    let triangle_count = triangulation.size();
    let mut ups: Vec<usize> = Vec::with_capacity(triangle_count / 2);

    let start = origin;

    let mut slice_start = start;
    'time: for t in 0..triangle_count / 2 {
        ups.push(0);
        let mut mark = slice_start;
        let mut down_mark: Option<Label> = None;
        'slice: for _ in 0..triangle_count {
            // Add triangle to the count
            match triangulation.get(mark).orientation {
                Orientation::Up => ups[t] += 1,
                Orientation::Down => {
                    down_mark.get_or_insert(mark);
                }
            }
            // Move along the slice to the right
            mark = triangulation.get(mark).right;
            // Stop if the original start is reached again
            if t > 0 && mark == start {
                break 'time;
            }
            // Go to next slice if slice_start is reached again
            if mark == slice_start {
                break 'slice;
            }
        }
        slice_start = triangulation
            .get(down_mark.expect(
                "No Down-triangle could be find in the timeslice, this should be impossible.",
            ))
            .center;
    }
    // Remove the last timeslice as it is the same as the first one
    ups.pop();

    LengthProfile {
        ups: ups.into_boxed_slice(),
    }
}

/// Returns the `LengthProfile` of the given `Triangulation`
///
/// To determine the length profile, the search starts at an arbitrary starting point.
pub fn length_profile<'a>(triangulation: &'a impl Triangulation<'a>) -> LengthProfile {
    // Pick random starting point
    if let Some(label) = triangulation.labels().next() {
        length_profile_origin(triangulation, label)
    } else {
        LengthProfile::empty()
    }
}

impl FullTriangulation {
    /// Return [`LengthProfile`] of [`FullTriangulation`]
    pub fn length_profile(&self) -> LengthProfile {
        LengthProfile {
            ups: self
                .timeslices
                .iter()
                .map(|timeslice| timeslice.len())
                .collect(),
        }
    }
}

impl LengthProfile {
    /// Returns the lengths of the `LengthProfile` as an array,
    ///
    /// The length here is the amount of vertices in the timeslices,
    /// or equivalently the amount of `Up` `Triangle`s
    /// in the upper thick timeslice
    pub fn lengths(&self) -> &[usize] {
        &self.ups
    }

    /// Return the amount of timeslices in the `Lengthprofile`
    pub fn timeslices_count(&self) -> usize {
        self.ups.len()
    }
}

impl Observable for LengthProfile {}

impl Display for LengthProfile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.ups)
    }
}

impl Serialize for LengthProfile {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let list = &self.ups;
        let mut seq = serializer.serialize_seq(Some(list.len()))?;
        for count in list.iter() {
            seq.serialize_element(count)?;
        }
        seq.end()
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::triangulation::dynamic_triangulation::DynTriangulation;

    #[test]
    fn length_profile_small() {
        let trian1 = DynTriangulation::new(3, 1);
        assert_eq!(length_profile(&trian1).lengths(), [3]);

        let trian2 = DynTriangulation::new(3, 2);
        assert_eq!(length_profile(&trian2).lengths(), [3, 3]);

        let trian = DynTriangulation::new(3, 10);
        assert_eq!(length_profile(&trian).lengths(), [3; 10]);
    }
}
