//! This implementation of a `Triangulation` is optimized to perform Monte Carlo simulations on,
//! specifically the (2-4, 4-2, and 2-2 flip) Monte Carlo moves.
//! It also includes methods which can be used to analyse the `Triangulation` and perform
//! measurements on it in a Monte Carlo simulation.

pub use super::unique_id::TriangulationID;

use super::triangle::{Orientation, Triangle};
use super::Triangulation;
use crate::collections::{LabelSet, LabelledList, TypedLabel};
use rand::Rng;

type Label = TypedLabel<Triangle>;

const MAX_LENGTH_FACTOR: usize = 2;

/// Struct holding the `Triangulation`, consisting of a `LabelledList` of all `Triangle`s
/// and additional tracking of all flipable `Triangle` pairs, and order four vertices.
pub struct DynTriangulation {
    triangles: LabelledList<Triangle>, // List of all triangles, representing the triangulation
    order_four: LabelSet<Triangle>, // Set of all order four vertices, labelled by the upper right triangle
    flip_pair: LabelSet<Triangle>, // Set of all flip pair triangles, labelled by the right triangle
}

impl<'a> Triangulation<'a> for DynTriangulation {
    fn labels(&'a self) -> Box<dyn Iterator<Item = Label> + 'a> {
        Box::new(self.triangles.labels())
    }

    fn get(&self, label: Label) -> &Triangle {
        &self.triangles[label]
    }

    fn vertex_order(&self, label: Label) -> usize {
        self.vertex_order(label)
    }

    fn size(&self) -> usize {
        self.size()
    }

    fn max_size(&self) -> usize {
        self.triangles.max_size()
    }
}

/// Associated functions mainly concerned with creation of `Triangulation`s
impl DynTriangulation {
    /// Create a new flat `Triangulation`.
    ///
    /// This `Triangulation` is created with a flat two-torus topology,
    /// with `timeslices` timeslices, each of which containing `vertex_length` vertices.
    /// So the `Triangulation` will have 2 · `vertex_length` · `timeslices` `Triangles`.
    ///
    /// This function allocates the `MAX_LENGTH_FACTOR` (`2` at the time of writing these docs)
    /// time the starting size of the `Triangulation` for possible growth.
    /// For more control over the amount of space allocated use [`new_allocate()`](Triangulation::new_allocate()).
    pub fn new(vertex_length: usize, timeslices: usize) -> DynTriangulation {
        let expected_length = 2 * vertex_length * timeslices; // Expected total amount of triangles
        Self::new_allocate(
            vertex_length,
            timeslices,
            MAX_LENGTH_FACTOR * expected_length,
        )
    }

    /// Create a new flat `Triangulation`.
    ///
    /// Same as [`new()`](Triangulation::new()), but with ability to set the maximum allowed size
    /// of the amount of `Triangle`s, set by `max_size`.
    pub fn new_allocate(
        vertex_length: usize,
        timeslices: usize,
        max_size: usize,
    ) -> DynTriangulation {
        let start_length = 2 * vertex_length; // Determine construction L

        let mut triangles = LabelledList::new(max_size);
        for t in 0..timeslices {
            for i in 0..start_length {
                let left = Label::from(t * start_length + (i + (start_length - 1)) % start_length);
                let right = Label::from(t * start_length + (i + 1) % start_length);
                let (center, orientation) = if i % 2 == 0 {
                    (
                        Label::from((t + (timeslices - 1)) % timeslices * start_length + i + 1),
                        Orientation::Up,
                    )
                } else {
                    (
                        Label::from((t + 1) % timeslices * start_length + i - 1),
                        Orientation::Down,
                    )
                };
                let label = triangles.add(Triangle {
                    left,
                    center,
                    right,
                    orientation,
                });
                let expected_label = Label::from(t * start_length + i);
                assert_eq!(
                    label, expected_label,
                    "Triangle at ({}, {}) was assigned {} instead of expected {}",
                    t, i, label, expected_label
                );
            }
        }

        // For the flat construction used every `Triangle` is a `flip_pair`
        // And none is an `order_four`.
        let order_four = LabelSet::new(max_size);
        let mut flip_pair = LabelSet::new(max_size);
        for label in triangles.labels() {
            flip_pair.add(label);
        }

        DynTriangulation {
            triangles,
            order_four,
            flip_pair,
        }
    }
}

/// Methods for `Triangulation`
impl DynTriangulation {
    /// Add an `Up`-`Down` `Triangle` pair right of the given `Label`.
    pub fn add(&mut self, label: Label) {
        // Determine neighbour of the to be added triangle pair
        let (neighbour_up_left, neighbour_down_left) = match self.triangles[label].orientation {
            Orientation::Up => (label, self.triangles[label].center),
            Orientation::Down => (self.triangles[label].center, label),
        };
        let neighbour_up_right = self.triangles[neighbour_up_left].right;
        let neighbour_down_right = self.triangles[neighbour_down_left].right;

        // Add new triangle pair with neighbours
        let temp = Label::from(usize::MAX);
        let new_up = self.triangles.add(Triangle {
            left: neighbour_up_left,
            center: temp, // Set to temp and fix later when other down triangle is added
            right: neighbour_up_right,
            orientation: Orientation::Up,
        });
        let new_down = self.triangles.add(Triangle {
            left: neighbour_down_left,
            center: new_up,
            right: neighbour_down_right,
            orientation: Orientation::Down,
        });
        self.triangles[new_up].center = new_down; // Set timelike neighbour correctly

        // Set new connections to triangle pair
        self.triangles[neighbour_up_left].right = new_up;
        self.triangles[neighbour_down_left].right = new_down;
        self.triangles[neighbour_up_right].left = new_up;
        self.triangles[neighbour_down_right].left = new_down;

        // Update label lists
        self.order_four.add(new_up);
        //TODO update flip pair
    }

    /// Remove an `Up`-`Down` `Triangle` pair at the given `Label`.
    ///
    /// NOTE that this `Label` should point to the upper right `Triangle`
    /// of an order four vertex, else this code `panic`s; for a removal without
    /// this check see [`remove_checkless()`](Triangulation::remove_checkless()).
    /// `Triangle`s can only be removed at four vertices, as this is the only
    /// case where the `Triangulation` can be stitched together after removal;
    /// and here the convention is used of marking order four vertices by
    /// their upper right `Triangle`.
    pub fn remove(&mut self, label: Label) {
        assert!(
            self.order_four.remove(label),
            "Attempted to remove triangle {}, which does not mark an order four vertex.",
            label
        );

        // Stitch together the neighbours
        let (up, down) = self.stitch_neighbours(label);

        // Remove triangles (with safe removals, in case `up` and `down` are not in the `Triangulation`)
        self.triangles.remove(up);
        self.triangles.remove(down);
    }

    /// Remove an `Up`-`Down` `Triangle` pair at the given `Label`.
    ///
    /// NOTE that this `Label` **must** point to the upper right `Triangle`
    /// of an order four vertex.
    /// **If this is not the case this will mess up the `Triangulation`**.
    /// To perform a safe removal of `Triangle`s use [`remove()`](Triangulation::remove()),
    /// which `panic`s in case the removal cannot be performed.
    /// `Triangle`s can only be removed at four vertices, as this is the only
    /// case where the `Triangulation` can be stitched together after removal;
    /// and here the convention is used of marking order four vertices by
    /// their upper right `Triangle`.
    pub fn remove_checkless(&mut self, label: Label) {
        // Remove label from the list of order four vertices
        self.order_four.remove(label);

        // Stitch together the neighbours
        let (up, down) = self.stitch_neighbours(label);

        // Remove triangles
        self.triangles.remove_checkless(up);
        self.triangles.remove_checkless(down);
    }

    /// Stitch together neighbours of an `Up`-`Down` `Triangle`-pair
    /// as to remove the pair from the connectivity.
    ///
    /// Returns the `Label`s of the `Up` and `Down` `Triangle` for re-use
    fn stitch_neighbours(&mut self, label: Label) -> (Label, Label) {
        // Find to be removed triangles
        let up_label = label;
        let up = &self.triangles[up_label];
        let down_label = up.center;
        let down = &self.triangles[down_label];

        // Find neighbours
        let neighbour_up_left = up.left;
        let neighbour_up_right = up.right;
        let neighbour_down_left = down.left;
        let neighbour_down_right = down.right;

        // Stitch neighbours together
        self.triangles[neighbour_up_left].right = neighbour_up_right;
        self.triangles[neighbour_up_right].left = neighbour_up_left;
        self.triangles[neighbour_down_left].right = neighbour_down_right;
        self.triangles[neighbour_down_right].left = neighbour_down_left;

        // Return up and down labels for re-use
        (up_label, down_label)
    }

    /// Flip a pair of `Triangle`s given by the right `Triangle` `Label`.
    ///
    /// NOTE that this method checks whether the the given `Label` actually
    /// corresponds to a flippable pair, if not it `panic`s.
    pub fn flip(&mut self, label: Label) {
        assert!(self.flip_pair.contains(label), "Attempted to flip a Triangle pair, of which the right Triangle is not in the flip_pair list");
        self.flip_centers(label, Self::add_order_four);
    }

    /// Flip a pair of `Triangle`s given by the right `Triangle` `Label`,
    /// that is for small `Triangulation`s (currently only for 2 timeslices)
    ///
    /// NOTE that this method checks whether the the given `Label` actually
    /// corresponds to a flippable pair, if not it `panic`s.
    pub fn flip_small(&mut self, label: Label) {
        assert!(self.flip_pair.contains(label), "Attempted to flip a Triangle pair, of which the right Triangle is not in the flip_pair list");
        self.flip_centers(label, Self::add_order_four_small);
    }

    /// Flip a pair of `Triangle`s given by the right `Triangle` `Label`,
    /// without checking whether this `Label` corresponds to a flippable pair.
    ///
    /// NOTE that this methods breaks the `Triangulation` if called on a `Label`
    /// which does not correspond to a flip pair; **so only use if certain that
    /// `Label` corresponds to the right `Triangle` of a flip pair!**
    /// Also note that this implementation assumes that the order_four list
    /// correctly represents the order four vertices in the `Triangulation`, if
    /// this is not it can break the data structures.
    pub fn flip_checkless(&mut self, label: Label) {
        self.flip_centers(label, Self::add_order_four_checkless);
    }

    /// Update the connectivity for flipping a `Triangle` pair, labelled by right `Triangle`.
    ///
    /// This flip is performed by by keeping left left, and right right, but flipping the orientations
    /// and updating the `center` neighbours; so effectively _flipping the `center`s_
    /// Alternatively one can change left to right and visa versa, and update the connectivity accordingly;
    /// NOTE that this also affects the possible update of what `Triangle` becomes the flip pair.
    fn flip_centers(
        &mut self,
        right: Label,
        add_order_four: fn(&mut DynTriangulation, Label) -> bool,
    ) {
        // Find flip pair neighbour
        let left = self.triangles[right].left;

        // Flip orientations
        let orientation_left = self.triangles[left].orientation;
        self.triangles[left].orientation = self.triangles[right].orientation;
        self.triangles[right].orientation = orientation_left;

        // Flip `center` neighbour connectivity
        let neighbour_left = self.triangles[left].center; // Find old timelike neighbours
        let neighbour_right = self.triangles[right].center;
        self.triangles[left].center = neighbour_right; // Set new neighbours
        self.triangles[right].center = neighbour_left;
        self.triangles[neighbour_left].center = right;
        self.triangles[neighbour_right].center = left;

        // Update the flip_pair list
        let slice_neighbour_right = self.triangles[right].right; // Right spacelike of the right triangle of the pair
        self.flip_pair.toggle(left);
        self.flip_pair.toggle(slice_neighbour_right);

        // Re-evaluate the order_four list
        match orientation_left {
            Orientation::Up => {
                // Up-Down -> Down-Up
                let neighbour_top_right = self.triangles[neighbour_right].right; // Top right triangle
                self.order_four.remove(neighbour_top_right); // These two could have been order four,
                self.order_four.remove(left); // but certainly are not after the flip.
                add_order_four(self, neighbour_right); // These two could have become order four after
                add_order_four(self, slice_neighbour_right); // the flip, but can't have been before.
            }
            Orientation::Down => {
                // Down-Up -> Up-Down
                let neighbour_top_right = self.triangles[neighbour_left].right; // Top right triangle
                self.order_four.remove(neighbour_left); // These two could have been order four,
                self.order_four.remove(slice_neighbour_right); // but certainly are not after the flip.
                add_order_four(self, neighbour_top_right); // These two could have become order four after
                add_order_four(self, left); // the flip, but can't have been before.
            }
        }
    }

    /// Check if the neighbours of a `Triangle` mark a _flip pair_.
    ///
    /// Returns `true` if either of them is a _flip pair_, and
    /// `false` if both do **not** mark a _flip pair_.
    pub fn are_neighbours_flip_pairs(&self, label: Label) -> bool {
        let triangle = &self.triangles[label];
        self.is_flip_pair(triangle.left) || self.is_flip_pair(triangle.right)
    }

    /// Check if `label` is order four, based on connectivity.
    /// If this is so add it to `order_four` list, and return if it is.
    fn add_order_four(triangulation: &mut DynTriangulation, label: Label) -> bool {
        if triangulation.is_order_four(label) {
            triangulation.order_four.add(label);
            true
        } else {
            false
        }
    }

    /// Check if `label` is order four for small `Triangulation`s, based on connectivity.
    /// If this is so add it to `order_four` list, and return if it is.
    fn add_order_four_small(triangulation: &mut DynTriangulation, label: Label) -> bool {
        if triangulation.is_order_four_small(label) {
            triangulation.order_four.add(label);
            true
        } else {
            false
        }
    }
    /// Add given `Label` to `order_four` list _if it is_ based on
    /// it's connectivity, returning whether it is of order four.
    ///
    /// NOTE: this does not check if the `label` was in the `order_four`
    /// before, and this will break the list if it was.
    /// So **only use this if certain that the `Triangle` wasn't in the
    /// `order_four` list before**, if uncertain use
    /// [`add_order_four()`](Triangulation::add_order_four())
    fn add_order_four_checkless(triangulation: &mut DynTriangulation, label: Label) -> bool {
        if triangulation.is_order_four(label) {
            triangulation.order_four.add_checkless(label);
            true
        } else {
            false
        }
    }

    /// Sample a random `Triangle` from `Triangulation` by `Label`
    ///
    /// This sampling makes use of the sampling of `LabelledList`,
    /// making it less efficient that the samplings of [`sample_flip()`](Triangulation::sample_flip())
    /// and [`sample_order_four()`](Triangulation::sample_order_four())
    pub fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Label {
        self.triangles.sample(rng)
    }

    /// Sample a random `Triangle` which has a _right_ neighbour of opposite orientation by `Label`
    ///
    /// This sampling makes use of an extra list using `LabelSet`, making this sampling very efficient.
    pub fn sample_flip<R: Rng + ?Sized>(&self, rng: &mut R) -> Label {
        self.flip_pair.sample(rng)
    }

    /// Sample a random vertex of order four by the `Label` of the `Triangle`
    /// at the top right of the vertex.
    ///
    /// This sampling makes use of an extra list using `LabelSet`, making this sampling very efficient.
    pub fn sample_order_four<R: Rng + ?Sized>(&self, rng: &mut R) -> Label {
        self.order_four.sample(rng)
    }

    /// Check if connectivity of given `Up` `Label` matches that of
    /// one marking an order four vertex.
    ///
    /// NOTE that the given `Label` must point to an `Up`-`Triangle`
    /// otherwise one could get **false positives**; use
    /// [`is_order_four()`](Triangulation::is_order_four()) for a safe
    /// check, which also checks the orientation.
    /// This is because the used convention is to mark the order four
    /// vertex with the upper right `Triangle`.
    /// This check does assume the `Triangulation` connectivity
    /// itself is correct.
    pub fn is_up_order_four(&self, label: Label) -> bool {
        // Check if 'left-down' is the same as 'down-left'
        let left = self.triangles[label].left;
        let down = self.triangles[label].center;
        self.triangles[left].center == self.triangles[down].left
    }

    /// Check if connectivity of given `Label` matches that of
    /// one marking an order four vertex.
    ///
    /// Note that the used convention is to mark the order four
    /// vertex with the upper right `Triangle`.
    /// This check does assume the `Triangulation` connectivity
    /// itself is correct.
    pub fn is_order_four(&self, label: Label) -> bool {
        if self.triangles[label].orientation == Orientation::Down {
            return false;
        }
        self.is_up_order_four(label)
    }

    /// Check if connectivity of given `Label` matches that of
    /// one marking an order four vertex, for a small `Triangulation`.
    ///
    /// This functions performs additional checks to [`is_order_four()`](Triangulation::is_order_four())
    /// to assure the result is also correct for `Triangulation`s that have 2 or fewer timeslices.
    /// These are extra checks so this also works for larger `Triangulation`s, although here those checks
    /// are not necessary.
    pub fn is_order_four_small(&self, label: Label) -> bool {
        if self.triangles[label].orientation == Orientation::Down {
            return false;
        }
        let left = self.triangles[label].left;
        if self.triangles[left].orientation == Orientation::Down {
            return false;
        }
        let down = self.triangles[label].center;
        self.triangles[left].center == self.triangles[down].left
    }

    /// Returns whether the `Triangle` pair given by the `Label` of the right `Triangle`
    /// forms a flip_pair, which is to say the orientations are opposite.
    pub fn is_flip_pair(&self, label: Label) -> bool {
        let left = self.triangles[label].left;
        self.triangles[label].orientation != self.triangles[left].orientation
    }

    /// Returns whether there are order four vertices in the `Triangulation`.
    pub fn has_order_four(&self) -> bool {
        !self.order_four.is_empty()
    }

    /// Returns whether there are flip pairs in the `Triangulation`.
    pub fn has_flip_pair(&self) -> bool {
        !self.flip_pair.is_empty()
    }
}

/// Some sort of measurement implementations
impl DynTriangulation {
    /// Returns the amount of `Triangle`s in the `Triangulation`.
    pub fn size(&self) -> usize {
        self.triangles.len()
    }

    /// Returns the order of the vertex associated with the `Triangle` given by `Label`.
    ///
    /// The vertex associated with a `Triangle` is the left-most vertex of a triangle,
    /// meaning the left vertex of the two vertices that are together in a timeslice.
    ///
    /// Note that this measurements requires the `Triangulation` to be setup correctly.
    pub fn vertex_order(&self, label: Label) -> usize {
        let (up, down) = match self.triangles[label].orientation {
            Orientation::Up => (label, self.triangles[label].center),
            Orientation::Down => (self.triangles[label].center, label),
        };
        let mut order: usize = 4; // Start at order 4, and see how many extra triangles there are
        let mut left = self.triangles[up].left;
        while self.triangles[left].orientation != Orientation::Up {
            left = self.triangles[left].left;
            order += 1;
        }
        left = self.triangles[down].left;
        while self.triangles[left].orientation != Orientation::Down {
            left = self.triangles[left].left;
            order += 1;
        }
        order
    }

    /// Return the amount of triangles that label a flip pair
    pub fn flip_pair_count(&self) -> usize {
        self.flip_pair.len()
    }

    /// Return the amount of triangles that label an order four vertex
    pub fn order_four_count(&self) -> usize {
        self.order_four.len()
    }
}

#[cfg(test)]
mod tests {
    // Note some of these test use RNG, which is done with a reproducable RNG using a fixed seed
    use super::*;

    use rand::SeedableRng;
    use rand_xoshiro::Xoshiro256StarStar;

    /// Implementations for `Triangulation` verification
    impl DynTriangulation {
        /// Perform possible check to assess the validity of the `Triangulation`.
        ///
        /// If the `Triangulation` is not valid something has gone wrong in construction,
        /// and unexpected behaviour can occur, so this function `panic`s.
        /// The validity is assessed by [`assert_biconnectivity()`](Triangulation::assert_biconnectivity()),
        /// [`assert_order_four()`](Triangulation::assert_order_four()),
        /// and [`assert_flip_pair()`](Triangulation::assert_flip_pair()).
        fn assert_triangulation(&self) {
            self.assert_biconnectivity();
            self.assert_order_four();
            self.assert_flip_pair();
        }

        /// Check whether all set connections of Triangles, are correctly set in reverse, `panic`s if not.
        fn assert_biconnectivity(&self) {
            let triangles = &self.triangles;
            for label in triangles.labels() {
                // Get all neighbours
                let neighbour_left = triangles[label].left;
                let neighbour_center = triangles[label].center;
                let neighbour_right = triangles[label].right;
                // Check connections of neighbours to original triangle
                assert_eq!(
                    triangles[neighbour_left].right, label,
                    "Connection between {} and left neighbour {} is not correct",
                    label, neighbour_left
                );
                assert_ne!(
                    triangles[label].orientation, triangles[neighbour_center].orientation,
                    "Timelike coupling on non Up-Down pair {} and {}",
                    label, neighbour_center
                );
                assert_eq!(
                    triangles[neighbour_center].center, label,
                    "Connection between {} and center neighbour {} is not correct",
                    label, neighbour_center
                );
                assert_eq!(
                    triangles[neighbour_right].left, label,
                    "Connection between {} and right neighbour {} is not correct",
                    label, neighbour_right
                );
            }
        }

        /// Check whether order_four list is correct and complete, `panic`s if not.
        fn assert_order_four(&self) {
            // First check if `order_four` list doesn't contain non-existent triangles
            for label in self.order_four.iter() {
                assert!(
                    self.triangles.contains(label),
                    "A non-existent Triangle is present in the order four list."
                );
            }

            // Check all Triangles for order four-ness and compare to order four list
            for label in self.triangles.labels() {
                if self.is_order_four_small(label) {
                    assert!(
                        self.order_four.contains(label),
                        "Triangle {} is order four, but not in the order_four list",
                        label
                    );
                } else {
                    assert!(
                        !self.order_four.contains(label),
                        "Triangle {} is not order four, but is in the order_four list",
                        label
                    );
                }
            }
        }

        /// Check whether the flip_pair list is correct and complete, `panic`s if not.
        fn assert_flip_pair(&self) {
            // First check if `flip_pair` list doesn't contain non-existent triangles
            for label in self.flip_pair.iter() {
                assert!(
                    self.triangles.contains(label),
                    "A non-existent Triangle is present in the flip_pair list."
                );
            }

            // Check all Triangles for flip_pair-ness and compare to flip_pair list
            for label in self.triangles.labels() {
                if self.is_flip_pair(label) {
                    assert!(
                        self.flip_pair.contains(label),
                        "Triangle {} marks a flip pair, but is not in the flip_pair list",
                        label
                    );
                } else {
                    assert!(
                        !self.flip_pair.contains(label),
                        "Triangle {} does not mark a flip pair, but is in the flip_pair list",
                        label
                    );
                }
            }
        }
    }

    #[test]
    fn triangulation_creation_small() {
        let mut triangulation = DynTriangulation::new(3, 1);
        triangulation.assert_triangulation();

        let mut rng = Xoshiro256StarStar::seed_from_u64(2);
        triangulation.add(triangulation.sample(&mut rng));
        triangulation.assert_triangulation();
    }

    #[test]
    fn triangulation_connectivity() {
        let triangulation = DynTriangulation::new(8, 11);
        triangulation.assert_triangulation();
    }

    #[test]
    fn triangulation_addition() {
        let mut triangulation = DynTriangulation::new(8, 11);
        triangulation.assert_triangulation();

        let mut rng = Xoshiro256StarStar::seed_from_u64(13);
        for _ in 0..18 {
            triangulation.add(triangulation.sample(&mut rng));
        }
        triangulation.assert_triangulation();

        assert_eq!(triangulation.size(), 2 * (8 * 11 + 18))
    }

    #[test]
    fn triangulation_addition_small() {
        let mut triangulation = DynTriangulation::new_allocate(3, 1, 30);
        triangulation.assert_triangulation();

        let mut rng = Xoshiro256StarStar::seed_from_u64(13);
        for _ in 0..5 {
            triangulation.add(triangulation.sample(&mut rng));
            triangulation.assert_triangulation();
        }
        triangulation.assert_triangulation();

        assert_eq!(triangulation.size(), 2 * (3 + 5))
    }

    #[test]
    fn triangulation_removal() {
        let mut triangulation = DynTriangulation::new(8, 11);
        triangulation.assert_triangulation();

        let mut rng = Xoshiro256StarStar::seed_from_u64(13);
        for _ in 0..18 {
            triangulation.add(triangulation.sample(&mut rng));
        }
        for _ in 0..8 {
            triangulation.remove(triangulation.sample_order_four(&mut rng))
        }
        triangulation.assert_triangulation();

        assert_eq!(triangulation.size(), 2 * (8 * 11 + 18 - 8))
    }

    #[test]
    fn triangulation_removal_small() {
        let mut triangulation = DynTriangulation::new_allocate(3, 2, 30);
        triangulation.assert_triangulation();

        let mut rng = Xoshiro256StarStar::seed_from_u64(13);
        println!("Start: {}", triangulation.id());
        for i in 0..1000 {
            println!("Step {}", i);
            triangulation.add(triangulation.sample(&mut rng));
            println!("Adding: {}", triangulation.id());
            triangulation.assert_triangulation();
            triangulation.remove(triangulation.sample_order_four(&mut rng));
            println!("Removing: {}", triangulation.id());
            triangulation.assert_triangulation();
            triangulation.flip_small(triangulation.sample_flip(&mut rng));
            println!("Flipping 1: {}", triangulation.id());
            triangulation.assert_triangulation();
            triangulation.flip_small(triangulation.sample_flip(&mut rng));
            println!("Flipping 2: {}", triangulation.id());
            triangulation.assert_triangulation();
        }

        triangulation.assert_triangulation();
    }

    #[test]
    fn triangulation_flip() {
        let mut triangulation = DynTriangulation::new(8, 11);
        triangulation.assert_triangulation();
        assert_eq!(
            triangulation.order_four.len(),
            0,
            "Triangulation started out with some order four Triangles"
        );

        let mut rng = Xoshiro256StarStar::seed_from_u64(13);
        for _ in 0..100 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }
        triangulation.assert_triangulation();
        assert_eq!(
            triangulation.order_four.len(),
            9,
            "Triangulation has an unexpected amount of order_four vertices"
        );

        for _ in 0..4 {
            triangulation.remove(triangulation.sample_order_four(&mut rng))
        }
        triangulation.assert_triangulation();
        assert_eq!(
            triangulation.order_four.len(),
            5,
            "Triangulation has an unexpected amount of order_four vertices"
        );

        for _ in 0..18 {
            triangulation.add(triangulation.sample(&mut rng));
        }
        for _ in 0..8 {
            triangulation.remove(triangulation.sample_order_four(&mut rng))
        }
        for _ in 0..30 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }
        triangulation.assert_triangulation();
        assert_eq!(triangulation.order_four.len(), 22)
    }

    #[test]
    fn triangulation_flip_small() {
        let mut triangulation = DynTriangulation::new(3, 2);
        triangulation.assert_triangulation();

        // let mut rng = Xoshiro256StarStar::seed_from_u64(13);
        println!("{}", triangulation.id());
        println!(
            "Triangle 3: {}",
            triangulation.triangles[Label::from(3)].orientation
        );
        triangulation.flip(Label::from(3));
        println!("{}", triangulation.id());

        triangulation.assert_triangulation();
    }

    #[test]
    fn triangulaton_vertex_order() {
        let mut triangulation = DynTriangulation::new(8, 11);
        let mut rng = Xoshiro256StarStar::seed_from_u64(12);

        // Check if flat indeed means vertex order is 6 everywhere
        for _ in 0..20 {
            let label = triangulation.sample(&mut rng);
            assert_eq!(
                triangulation.vertex_order(label),
                6,
                "Vertex order of Triangle in starting Triangulation is not 6"
            );
        }

        // Perform some moves to get a spread of vertex orders
        for _ in 0..100 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }
        for _ in 0..18 {
            triangulation.add(triangulation.sample(&mut rng));
        }
        for _ in 0..8 {
            triangulation.remove(triangulation.sample_order_four(&mut rng))
        }
        for _ in 0..30 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }
        triangulation.assert_triangulation();

        // Check if vertex_order() indeed gives vertex order 4 for all those in order_four
        for label in triangulation.order_four.iter() {
            assert_eq!(
                triangulation.vertex_order(label),
                4,
                "Vertex order of Triangle in order_four list is not 4"
            );
        }

        assert_eq!(
            triangulation.vertex_order(triangulation.sample(&mut rng)),
            6
        );
        assert_eq!(
            triangulation.vertex_order(triangulation.sample(&mut rng)),
            7
        );
        assert_eq!(
            triangulation.vertex_order(triangulation.sample(&mut rng)),
            5
        );
        assert_eq!(
            triangulation.vertex_order(triangulation.sample(&mut rng)),
            4
        );
        assert_eq!(
            triangulation.vertex_order(triangulation.sample(&mut rng)),
            6
        );
    }

    #[test]
    fn triangulation_are_neighbours_flip_pair() {
        let mut triangulation = DynTriangulation::new(8, 11);

        assert!(triangulation.are_neighbours_flip_pairs(Label::from(8)));
        triangulation.flip(Label::from(8));
        assert!(!triangulation.are_neighbours_flip_pairs(Label::from(8)));

        assert!(triangulation.are_neighbours_flip_pairs(Label::from(34)));
        triangulation.flip(triangulation.triangles[Label::from(34)].left);
        assert!(
            triangulation.are_neighbours_flip_pairs(triangulation.triangles[Label::from(34)].right)
        );
    }
}
