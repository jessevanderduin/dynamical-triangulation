#%% Setup
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as int

#%%
def distance(th1, th2, r):
    n = np.array([[-1, 0, 1], [-1, 0, 1], [-1, 0, 1]])
    m = np.array([[-1, -1, -1], [0, 0, 0], [1, 1, 1]])
    dx = np.cos(th1) - np.cos(th2) + n/r + 1.0
    dy = np.sin(th1) - np.sin(th2) + m/r
    return np.min(np.sqrt(dx**2 + dy**2))

def sphere_distance(r):
    if r < 0.1:
        return 1.574597
    print(r)
    return int.dblquad(distance, 0.0, 2*np.pi, 0.0, 2*np.pi, args=(r,), epsabs=0.01)[0] / 4.0/np.pi**2

r = np.linspace(0.01, 0.5, 10)
plt.plot(r, np.vectorize(sphere_distance)(r))