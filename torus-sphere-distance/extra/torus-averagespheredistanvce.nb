(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     14792,        390]
NotebookOptionsPosition[     12797,        347]
NotebookOutlinePosition[     13194,        363]
CellTagsIndexPosition[     13151,        360]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"d", "[", 
   RowBox[{"\[Theta]1_", ",", "\[Theta]2_"}], "]"}], ":=", 
  SqrtBox[
   RowBox[{
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       RowBox[{"Cos", "[", "\[Theta]1", "]"}], "-", 
       RowBox[{"Cos", "[", "\[Theta]2", "]"}], "+", "1"}], ")"}], "2"], "+", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       RowBox[{"Sin", "[", "\[Theta]1", "]"}], "-", 
       RowBox[{"Sin", "[", "\[Theta]2", "]"}]}], ")"}], "2"]}]]}]], "Input",
 CellChangeTimes->{{3.854878818553183*^9, 3.854878884798606*^9}, 
   3.854878960175742*^9, {3.854879854970119*^9, 3.854879856306464*^9}, {
   3.8550595643237877`*^9, 3.855059565204053*^9}, 3.855059970722375*^9},
 CellLabel->"In[1]:=",ExpressionUUID->"e69ff8b6-bd64-4582-8828-4b9e231917cc"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"NIntegrate", "[", 
   RowBox[{
    RowBox[{"d", "[", 
     RowBox[{"\[Theta]1", ",", "\[Theta]2"}], "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"\[Theta]1", ",", "0", ",", 
      RowBox[{"2", "\[Pi]"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"\[Theta]2", ",", "0", ",", 
      RowBox[{"2", "\[Pi]"}]}], "}"}]}], "]"}], "/", 
  SuperscriptBox[
   RowBox[{"(", 
    RowBox[{"2", "\[Pi]"}], ")"}], "2"]}]], "Input",
 CellChangeTimes->{{3.854878888107572*^9, 3.854878930273576*^9}},
 CellLabel->"In[20]:=",ExpressionUUID->"f996468e-b36a-4d2c-ad63-ea72413fefaa"],

Cell[BoxData["1.5745972697426058`"], "Output",
 CellChangeTimes->{{3.8548789324156857`*^9, 3.854878961657092*^9}, 
   3.854879857613451*^9},
 CellLabel->"Out[20]=",ExpressionUUID->"10a90291-63f2-4a5e-8783-c8b5fa2281cc"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Integrate", "[", 
   RowBox[{
    RowBox[{"d", "[", 
     RowBox[{"\[Theta]1", ",", "\[Theta]2"}], "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"\[Theta]1", ",", "0", ",", 
      RowBox[{"2", "\[Pi]"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"\[Theta]2", ",", "0", ",", 
      RowBox[{"2", "\[Pi]"}]}], "}"}]}], "]"}], "/", 
  SuperscriptBox[
   RowBox[{"(", 
    RowBox[{"2", "\[Pi]"}], ")"}], "2"]}]], "Input",
 CellChangeTimes->{3.854879117297408*^9},
 CellLabel->"In[21]:=",ExpressionUUID->"14a1f47f-c7d5-41be-8317-7201d0675e87"],

Cell[BoxData["$Aborted"], "Output",
 CellChangeTimes->{3.854879153411484*^9, 3.854879866294331*^9},
 CellLabel->"Out[21]=",ExpressionUUID->"2412cd8d-3fed-402a-aa21-5d0cd846c194"]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"dr", "[", 
   RowBox[{"\[Theta]1_", ",", "\[Theta]2_", ",", "r_"}], "]"}], ":=", 
  RowBox[{"Flatten", "[", 
   RowBox[{"Table", "[", 
    RowBox[{
     SqrtBox[
      RowBox[{
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{
          RowBox[{"Cos", "[", "\[Theta]1", "]"}], "-", 
          RowBox[{"Cos", "[", "\[Theta]2", "]"}], "+", "1", " ", "+", " ", 
          FractionBox["n", "r"]}], ")"}], "2"], "+", 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{
          RowBox[{"Sin", "[", "\[Theta]1", "]"}], "-", 
          RowBox[{"Sin", "[", "\[Theta]2", "]"}], "+", 
          FractionBox["m", "r"]}], ")"}], "2"]}]], ",", 
     RowBox[{"{", 
      RowBox[{"n", ",", 
       RowBox[{"-", "1"}], ",", "1"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"m", ",", 
       RowBox[{"-", "1"}], ",", "1"}], "}"}]}], "]"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.854879599500082*^9, 3.854879648997469*^9}, {
   3.85487972354707*^9, 3.854879724411775*^9}, {3.854879793216598*^9, 
   3.8548798869870977`*^9}, 3.855051181782298*^9},
 CellLabel->"In[1]:=",ExpressionUUID->"be61433b-c8c6-4da8-8af9-90e4c863a34d"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"NIntegrate", "[", 
   RowBox[{
    RowBox[{"Min", "[", 
     RowBox[{"dr", "[", 
      RowBox[{"\[Theta]1", ",", "\[Theta]2", ",", "0.1"}], "]"}], "]"}], ",", 
    
    RowBox[{"{", 
     RowBox[{"\[Theta]1", ",", "0", ",", 
      RowBox[{"2", "\[Pi]"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"\[Theta]2", ",", "0", ",", 
      RowBox[{"2", "\[Pi]"}]}], "}"}]}], "]"}], "/", 
  SuperscriptBox[
   RowBox[{"(", 
    RowBox[{"2", "\[Pi]"}], ")"}], "2"]}]], "Input",
 CellChangeTimes->{{3.854880042697402*^9, 3.854880043100149*^9}},
 CellLabel->"In[3]:=",ExpressionUUID->"1d6d0ea3-128d-4b4e-989c-1516cd69be1e"],

Cell[BoxData["1.5745972697426058`"], "Output",
 CellChangeTimes->{3.8548800485193653`*^9},
 CellLabel->"Out[3]=",ExpressionUUID->"a7a83e4c-c9c0-49e4-910c-55b99a03b6fd"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"NIntegrate", "[", 
     RowBox[{
      RowBox[{"Min", "[", 
       RowBox[{"dr", "[", 
        RowBox[{"\[Theta]1", ",", "\[Theta]2", ",", "r"}], "]"}], "]"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"\[Theta]1", ",", "0", ",", 
        RowBox[{"2", "\[Pi]"}]}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"\[Theta]2", ",", "0", ",", 
        RowBox[{"2", "\[Pi]"}]}], "}"}]}], "]"}], "/", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{"2", "\[Pi]"}], ")"}], "2"]}], ",", 
   RowBox[{"{", 
    RowBox[{"r", ",", "0", ",", "0.5"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.854879741981082*^9, 3.8548797494740343`*^9}, 
   3.854879812088529*^9, {3.8548798984058657`*^9, 3.8548799342531157`*^9}, {
   3.854879988499382*^9, 3.8548799973232393`*^9}},
 CellLabel->"In[4]:=",ExpressionUUID->"ff394119-ddba-4b1b-b552-9adc32308188"],

Cell[BoxData[
 GraphicsBox[{{{}, {}, 
    TagBox[
     {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
      1.], LineBox[CompressedData["
1:eJxF03k4lekbB3CkkBLaKEvOoJMWZYwJja9SpFKDSjX2iRqaEiKSUrhsg19p
QagsWTrZKRHJ+1qzU3TyTqjx2qIs50SZd/74nf54ruf6/PFcz/187/tRcTxj
4SQiJCQUxqz/9j1OAy0V9C6Dk6lKAxFb+Li7/EBwmLwn/u8A8fy1ofKBAh85
ei97bDxGYMm5AGsTpSSB1cxHB16Epwjs3aiL2xMZAq8skOkRFs4WuKjXqKFp
W57AlS4Jq48OFwg8qf7LEs6fxQKL8LOkV6uVCKwzZ19k+ahU4IZA7UZSo1xg
5eghfsZohcBD01RPx+ZKgVMGn9ZJRb4QWErxwcf+/iqBEz+E2HIXEQJvynF5
2ab93ddYP2S0GHz3VMybwKbd381uLHZvNPvuY2Ix9i8tvjvC0G1/gxWBsyFl
N/7zM5992+qtCfj+7c8KYzyWx9aocyAwM7BfKIgxa1hUrtaZgLpCO+ci44Nq
7+bXuBKYs3PK8GAcbFv2mXQjECHn4HmC8eNbse+IcwQKoq76HGE82HyuqcqX
wOeeVlMTxgoLLcpeXCKg/HrU40fGZkabsioDCXBSrBwVGfv7LYx9HsrcH/qE
K8o4p/BDcEUkAYMwJfnBzXz0jlZ6ll8nkDPb193AeBk7yfHZbQI3uKnaHMbG
Dhd+LUsgIDHsLxnO+HyclUHpfQJuDRYDzoy5i6RXlTwk4CofcWIlYynjYbEn
uUx9md6mw5p8GF6qmSwuIpAeJtH8jHHK+OWWwgoCsfZpX6wZd2rYlBcQzHlz
ozg2Y/Hjupz8OgI1hpKbP23i41TneEhuO4Ft3pV/+DNOXNLoldNFwPTmDfXt
jJt3Zx7P7iEQsmZqSJix9lNHQ84AAXacHsdvIx+zSW3T6bMEUorN5x/ZwPS/
K/v9A2ESzq5areKM7WUj2tIWkLA0/rGleD0fVYE7s1OkSZgl1TbKMI74o9D5
rioJuQRl08x1TP6GnBBJDRI2+f6aRoxN5FIzvTRJaJZNEl1sZr6rY0b36ZGw
0lI/P7eWDx9VTy/+ARJfpnsWa6oz751xvX38EInq/tzmQjWm3rbfS5qOkRhf
fqBel/FYgOW3VCcSu3pn+vVU+XCmtIItLpCYciDOsVhMfUUa6WWXSVzXb+i7
qcLMUySrjh1Moq7q7XoJxm+3yUrNRZNItfSV/keZma+48RtZaSS81GrtQxX5
2H4oJ1W0lcQC1eB+Fzkmzw3p1WdekTggzOkqW8nkNe8u3c0lMXJxoFWK8eO8
qI25/zD5sB6qZy1n8pM+U2jzlcRgwq3Eellm/uo3EkXsavj36XDyFzH/xzCr
/+SlakTGOsyy53goTRmK9w6sxvv0JBPzbzxcEd9gGRxaDZounvH+yoNU88Pn
yderoTqh/tvzGR7U7R4l9jyohl2GkZgRj4fDF3OPHGyuRnDVyDXRcR4KnxQ3
QKUGZrLmkWHvePDUqipY8aIGBb9a10qU8+Csvebu/2pq0H7g69X6Mh6O6PiF
L2qsgYhcHie8lAd9PW1Hka4aXN1iuUaihAeRHSlLRkdr4H6t2mOygIco80AX
YlUtYq5xP93K5CHTbecaT/da+CYNN0nf4IF6VBXeolIHxdQL7zVO8DDj0h6/
zrce3Z0KDU6iPKzfLvbN+WkDTE4Ytandm4bJfZUTuSsb0aXhuG75lml4K9qx
3lk3IYO3lx1NTmHD8CHJV2bN6HBseuxkPYWlS8bEq740Q3OVa4pnzyREy+Oz
f7jTAsOMMcdB+0m4aj/tktjbiqKEnyvy2yaQZep499lUK2w+BpGBxhOYHFol
/eFmG3S3cVlOlZ9RaGkyL2ZnO9pN41z3an/Gpd1BSQUf2iF2/Mr42rxPUHm7
53VyUAdW6Kgke+l8gt7PgTHyWp3QWZYf7100DmulJr5wWyfcs24l5Owcx2Td
X71uAa+gn7uJP9s4htuitp4Wqq9hZxveMO00hqU9hfdFql6DrV+yf8e8Mcy/
GHBQ92wXSh+/qTXW/QihDNvsXtluzPgs/hITMAr3uOy/ZCu7YXBf6tC9yhGU
rQ8Rlzn9BpH5d/7kS41AZo/7hitSXNBtQZwVp4fB3djc5lPKRXdZQmcuOYSc
RKj2275FniS/umvzEGyydWQUv71Fitex+pbkQVwOuhfAzeyBZYTk4Z9kBsEZ
UIyX2kfhTILe4a2RNEISgux79lOIT6D1ZsNpHLcYUXtkTqHpw3XfilAaCqWl
OWZWFM55+O0yDaIRFvUbGeFIodDunYq9H42TOrHjC30oBPeNpt5zocEKXLZb
LI0CoXTUzcOExretfotfpVOwku/oN9pFo2ukrzUti0JfTlbgMiMa0Vb5Nsa5
FCRYuQnFBjTmNCw8gkop2K083SysQ4PbEpU4r42Cw+pTh9NVaTwOnv69rYNi
+qSufIlFI0bfbl3yawq755RcD62hsTd1U8GOHgpO7rHSogo0Ss6/rA2gKVi8
UKhyWkrj5safosyHKXAds8t/kaFxtvfOQZWPFEwXCbnKLaHB3neKqpigIG+f
4diwkIaocHtK9DSFrS3s/DRxGn8X6rvYf6GgFT2RHLCARqlLsubmrxSMVeZu
W4vSuKUsOTk3R+H55Y55W0Vo/At7NWrf
       "]]},
     Annotation[#, "Charting`Private`Tag$4241#1"]& ]}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0.7815208506472332},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  Method->{
   "DefaultBoundaryStyle" -> Automatic, 
    "DefaultGraphicsInteraction" -> {
     "Version" -> 1.2, "TrackMousePosition" -> {True, False}, 
      "Effects" -> {
       "Highlight" -> {"ratio" -> 2}, "HighlightPoint" -> {"ratio" -> 2}, 
        "Droplines" -> {
         "freeformCursorMode" -> True, 
          "placement" -> {"x" -> "All", "y" -> "None"}}}}, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None, 
    "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& )}},
  PlotRange->{{0, 0.5}, {0.7815208506472332, 1.5745972697426058`}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{3.854880781578347*^9},
 CellLabel->"Out[4]=",ExpressionUUID->"2a1f9714-096f-4e95-ad58-415f60d761a8"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"N", "[", 
  RowBox[{"1", "/", "6"}], "]"}]], "Input",
 CellChangeTimes->{{3.8548808443281507`*^9, 3.8548808497564993`*^9}},
 CellLabel->"In[5]:=",ExpressionUUID->"b84cdb78-5b40-4781-8931-bad76ae2dc9c"],

Cell[BoxData["0.16666666666666666`"], "Output",
 CellChangeTimes->{3.854880850038949*^9},
 CellLabel->"Out[5]=",ExpressionUUID->"e5b390fd-95d8-4b30-b070-0bd91acd360a"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"NIntegrate", "[", 
   RowBox[{
    RowBox[{"Min", "[", 
     RowBox[{"dr", "[", 
      RowBox[{"\[Theta]1", ",", "\[Theta]2", ",", "0.0001"}], "]"}], "]"}], 
    ",", 
    RowBox[{"{", 
     RowBox[{"\[Theta]1", ",", "0", ",", 
      RowBox[{"2", "\[Pi]"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"\[Theta]2", ",", "0", ",", 
      RowBox[{"2", "\[Pi]"}]}], "}"}]}], "]"}], "/", 
  SuperscriptBox[
   RowBox[{"(", 
    RowBox[{"2", "\[Pi]"}], ")"}], "2"]}]], "Input",
 CellChangeTimes->{{3.855051186582294*^9, 3.855051207088613*^9}},
 CellLabel->"In[3]:=",ExpressionUUID->"a5bae822-b96b-4528-b7e6-9f4fedcc3710"],

Cell[BoxData["1.5745972697426058`"], "Output",
 CellChangeTimes->{{3.855051202291523*^9, 3.855051212698462*^9}},
 CellLabel->"Out[3]=",ExpressionUUID->"e7700c85-1386-4fe5-8e4c-50db1546cb24"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  SqrtBox[
   RowBox[{
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       RowBox[{"Cos", "[", "\[Theta]1", "]"}], "-", 
       RowBox[{"Cos", "[", "\[Theta]2", "]"}], "+", "1"}], ")"}], "2"], "+", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       RowBox[{"Sin", "[", "\[Theta]1", "]"}], "-", 
       RowBox[{"Sin", "[", "\[Theta]2", "]"}]}], ")"}], "2"]}]], "//", 
  "FullSimplify"}]], "Input",
 CellChangeTimes->{{3.8550599749640636`*^9, 3.8550599766082287`*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"238068e7-ae17-44ac-b9a3-f1657fddef56"],

Cell[BoxData[
 SqrtBox[
  RowBox[{"3", "+", 
   RowBox[{"2", " ", 
    RowBox[{"Cos", "[", "\[Theta]1", "]"}]}], "-", 
   RowBox[{"2", " ", 
    RowBox[{"Cos", "[", 
     RowBox[{"\[Theta]1", "-", "\[Theta]2"}], "]"}]}], "-", 
   RowBox[{"2", " ", 
    RowBox[{"Cos", "[", "\[Theta]2", "]"}]}]}]]], "Output",
 CellChangeTimes->{3.855059977060549*^9},
 CellLabel->"Out[1]=",ExpressionUUID->"b79f1ce3-4d43-41e4-aec4-e060a36e1c4b"]
}, Open  ]]
},
WindowSize->{606., 658.5},
WindowMargins->{{417, Automatic}, {0, Automatic}},
FrontEndVersion->"12.2 for Linux x86 (64-bit) (December 12, 2020)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"86e64c1d-6c21-446b-9718-d8fe13611b6f"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 786, 19, 34, "Input",ExpressionUUID->"e69ff8b6-bd64-4582-8828-4b9e231917cc"],
Cell[CellGroupData[{
Cell[1369, 43, 599, 16, 31, "Input",ExpressionUUID->"f996468e-b36a-4d2c-ad63-ea72413fefaa"],
Cell[1971, 61, 219, 3, 33, "Output",ExpressionUUID->"10a90291-63f2-4a5e-8783-c8b5fa2281cc"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2227, 69, 574, 16, 31, "Input",ExpressionUUID->"14a1f47f-c7d5-41be-8317-7201d0675e87"],
Cell[2804, 87, 178, 2, 33, "Output",ExpressionUUID->"2412cd8d-3fed-402a-aa21-5d0cd846c194"]
}, Open  ]],
Cell[2997, 92, 1173, 30, 127, "Input",ExpressionUUID->"be61433b-c8c6-4da8-8af9-90e4c863a34d"],
Cell[CellGroupData[{
Cell[4195, 126, 650, 18, 52, "Input",ExpressionUUID->"1d6d0ea3-128d-4b4e-989c-1516cd69be1e"],
Cell[4848, 146, 168, 2, 33, "Output",ExpressionUUID->"a7a83e4c-c9c0-49e4-910c-55b99a03b6fd"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5053, 153, 924, 24, 54, "Input",ExpressionUUID->"ff394119-ddba-4b1b-b552-9adc32308188"],
Cell[5980, 179, 4430, 91, 240, "Output",ExpressionUUID->"2a1f9714-096f-4e95-ad58-415f60d761a8"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10447, 275, 225, 4, 29, "Input",ExpressionUUID->"b84cdb78-5b40-4781-8931-bad76ae2dc9c"],
Cell[10675, 281, 167, 2, 33, "Output",ExpressionUUID->"e5b390fd-95d8-4b30-b070-0bd91acd360a"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10879, 288, 653, 18, 52, "Input",ExpressionUUID->"a5bae822-b96b-4528-b7e6-9f4fedcc3710"],
Cell[11535, 308, 190, 2, 33, "Output",ExpressionUUID->"e7700c85-1386-4fe5-8e4c-50db1546cb24"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11762, 315, 588, 16, 34, "Input",ExpressionUUID->"238068e7-ae17-44ac-b9a3-f1657fddef56"],
Cell[12353, 333, 428, 11, 33, "Output",ExpressionUUID->"b79f1ce3-4d43-41e4-aec4-e060a36e1c4b"]
}, Open  ]]
}
]
*)

