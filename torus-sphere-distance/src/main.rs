#![allow(warnings)]

use plotters::prelude::*;
use sequential_integration::{calculate_double_integral_simpson, calculate_triple_integral_simpson, errors::Error};
use std::f64::consts::TAU;
use gsl_rust::integration::qag;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("{:?}", qag(0.0, 1.0, |x| 2.0*x).unwrap());
    // plotting()
    Ok(())
}

fn plotting() -> Result<(), Box<dyn std::error::Error>> {
    let root = BitMapBackend::new("./plots/torus.png", (640, 480)).into_drawing_area();
    root.fill(&WHITE)?;
    let mut chart = ChartBuilder::on(&root)
        .margin(10)
        .x_label_area_size(30)
        .y_label_area_size(30)
        .build_cartesian_2d(0.0f32..0.8f32, 0.5f32..1.65f32)?;

    chart.configure_mesh().draw()?;

    let rs: Vec<f64> = linspace(0.0, 0.8, 50).collect();
    let mut ds: Vec<f64> = Vec::with_capacity(rs.len());

    for &r in &rs {
        let d = average_sphere_distance(r, 0.1)?;
        ds.push(d);
        println!("{}", d)
    }

    chart
        .draw_series(LineSeries::new(
            rs.iter().zip(ds).map(|(&r, d)| (r as f32, d as f32)),
            &RED,
        ))?;

    chart
        .configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .draw()?;

    Ok(())
}

fn distance(th1: f64, th2: f64, alpha: f64, r: f64) -> f64 {
    [-1.0, 0.0, 1.0]
        .into_iter()
        .flat_map(|n| {
            [-1.0, 0.0, 1.0].into_iter().map(move |m| {
                let dx = th1.cos() - th2.cos() + alpha.cos() + n / r;
                let dy = th1.sin() - th2.sin() + alpha.sin() + m / r;
                (dx * dx + dy * dy).sqrt()
            })
        })
        .reduce(|min, dist| if dist < min { dist } else { min })
        .unwrap()
}

fn sphere_distance(r: f64, step: f64, alpha: f64) -> Result<f64, Error> {
    if r < 0.1 {
        return sphere_distance(0.1, step, alpha);
    }

    let d = calculate_double_integral_simpson(
        |th1, th2| distance(th1, th2, alpha, r),
        0.0,
        TAU,
        step,
        |_| 0.0,
        |_| TAU,
        step,
    )?;
    Ok(d / TAU / TAU)
}

fn average_sphere_distance(r: f64, step: f64) -> Result<f64, Error> {
    if r < 0.1 {
        return average_sphere_distance(0.1, step);
    }

    let d = calculate_triple_integral_simpson(
        |th1, th2, alpha| distance(th1, th2, alpha, r),
        0.0,
        TAU,
        step,
        |_| 0.0,
        |_| TAU,
        step,
        |_, _| 0.0,
        |_, _| TAU,
        0.5
    )?;
    Ok(d / TAU / TAU / TAU)
}

fn linspace(start: f64, stop: f64, count: usize) -> Box<dyn Iterator<Item = f64>> {
    let step = (stop - start) / (count as f64);
    Box::new((0..count).map(move |i| start + step * i as f64))
}
