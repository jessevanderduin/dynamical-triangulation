use serde::{Deserialize, Serialize};
use config_file_test::number_range::NumberRange;

pub fn parse_toml() {
    let file = include_str!("iter_config.toml");
    let config: Config = toml::from_str(file).unwrap();
    println!("{:?}", config);
    for i in config.range.into_iter() {
        print!("{} ", i);
    }
    println!();
}

pub fn create_toml() {
    let config = Config {
        range: NumberRange::Stepped((2..=5).into()),
    };
    let toml = toml::to_string_pretty(&config).unwrap();
    println!("{}", toml);
}

#[derive(Debug, Serialize, Deserialize)]
struct Config {
    range: NumberRange<i32>,
}


