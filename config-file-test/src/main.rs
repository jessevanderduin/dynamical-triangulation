#![allow(warnings)]
//! This crate is a test create for creating a good configuration file
//! for a scientific copmutational project.
//!
//! In this example for a Causal Dynamical Triangulation Monte Carlo simulation
//!
//! The configuration file is in TOML format and of the following form
//! ```ignore
#![doc = include_str!("../config.toml")]
//! ```

mod iterator;

use std::{fs::File, io::Read, path::PathBuf};

use observable_parameters::ObservableParameters;
use serde::Deserialize;

const DEFAULT_MEASUREMENT_AMOUNT: usize = 1;
const DEFAULT_BURNIN_SWEEP: usize = 0;
const DEFAULT_MEASUREMENT_INTERVAL: f32 = 1.0;

fn main() {
    iterator::parse_toml();
    iterator::create_toml();
    // let toml = read_config_file("./config.toml").unwrap();
    // println!("{:?}", toml);
}

fn read_config_file(config_path: &str) -> std::io::Result<Config> {
    let mut config_file = File::open(config_path)?;
    let mut config_str = String::new();
    config_file.read_to_string(&mut config_str)?;

    let config = toml::from_str(&config_str).expect("Could not parse the TOML");
    Ok(config)
}

/// ```
/// println!("test2");
/// ```
#[derive(Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct Config {
    size: usize,

    timeslices: usize,

    #[serde(default = "default_measurement_amount")]
    measurements: usize,

    #[serde(default = "default_burnin_sweep")]
    burnin: usize,

    #[serde(default = "default_measurement_interval")]
    interval: f32,

    output_directory: Option<PathBuf>,

    seed: Option<u64>,

    observables: ObservableParameters,
}

fn default_measurement_amount() -> usize {
    DEFAULT_MEASUREMENT_AMOUNT
}

fn default_burnin_sweep() -> usize {
    DEFAULT_BURNIN_SWEEP
}

fn default_measurement_interval() -> f32 {
    DEFAULT_MEASUREMENT_INTERVAL
}

#[allow(dead_code)]
mod observable_parameters {
    const DISTANCE_PROFILE_MAX_DISTANCE: usize = 15;

    use std::path::PathBuf;

    use serde::Deserialize;

    #[derive(Debug, Default, Deserialize)]
    #[serde(rename_all = "kebab-case", default)]
    pub struct ObservableParameters {
        distance_profile: Vec<DistanceProfile>,
        vertex_order: Vec<VertexOrder>,
        length_profile: Vec<LengthProfile>,
        average_sphere_distance: Vec<AverageSphereDistance>,
        asd_correlation_profile: Vec<ASDCorrelationProfile>,
    }

    #[derive(Debug, Deserialize)]
    #[serde(rename_all = "kebab-case")]
    struct DistanceProfile {
        filename: Option<PathBuf>,
        #[serde(default = "ObservableParameters::distance_profile_max_distance")]
        max_distance: usize,
    }

    #[derive(Debug, Deserialize)]
    struct VertexOrder {
        filename: Option<PathBuf>,
    }

    #[derive(Debug, Deserialize)]
    struct LengthProfile {
        filename: Option<PathBuf>,
    }

    #[derive(Debug, Deserialize)]
    struct AverageSphereDistance {
        filename: Option<PathBuf>,
        max_delta: usize,
    }

    #[derive(Debug, Deserialize)]
    struct ASDCorrelationProfile {
        filename: Option<PathBuf>,
    }

    impl ObservableParameters {
        fn distance_profile_max_distance() -> usize {
            DISTANCE_PROFILE_MAX_DISTANCE
        }
    }
}

#[test]
fn test_toml() {
    let config: Config = toml::from_str(include_str!("../config.toml")).unwrap();
    println!("{:?}", config);
}
