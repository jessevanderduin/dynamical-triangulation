mod stepped_range;

use serde::{Deserialize, Serialize};
use std::{
    iter,
    ops::{AddAssign, RangeInclusive},
    vec,
};

use self::stepped_range::DefaultStepSize;
pub use self::stepped_range::SteppedRange;

#[derive(Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum NumberRange<T: DefaultStepSize> {
    Number(T),
    List(Vec<T>),
    Stepped(SteppedRange<T>),
}

pub enum IntoIter<Idx> {
    Number(iter::Once<Idx>),
    List(vec::IntoIter<Idx>),
    Stepped(stepped_range::IntoIter<Idx>),
}

impl<Idx: Copy + PartialOrd + AddAssign> Iterator for IntoIter<Idx> {
    type Item = Idx;
    fn next(&mut self) -> Option<Self::Item> {
        match self {
            IntoIter::Number(iter) => iter.next(),
            IntoIter::List(iter) => iter.next(),
            IntoIter::Stepped(iter) => iter.next(),
        }
    }
}

impl<T: DefaultStepSize + Copy + PartialOrd + AddAssign> IntoIterator for NumberRange<T> {
    type Item = T;
    type IntoIter = IntoIter<T>;

    fn into_iter(self) -> IntoIter<T> {
        match self {
            NumberRange::Number(x) => IntoIter::Number(iter::once(x)),
            NumberRange::List(list) => IntoIter::List(list.into_iter()),
            NumberRange::Stepped(range) => IntoIter::Stepped(range.into_iter()),
        }
    }
}

#[test]
fn test_number_range() {
    // Test a number
    let range = NumberRange::Number(5);
    let mut iter = range.into_iter();
    assert_eq!(iter.next(), Some(5));
    assert_eq!(iter.next(), None);

    // Test a vector
    let range = NumberRange::List(vec![3, 1, 5]);
    let mut iter = range.into_iter();
    assert_eq!(iter.next(), Some(3));
    assert_eq!(iter.next(), Some(1));
    assert_eq!(iter.next(), Some(5));
    assert_eq!(iter.next(), None);

    // Test a stepped range
    let range = NumberRange::Stepped(SteppedRange {
        start: 1,
        end: 5,
        step: 2,
    });
    let mut iter = range.into_iter();
    assert_eq!(iter.next(), Some(1));
    assert_eq!(iter.next(), Some(3));
    assert_eq!(iter.next(), Some(5));
    assert_eq!(iter.next(), None);
}
